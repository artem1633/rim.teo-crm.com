<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class StandingsAsset
 * @package app\assets
 */
class StandingsAsset extends AssetBundle
{
    public $css = [
        'css/standings.css'
    ];

    public $js = [
        'js/standings.js'
    ];

    public $depends = [
        'app\assets\AppAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}