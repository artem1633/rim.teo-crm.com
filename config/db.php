<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=rim.teo-crm.com',
    'username' => 'rim.teo-crm.com',
    'password' => 'rim.teo-crm.com',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,

    //'schemaCache' => 'cache',
];
