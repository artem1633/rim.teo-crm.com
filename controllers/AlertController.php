<?php

namespace app\controllers;

use Yii;
use app\models\Alert;
use app\models\AlertSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Resume;
use yii\data\ActiveDataProvider;

/**
 * AlertController implements the CRUD actions for Alert model.
 */
class AlertController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Alert models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new AlertSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Alert model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Alert #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Alert model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($questionary_id = null)
    {
        $request = Yii::$app->request;
        $model = new Alert();  
        $model->questionary_id = $questionary_id;

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->validate()){

                $post = $request->post();
                $result = "";
                $statuses = [];
                $i = 0;
                foreach ($post['Alert']['statuses'] as $value) {
                    if($i == 0) $result .= $value;
                    else $result .= ',' . $value;
                    $statuses [] = $value;
                    $i++;
                }
                $model->statuses = $result;

                $result = "";
                $groups = [];
                $i = 0;
                foreach ($post['Alert']['groups'] as $value) {
                    if($i == 0) $result .= $value;
                    else $result .= ',' . $value;
                    $groups [] = $value;
                    $i++;
                }
                $model->groups = $result;

                $result = "";
                $vacancies = [];
                $i = 0;
                foreach ($post['Alert']['vacancies'] as $value) {
                    if($i == 0) $result .= $value;
                    else $result .= ',' . $value;
                     $vacancies [] = $value;
                    $i++;
                }
                $model->vacancies = $result;

                $result = "";
                $tags = [];
                $i = 0;
                foreach ($post['Alert']['tags'] as $value) {
                    if($i == 0) $result .= $value;
                    else $result .= ',' . $value;
                     $tags [] = $value;
                    $i++;
                }
                $model->tags = $result;



                $query = Resume::find()->where(['questionary_id' => $model->questionary_id]);
                $dataProvider = new ActiveDataProvider([
                    'query' => $query,
                ]);
                if($statuses != null ) $query->andFilterWhere([ 'status_id' => $statuses ]);
                if($groups != null ) $query->andFilterWhere([ 'group_id' => $groups ]);
                if($vacancies != null ) $query->andFilterWhere([ 'vacancy_id' => $vacancies ]);

                if($tags != null){                    
                    $resumesId = [];
                    foreach ($dataProvider->getModels() as $models) {
                        $modelsTag = json_decode($models->tags);
                        foreach ($tags as $value) {
                            foreach ($modelsTag as $tag_id) {
                                if($tag_id->id == $value) $resumesId [] = $models->id;
                            }
                        }
                    }
                    $model->count = count($resumesId);
                }
                else $model->count = $dataProvider->getTotalCount();
                $model->save();

                return $this->redirect(['/questionary/questions', 'id' => $model->questionary_id]);
                /*return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Оповещение",
                    'content'=>$this->renderAjax('view', [
                        'result' => $post,
                    ]),
                    'footer'=> Html::button('Ок',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create', 'questionary_id' => $questionary_id],['class'=>'btn btn-info','role'=>'modal-remote'])
        
                ];         */
            }else{           
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
        
                ];         
            }
        }else{
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Alert model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update Alert #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Alert #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Update Alert #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /*public function actionCount($questionary, $statuses, $groups, $vacancies, $tags = null )
    {
        $query = Resume::find()->where(['questionary_id' => $questionary]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $resultStatus = [];
        foreach ($statuses as $value) {
            $resultStatus [] = $value;
        }

        $resultGroups = [];
        foreach ($groups as $value) {
            $resultGroups [] = $value;
        }

        $resultVacancies = [];
        foreach ($vacancies as $value) {
            $resultVacancies [] = $value;
        }

        if($resultStatus != [] ) $query->andFilterWhere([ 'status_id' => $resultStatus ]);
        if($resultGroups != null ) $query->andFilterWhere([ 'group_id' => $resultGroups ]);
        if($resultVacancies != null ) $query->andFilterWhere([ 'vacancy_id' => $resultVacancies ]);

        return $dataProvider->getTotalCount();
    }*/

    /**
     * Delete an existing Alert model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Alert model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Alert model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Alert the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Alert::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
