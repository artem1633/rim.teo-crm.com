<?php

namespace app\controllers;

use Yii;
use app\models\Questions;
use app\models\QuestionsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Directory;
use yii\web\UploadedFile;

/**
 * QuestionsController implements the CRUD actions for Questions model.
 */
class QuestionsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    //Скачать картинки товара на сервер.
    public function actionUploadDocuments($number)
    {
        $uploadPath = 'uploads/questions/'.$number;

        if (isset($_FILES['documents'])) {
            $file = \yii\web\UploadedFile::getInstanceByName('documents');
            $original_name = $file->baseName;  
            $newFileName = $original_name.'.'.$file->extension;
            // you can write save code here before uploading.
            if ($file->saveAs($uploadPath . '/' . $newFileName)) {
            }
        }
    }

    // Удаление картинок товара из сервера.
    public function actionRemoveFile($id, $filename)
    {
        if (file_exists('uploads/questions/' . $id . '/' . $filename)) unlink(Yii::getAlias('uploads/questions/' . $id . '/' . $filename));
    }

    //Загружение картинки товара.
    public function actionSetDocuments($id, $doc)
    {
        $question = Questions::findOne($id);
        if($question != null)
        {
            $question->foto = $doc;
            $question->save();
        }
    }

    //Скачать все документы
    public function actionDownloadAllDocuments($id)
    {
        $task = Tasks::findOne($id);
        $zip = new \ZipArchive;
        unlink(Yii::getAlias('download.zip'));
        $download = 'download.zip';
        $zip->open($download, \ZipArchive::CREATE);

        $files = json_decode($task->files);
        foreach ($files as $file) {
            //download file
            $download_file = file_get_contents($file->path);
            //add it to the zip
            $zip->addFromString($file->name, $download_file);
        }

        $zip->close();
        header('Content-Type: application/zip');
        header("Content-Disposition: attachment; filename = $download");
        header('Content-Length: ' . filesize($download));
        header("Location: $download");

        \Yii::$app->response->sendFile('download.zip');
    }

    /**
     * Lists all Questions models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new QuestionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Questions model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Questions #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-info','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Questions model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($questionary_id)
    {
        $request = Yii::$app->request;
        $model = new Questions(); 
        $model->questionary_id = $questionary_id; 
        $model->type = 1;
        $number = Questions::getLastId(); 
        Directory::createQuestionsDirectory($number);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){

                $post = $request->post();
                if($model->type == 3){
                    $result = [];
                    $i = -1;
                    foreach ($post['individual'] as $value) {
                        $i++;
                        if($value != ''){
                            $result [] = [
                                'value' => $value,
                                'ball' => $post['ball_indiv'][$i]
                            ];
                        }
                    }
                    $model->individual = json_encode($result);
                }

                if($model->type == 4){
                    $result = [];
                    $i = -1;
                    foreach ($post['multiple'] as $value) {
                        $i++;
                        if($value != ''){
                            $result [] = [
                                'question' => $value,
                                'ball' => $post['ball_multiple'][$i]
                            ];
                        }
                    }
                    $model->multiple = json_encode($result);
                }


                $model->save();
                $model->file = UploadedFile::getInstance($model, 'file');
                if(!empty($model->file))
                {
                    $model->file->saveAs('uploads/questions/'. $number . '/' .$model->file->baseName.'.'.$model->file->extension);
                    Yii::$app->db->createCommand()->update('questions', ['foto' => $model->file->baseName.'.'.$model->file->extension], [ 'id' => $model->id ])->execute();
                }
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Вопросы",
                    'content'=>'<span class="text-success">Успешно выполнено</span>',
                    'footer'=> Html::button('Ок',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create', 'questionary_id' => $questionary_id],['class'=>'btn btn-info','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Добавить",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'number' => $number,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit",'data-introindex'=>'6-8'])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Questions model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $number = $id;
        Directory::createQuestionsDirectory($number);   

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){

                $post = $request->post();

                if($model->type == 3  && $post['individual'] != null){
                    $result = [];
                    $i = -1;
                    foreach ($post['individual'] as $value) {
                        $i++;
                        if($value != ''){
                            $result [] = [
                                'value' => $value,
                                'ball' => $post['ball_indiv'][$i]
                            ];
                        }
                    }
                    $model->individual = json_encode($result);
                }

                if($model->type == 4  && $post['multiple'] != null){
                    $result = [];
                    $i = -1;
                    foreach ($post['multiple'] as $value) {
                        $i++;
                        if($value != ''){
                            $result [] = [
                                'question' => $value,
                                'ball' => $post['ball_multiple'][$i]
                            ];
                        }
                    }
                    $model->multiple = json_encode($result);
                }

                $model->save();
                $model->file = UploadedFile::getInstance($model, 'file');
                if(!empty($model->file))
                {
                    $model->file->saveAs('uploads/questions/'. $number . '/' .$model->file->baseName.'.'.$model->file->extension);
                    Yii::$app->db->createCommand()->update('questions', ['foto' => $model->file->baseName.'.'.$model->file->extension], [ 'id' => $model->id ])->execute();
                }
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Вопросы",
                    'forceClose'=>true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'number' => $id,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-success','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Questions model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
    * Поменят порядок частей
    */
    public function actionSorting($questionary_id)
    {
        $request = Yii::$app->request;
        $list = Questions::getColumnsList($questionary_id);

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($request->post()){
            
            $i = 0;
            $result = [];
            $string = $request->post()['columnValues'];
            if($string != null && !is_array($string)){
                $result = explode(',', $string);
            }
            foreach ($result as $value) {
                $i++;
                $question = Questions::findOne($value);
                $question->ordering = $i;
                $question->save();
            }
            return [
                'forceReload'=>'#crud-datatable-pjax',
                'title'=> "Вопросы",
                'size' => 'normal', 
                'forceClose'=>true,
            ];         
        }else{           
            return [
                'title'=> "Сортировка вопросов",
                'size' => 'normal',
                'content'=>$this->renderAjax('_sorting_form', [
                    'list' => $list,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])        
            ];         
        }       
    }

    /**
     * Finds the Questions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Questions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Questions::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
