<?php

namespace app\controllers;

use app\models\LessonsUsers;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\RegisterForm;
use app\models\Users;
use app\models\Questionary;
use app\modules\api\controllers\BotinfoController;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
/*            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],*/
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) { 
            return $this->redirect(['/questionary/questions', 'user_id' => Yii::$app->user->identity->id]);
        }else
        {
            return $this->redirect(['site/login']);
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */

    public function actionAvtorizatsiya()
    {
        if(isset(Yii::$app->user->identity->id))
        {
            return $this->render('error');
        }        
        else
        {
            Yii::$app->user->logout();
            $this->redirect(['login']);
        }
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

//    public function actionTest()
//    {
//        $session = Yii::$app->session;
//
//        VarDumper::dump($session['answers'], 10, true);
//        exit;
//    }

    public function actionStartlearn()
    {
        $lessons = LessonsUsers::find()->where(['user_id'=>Yii::$app->user->id])->all();
        foreach ($lessons as $lesson) {
            $lesson->passed = 0;
            $lesson->save();
        }
        return $this->redirect(['/users/dashboard']);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        $this->redirect(['login']);
    }

    /**
     * Регистрирует нового пользователя
     * @return string|Response
     */
    public function actionRegister()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        //$this->layout = 'main-login';

        $model = new RegisterForm();
        $ref = null;
        if (isset($_GET['ref'])) $ref = $_GET['ref'];

        if($model->load(Yii::$app->request->post()) && $model->register($ref))
        {
            $text = 'Новый пользователь системы';
            if ($model->fio) $text .= "\n ФИО: ".$model->fio;
            if ($model->telephone) $text .= "\n Телефон: ".$model->telephone;
            BotinfoController::sendadmin(['chat_id' => '247187885', 'parse_mode'=>'HTML', 'text' => $text]);

            $login_model = new LoginForm();
            $login_model->username = $model->login;
            $login_model->password = $model->password;
            if ($login_model->login()) {
                Users::setDefaultValues();
                return $this->goBack();
            }
            else return $this->redirect(['login']);
        } else {
            //Yii::$app->runAction('botinfo/send', ['users' => 1, 'text' => 'asdasd']);
            return $this->render('register', [
                'model' => $model,
            ]);
        }
    }


    public function actionMenuPosition()
    {
        $session = Yii::$app->session;
        if($session['body'] == null | $session['body'] == 'small') $session['body'] = 'large';
        else $session['body'] = 'small';

        if($session['left'] == null | $session['left'] == 'small') $session['left'] = 'large';
        else $session['left'] = 'small';

    }
}
