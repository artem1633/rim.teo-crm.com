<?php

namespace app\controllers;

use app\models\CommandSearch;
use app\models\ResumeSearch;
use Yii;
use app\models\Users;
use app\models\UsersSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;
use kartik\mpdf\Pdf;
use app\models\Settings;
use app\models\User;
use app\models\additional\Permissions;
use app\models\Questionary;
use app\models\Resume;
use app\models\Chat;
use app\modules\api\controllers\BotinfoController;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'except' => ['dashboard-result'],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    public function actionDashboard()
    {
        $identity = Yii::$app->user->identity;
        $questionaryCount = Questionary::find()->count();

//        if($identity->type != 0) {
//            $resumeCount = Resume::find()
//                ->joinWith('questionary', true)
//                ->where(['questionary.user_id' => $identity->id ])
//                ->count();
//        }
//        else $resumeCount = Resume::find()->count();
//
//
//        if($identity->type != 0) {
//            $resumeTodayCount = Resume::find()
//                ->joinWith('questionary', true)
//                ->where(['questionary.user_id' => $identity->id ])
//                ->andWhere(['between', 'resume.date_cr', date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59') ])
//                ->count();
//        }
//        else {
//            $resumeTodayCount = Resume::find()
//                ->where(['between', 'resume.date_cr', date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59') ])
//                ->count();
//        }
//
//        $sendCount = 0;
//        if($identity->type != 0) {
//            $sendCount = Resume::find()
//                ->joinWith('questionary', true)
//                ->where(['connect_telegram' => 1 ])
//                ->andWhere(['questionary.user_id' => $identity->id ])
//                ->count();
//        }
//        else{
//            $sendCount = Resume::find()
//                ->where(['connect_telegram' => 1 ])
//                ->count();
//        }
//
//        $days = [];
//        $values = [];
//        for ($i = 1; $i <= date('t'); $i++) {
//            if($i < 10) $day = '0'.$i;
//            else $day = $i;
//            $days [] = $day.'.'.date('m');
//            $date = 'Y-' . date('m') . '-' . $day;
//            if($identity->type != 0) {
//                $count = Resume::find()
//                    ->joinWith('questionary', true)
//                    ->where(['between', 'resume.date_cr', date( $date . ' 00:00:00'), date( $date . ' 23:59:59') ])
//                    ->andWhere(['questionary.user_id' => $identity->id ])
//                    ->count();
//            }
//            else{
//                $count = Resume::find()
//                    ->where(['between', 'resume.date_cr', date( $date . ' 00:00:00'), date( $date . ' 23:59:59') ])
//                    ->count();
//            }
//            $values [] = $count;
//        }
//
//        $questionaries = Questionary::find()->where(['show_in_desktop' => 1])->all();


        $searchModel = new CommandSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy('balls desc');
//        $dataProvider->pagination = true;


        return $this->render('dashboard', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'questionaryCount' => $questionaryCount,
//            'resumeCount' => $resumeCount,
//            'resumeTodayCount' => $resumeTodayCount,
//            'sendCount' => $sendCount,
//            'days' => $days,
//            'values' => $values,
//            'questionaries' => $questionaries,
        ]);  
    }

    public function actionDashboardResult()
    {
        $searchModel = new CommandSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy('balls desc');
        $dataProvider->pagination = false;

        $this->layout = 'iframe';

        return $this->render('_dashboard_result', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionInstruction()
    {    
        $setting = Settings::find()->where(['key' => 'instruction_editor'])->one();

        return $this->render('instruction', [
            'setting' => $setting,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) 
    {   
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $chat = new Chat();
        $chat->chat_id = '#users-'.$id;

        if ( $request->post() ) {
            if($request->post()['text'] != ''){
                $chat->text = $request->post()['text'];
                $chat->save();
                if ($model->connect_to_telegram) {
                    BotinfoController::getReq('sendMessage', ['chat_id' => $model->telegram_id, 'parse_mode'=>'HTML', 'text' => $request->post()['text']]);
                }
                return $this->redirect(['/users/view', 'id' => $id]);
            }
        }

        $chatText = Chat::find()->where(['chat_id' => '#users-'.$id ])->all();

        return $this->render('view', [
            'model' => $model,
            'chatText' => $chatText,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewInTable($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "ФИО : " . $this->findModel($id)->fio,
                    'content'=>$this->renderAjax('view_in_table', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-info','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view_in_table', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Users model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Users();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'size' => 'normal',
                    'title'=> "Создать",
                    'content'=>'<span class="text-success">Успешно выпольнено</span>',
                    'footer'=> Html::button('Ок',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn-info','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Users model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            if($model->load($request->post()) && $model->save()) {
              return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];   
            }else{
                 return [
                    'title'=> $model->getTypeDescription(),
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Creates a new Users model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAvatar()
    {
        $request = Yii::$app->request;
        $id = Yii::$app->user->identity->id;
        $model = Users::findOne(Yii::$app->user->identity->id);  

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->validate()){

                $model->file = UploadedFile::getInstance($model, 'file');
                if(!empty($model->file)){
                    $model->file->saveAs('avatars/'.$id.'.'.$model->file->extension);
                    Yii::$app->db->createCommand()->update('users', ['foto' => $id.'.'.$model->file->extension], [ 'id' => $id ])->execute();
                }

                return [
                    'forceReload'=>'#profile-pjax',
                    'size' => 'normal',
                    'title'=> "Аватар",
                    'forceClose' => true,     
                ];         
            }else{           
                return [
                    'title'=> "Аватар",
                    'size' => 'small',
                    'content'=>$this->renderAjax('avatar', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
        
                ];         
            }
        }
       
    }

    public function actionChange($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                
                return [
                    'forceClose' => true,
                    'forceReload'=>'#profile-pjax',
                ];    
            }else{
                 return [
                    'title'=> "Изменить",
                    'size' => "large",
                    'content'=>$this->renderAjax('change', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                ];        
            }
        }
        else{
            echo "ddd";
        }
    }

    public function actionProfile()
    {
        $request = Yii::$app->request;
        return $this->render('profile', [
        ]);        
    }

    /**
     * Delete an existing Users model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        if($id != 1) $this->findModel($id)->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete multiple existing Vacancy model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            if($pk != 1) $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
