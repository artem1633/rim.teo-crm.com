<?php

use yii\db\Migration;

/**
 * Handles the creation of table `group`.
 */
class m181107_153120_create_group_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('group', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'color' => $this->string(10),
            'user_id' => $this->integer(),
        ]);

        $this->createIndex('idx-group-user_id', 'group', 'user_id', false);
        $this->addForeignKey("fk-group-user_id", "group", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-group-user_id','group');
        $this->dropIndex('idx-group-user_id','group');
        
        $this->dropTable('group');
    }
}
