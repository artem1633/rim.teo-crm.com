<?php

use yii\db\Migration;

/**
 * Handles the creation of table `resume_status`.
 */
class m181107_153208_create_resume_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('resume_status', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'icon' => $this->string(255),
            'user_id' => $this->integer(),
        ]);

        $this->createIndex('idx-resume_status-user_id', 'resume_status', 'user_id', false);
        $this->addForeignKey("fk-resume_status-user_id", "resume_status", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-resume_status-user_id','resume_status');
        $this->dropIndex('idx-resume_status-user_id','resume_status');
        
        $this->dropTable('resume_status');
    }
}
