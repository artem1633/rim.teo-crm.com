<?php

use yii\db\Migration;

/**
 * Handles the creation of table `vacancy`.
 */
class m181107_165917_create_vacancy_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('vacancy', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'user_id' => $this->integer(),
        ]);

        $this->createIndex('idx-vacancy-user_id', 'vacancy', 'user_id', false);
        $this->addForeignKey("fk-vacancy-user_id", "vacancy", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-vacancy-user_id','vacancy');
        $this->dropIndex('idx-vacancy-user_id','vacancy');
        
        $this->dropTable('vacancy');
    }
}
