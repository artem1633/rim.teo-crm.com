<?php

use yii\db\Migration;

/**
 * Handles the creation of table `questionary`.
 */
class m181108_050134_create_questionary_table extends Migration
{
    /**
     * {@inheritdoc} 
     */
    public function safeUp()
    {
        $this->createTable('questionary', [
            'id' => $this->primaryKey(),
            'active' => $this->boolean()->comment('Активность'),
            'name' => $this->string(255)->comment('Название'),
            'description' => $this->text()->comment('Описание'),
            'vacancy_id' => $this->integer()->comment('Вакансия'),
            'link' => $this->string(255)->comment('Короткая ссылка'),
            'user_id' => $this->integer()->comment('Компания'),
            'date_cr' => $this->datetime()->comment('Дата создание'),
            'date_up' => $this->datetime()->comment('Дата изменение'),
            'access' => $this->boolean()->comment('Доступ'),
            'show_in_desktop' => $this->boolean()->comment('Показывать информацию на рабочем столе'),
        ]);

        $this->createIndex('idx-questionary-vacancy_id', 'questionary', 'vacancy_id', false);
        $this->addForeignKey("fk-questionary-vacancy_id", "questionary", "vacancy_id", "vacancy", "id");

        $this->createIndex('idx-questionary-user_id', 'questionary', 'user_id', false);
        $this->addForeignKey("fk-questionary-user_id", "questionary", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-questionary-vacancy_id','questionary');
        $this->dropIndex('idx-questionary-vacancy_id','questionary');

        $this->dropForeignKey('fk-questionary-user_id','questionary');
        $this->dropIndex('idx-questionary-user_id','questionary');

        $this->dropTable('questionary');
    }
}
