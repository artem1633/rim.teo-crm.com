<?php

use yii\db\Migration;

/**
 * Handles the creation of table `resume`.
 */
class m181108_154547_create_resume_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('resume', [
            'id' => $this->primaryKey(),
            'questionary_id' => $this->integer()->comment('Анкета'),
            'group_id' => $this->integer()->comment('Группа'),
            'status_id' => $this->integer()->comment('Статус'),
            'special' => $this->string(255)->comment('Специальность'),
            'fit' => $this->boolean()->comment('Подходит'),
            'date_cr' => $this->datetime()->comment('Дата и время заполнения'),
            'category' => $this->integer()->comment('Список из анкет по категориям'),
            'is_new' => $this->boolean()->comment('Новая'),
            'mark' => $this->integer()->comment('Оценка Оценка'),
            'code' => $this->string(255)->comment('Уникальный код'),
            'telegram_chat_id' => $this->string(255)->comment('Ид чата телеграма'),
            'connect_telegram' => $this->boolean()->comment('Подключен к телеграмм'),
            'new_sms' => $this->integer()->comment('Новое сообщение'),
        ]);

        $this->createIndex('idx-resume-questionary_id', 'resume', 'questionary_id', false);
        $this->addForeignKey("fk-resume-questionary_id", "resume", "questionary_id", "questionary", "id");

        $this->createIndex('idx-resume-group_id', 'resume', 'group_id', false);
        $this->addForeignKey("fk-resume-group_id", "resume", "group_id", "group", "id");

        $this->createIndex('idx-resume-status_id', 'resume', 'status_id', false);
        $this->addForeignKey("fk-resume-status_id", "resume", "status_id", "resume_status", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-resume-questionary_id','resume');
        $this->dropIndex('idx-resume-questionary_id','resume');

        $this->dropForeignKey('fk-resume-group_id','resume');
        $this->dropIndex('idx-resume-group_id','resume');

        $this->dropForeignKey('fk-resume-status_id','resume');
        $this->dropIndex('idx-resume-status_id','resume');

        $this->dropTable('resume');
    }
}
