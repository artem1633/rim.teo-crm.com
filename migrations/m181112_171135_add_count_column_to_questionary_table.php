<?php

use yii\db\Migration;

/**
 * Handles adding count to table `questionary`.
 */
class m181112_171135_add_count_column_to_questionary_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('questionary', 'count', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('questionary', 'count');
    }
}
