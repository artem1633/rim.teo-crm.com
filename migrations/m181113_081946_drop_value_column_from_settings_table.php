<?php

use yii\db\Migration;

/**
 * Handles dropping value from table `settings`.
 */
class m181113_081946_drop_value_column_from_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('settings', 'value');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('settings', 'value', $this->string(255));
    }
}
