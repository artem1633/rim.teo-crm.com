<?php

use yii\db\Migration;

/**
 * Class m181114_130710_add_default_value
 */
class m181114_130710_add_default_value extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->insert('settings',array(
            'name' => 'Телеграм чат',
            'key' => 'telegram_chat',
            'text' =>'https://t.me/teo_job_bot',
        ));
    }

    public function down()
    {
        
    }
}
