<?php

use yii\db\Migration;

/**
 * Handles adding fio to table `resume`.
 */
class m181114_163600_add_fio_column_to_resume_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('resume', 'fio', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('resume', 'fio');
    }
}
