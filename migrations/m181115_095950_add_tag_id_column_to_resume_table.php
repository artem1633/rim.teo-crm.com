<?php

use yii\db\Migration;

/**
 * Handles adding tag_id to table `resume`.
 */
class m181115_095950_add_tag_id_column_to_resume_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('resume', 'tags', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('resume', 'tags');
    }
}
