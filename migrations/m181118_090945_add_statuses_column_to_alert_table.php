<?php

use yii\db\Migration;

/**
 * Handles adding statuses to table `alert`.
 */
class m181118_090945_add_statuses_column_to_alert_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('alert', 'statuses', $this->text());
        $this->addColumn('alert', 'groups', $this->text());
        $this->addColumn('alert', 'vacancies', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('alert', 'statuses');
        $this->dropColumn('alert', 'groups');
        $this->dropColumn('alert', 'vacancies');
    }
}
