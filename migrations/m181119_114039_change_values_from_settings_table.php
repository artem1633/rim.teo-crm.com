<?php

use yii\db\Migration;

/**
 * Class m181119_114039_change_values_from_settings_table
 */
class m181119_114039_change_values_from_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->update('settings', array(
            'text' => 'Для того чтобы получать уведомления вступите в телеграм бот @teo_job_info_bot и введите код "{unique_code_for_telegram}"'), 
            'id=3'
        );
    }

    public function down()
    {

    }
}
