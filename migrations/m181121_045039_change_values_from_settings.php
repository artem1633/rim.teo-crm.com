<?php

use yii\db\Migration;

/**
 * Class m181121_045039_change_values_from_settings
 */
class m181121_045039_change_values_from_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->update('settings', array(
            'text' => '<p style="text-align:center"><span style="font-size:28px"><strong>Краткое руководство по размещению теста на платформе TEO-Job</strong></span></p>

<p><strong>1. Регистрируемся на платформе</strong></p>

<p><strong><img alt="" src="http://joxi.ru/J2bxbjYUXbk7Zr" style="height:150px; margin-bottom:2px; margin-top:2px; width:200px" /><img alt="" src="http://dl4.joxi.net/drive/2018/11/21/0023/2488/1571256/56/a36310d537.png" style="height:538px; width:768px" /></strong></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><strong>2. Для создания теста нажимаем Список анкет</strong></p>

<p><img alt="" src="http://dl3.joxi.net/drive/2018/11/21/0023/2488/1571256/56/ca0bba5936.png" style="height:571px; width:695px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><strong>3. Жмем на кнопку добавить.</strong></p>

<p><img alt="" src="http://dl4.joxi.net/drive/2018/11/21/0023/2488/1571256/56/89f43ef1bf.png" style="height:599px; width:779px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><strong>4. Пишем название вакансии, а также <u>описание вакансии</u> и <u>требования к кандидату</u>.</strong></p>

<p><strong>Можно указать условия оплаты труда. Жмем сохранить.</strong></p>

<p><img alt="" src="http://dl4.joxi.net/drive/2018/11/21/0023/2488/1571256/56/401f475485.png" style="height:545px; width:679px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><strong>5. Попадаем в окно Список анкет и жмем на карандаш для добавления вопросов</strong></p>

<p><img alt="" src="http://dl4.joxi.net/drive/2018/11/21/0023/2488/1571256/56/0afec991bd.png" style="height:514px; width:779px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><strong>6. Добавляем первый вопрос</strong></p>

<p><img alt="" src="http://dl4.joxi.net/drive/2018/11/21/0023/2488/1571256/56/82171eab44.png" style="height:412px; width:776px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><strong>7. В выпадающем списке внизу можно выбрать тип ответа : Текст, Число, Выбор одного варианта, Выбор несколько вариантов, Дата.&nbsp; В вопросе о возрасте будет уместно поставить тип ответа &laquo;Число&raquo;</strong></p>

<p><strong><img alt="" src="http://dl3.joxi.net/drive/2018/11/21/0023/2488/1571256/56/7ba9742dd4.png" style="height:666px; width:584px" /></strong></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><strong>8. Сохраняем, жмем &laquo;Создать еще&raquo;. Следующий вопрос &laquo;Ваш пол&raquo; зададим в формате &laquo;выбор одного варианта&raquo; с помощью кнопки добавить заполняем &laquo;Муж&raquo; и &laquo;Жен&raquo;&nbsp;</strong></p>

<p><strong><img alt="" src="http://dl4.joxi.net/drive/2018/11/21/0023/2488/1571256/56/aac976f6db.png" style="height:453px; width:567px" /></strong></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><strong>9. В следующем вопросе, например, просим указать электронную почту, и ставим галочку &laquo;обязательное&raquo;</strong></p>

<p><strong><img alt="" src="http://dl4.joxi.net/drive/2018/11/21/0023/2488/1571256/56/e596ba5299.png" style="height:719px; width:613px" /></strong></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><strong>10. Подобными действиями заполняем тест различными вопросами. Список вопросов может быть примерно следующий:</strong></p>

<p><strong>Телефон?</strong></p>

<p><strong>Ссылка на соц сеть (вк, ок, instagram)?</strong></p>

<p><strong>Где проживаете (населенный пункт)?</strong></p>

<p><strong>Какими программами владеете?</strong></p>

<p><strong>Сколько часов в день готовы уделять работе?</strong></p>

<p><strong>Сейчас где-то работаете?</strong></p>

<p><strong>На какой период планируете удаленную работу?</strong></p>

<p><strong>Какая стоимость в час?</strong></p>

<p><strong>Какая должна быть оплата в месяц, менее которой вам не интересно работать?</strong></p>

<p><strong>Ваш оптимальный уровень заработной платы?</strong></p>

<p><strong>Когда готовы приступить к работе?</strong></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><strong>11.&nbsp; В итоге, ссылка на анкету будет отображаться здесь</strong></p>

<p><strong><img alt="" src="http://dl3.joxi.net/drive/2018/11/21/0023/2488/1571256/56/9bf9efb6cc.png" style="height:491px; width:781px" /></strong></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Ссылка на нашу Демо анкету:&nbsp;&nbsp;&nbsp; <a href="https://demo.teo-job.ru/y1IL4Uaj">https://demo.teo-job.ru/y1IL4Uaj</a></p>
'), 
            'id=4'
        );

    }

    public function down()
    {
        
    }
}
