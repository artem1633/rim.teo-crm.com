<?php

use yii\db\Migration;

/**
 * Handles adding publish_company to table `questionary`.
 */
class m181222_084541_add_publish_company_column_to_questionary_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('questionary', 'publish_company', $this->boolean()->comment('Публиковать на страницы компании'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('questionary', 'publish_company');
    }
}
