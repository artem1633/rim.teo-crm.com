<?php

use yii\db\Migration;

/**
 * Handles dropping ball_for_question from table `questionary`.
 */
class m181222_132648_drop_ball_for_question_column_from_questionary_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('questionary', 'ball_for_question');
        $this->addColumn('resume', 'ball_for_question', $this->integer()->comment('Баллы за текстовые ответы'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('questionary', 'ball_for_question', $this->integer()->comment('Баллы за текстовые ответы'));
        $this->dropColumn('resume', 'ball_for_question');
    }
}
