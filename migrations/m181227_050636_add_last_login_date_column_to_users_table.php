<?php

use yii\db\Migration;

/**
 * Handles adding last_login_date to table `users`.
 */
class m181227_050636_add_last_login_date_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'last_login_date', $this->datetime()->comment('Дата последнего захода'));
        $this->addColumn('users', 'new_sms', $this->boolean()->comment('Новое сообщение'));
        $this->addColumn('users', 'telegram_login', $this->string(255)->comment('Логин телеграма'));
        $this->addColumn('users', 'connect_to_telegram', $this->boolean()->comment('Подключен к телеграма'));
        $this->addColumn('users', 'register_date', $this->datetime()->comment('Дата регистрации'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'last_login_date');
        $this->dropColumn('users', 'new_sms');
        $this->dropColumn('users', 'telegram_login');
        $this->dropColumn('users', 'connect_to_telegram');
        $this->dropColumn('users', 'register_date');
    }
}
