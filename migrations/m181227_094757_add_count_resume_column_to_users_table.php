<?php

use yii\db\Migration;

/**
 * Handles adding count_resume to table `users`.
 */
class m181227_094757_add_count_resume_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'count_resume', $this->integer()->comment('Количество Резюме'));
        $this->addColumn('users', 'count_questionery', $this->integer()->comment('Количество анкеты'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'count_resume');
        $this->dropColumn('users', 'count_questionery');
    }
}
