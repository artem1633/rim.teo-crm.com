<?php

use yii\db\Migration;

/**
 * Handles dropping description from table `users`.
 */
class m181227_155731_drop_description_column_from_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('users', 'description');
        $this->addColumn('users', 'description', $this->binary()->comment('Описание'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'description');
        $this->addColumn('users', 'description', $this->text());
    }
}
