<?php

use yii\db\Migration;

/**
 * Handles adding avatar to table `resume`.
 */
class m181228_183950_add_avatar_column_to_resume_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('resume', 'avatar', $this->string(255)->comment('Аватар'));
        $this->addColumn('questions', 'avatar', $this->string(255)->comment('Аватар'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('resume', 'avatar');
        $this->dropColumn('questions', 'avatar');
    }
}
