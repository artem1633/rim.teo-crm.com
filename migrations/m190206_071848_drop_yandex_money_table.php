<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `yandex_money`.
 */
class m190206_071848_drop_yandex_money_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('yandex_money');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('yandex_money', [
            'id' => $this->primaryKey(),
            'secret' =>  $this->string(250)->null()->comment('Секрет'),
            'wallet' => $this->string(20)->null()->comment('Кошелек'),
        ], $tableOptions);

        $this->insert('yandex_money',array(
            'secret' => 'secret',          
            'wallet' => 'wallet',
        ));
    }
}
