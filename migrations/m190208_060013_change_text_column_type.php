<?php

use yii\db\Migration;

/**
 * Class m190208_060013_change_text_column_type
 */
class m190208_060013_change_text_column_type extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->alterColumn('sliders', 'text', 'binary');//timestamp new_data_type
    }

    public function down()
    {
        $this->alterColumn('sliders','text', 'text' );//int is old_data_type
    }
}
