<?php

use yii\db\Migration;

/**
 * Handles dropping passage_ball from table `questionary`.
 */
class m190329_100009_drop_passage_ball_column_from_questionary_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('questionary', 'passage_ball');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('questionary', 'passage_ball', $this->float());
    }
}
