<?php

use yii\db\Migration;

/**
 * Handles adding own_option to table `questions`.
 */
class m190331_055919_add_own_option_column_to_questions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('questions', 'own_option', $this->boolean()->comment('Свой вариант'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('questions', 'own_option');
    }
}
