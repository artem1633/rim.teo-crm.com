<?php

use yii\db\Migration;

/**
 * Handles adding balls to table `questions`.
 */
class m200515_150412_add_balls_column_to_questions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('questions', 'balls', $this->integer()->comment('Баллы'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('questions', 'balls');
    }
}
