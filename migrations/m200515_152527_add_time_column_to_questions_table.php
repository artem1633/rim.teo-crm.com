<?php

use yii\db\Migration;

/**
 * Handles adding time to table `questions`.
 */
class m200515_152527_add_time_column_to_questions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('questions', 'time', $this->integer()->comment('Время (в секундах)'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('questions', 'time');
    }
}
