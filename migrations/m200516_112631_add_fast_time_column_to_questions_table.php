<?php

use yii\db\Migration;

/**
 * Handles adding fast_time to table `questions`.
 */
class m200516_112631_add_fast_time_column_to_questions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('questions', 'fast_time', $this->integer()->comment('Время быстрого ответа'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('questions', 'fast_time');
    }
}
