<?php

use yii\db\Migration;

/**
 * Handles adding fast_balls to table `questions`.
 */
class m200516_112641_add_fast_balls_column_to_questions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('questions', 'fast_balls', $this->integer()->comment('Баллы за быстрый ответ'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('questions', 'fast_balls');
    }
}
