<?php

use yii\db\Migration;

/**
 * Handles adding command_specify to table `questions`.
 */
class m200518_100420_add_command_specify_column_to_questions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('questions', 'command_specify', $this->boolean()->defaultValue(false)->comment('Вопрос определяющий команду'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('questions', 'command_specify');
    }
}
