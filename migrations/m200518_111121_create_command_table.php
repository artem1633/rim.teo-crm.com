<?php

use yii\db\Migration;

/**
 * Handles the creation of table `command`.
 */
class m200518_111121_create_command_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('command', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'balls' => $this->integer()->defaultValue(0)->comment('Баллы'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('command');
    }
}
