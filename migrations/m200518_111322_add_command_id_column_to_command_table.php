<?php

use yii\db\Migration;

/**
 * Handles adding command_id to table `command`.
 */
class m200518_111322_add_command_id_column_to_command_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('resume', 'command_id', $this->integer()->after('id')->comment('Команда'));

        $this->createIndex(
            'idx-resume-command_id',
            'resume',
            'command_id'
        );

        $this->addForeignKey(
            'fk-resume-command_id',
            'resume',
            'command_id',
            'command',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-resume-command_id',
            'resume'
        );

        $this->dropIndex(
            'idx-resume-command_id',
            'resume'
        );

        $this->dropColumn('resume', 'command_id');
    }
}
