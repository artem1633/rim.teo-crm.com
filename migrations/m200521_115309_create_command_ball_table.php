<?php

use yii\db\Migration;

/**
 * Handles the creation of table `command_ball`.
 */
class m200521_115309_create_command_ball_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('command_ball', [
            'id' => $this->primaryKey(),
            'command_id' => $this->integer()->comment('Команда'),
            'balls' => $this->integer()->comment('Баллы'),
            'comment' => $this->text()->comment('Комменатрий'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-command_ball-command_id',
            'command_ball',
            'command_id'
        );

        $this->addForeignKey(
            'fk-command_ball-command_id',
            'command_ball',
            'command_id',
            'command',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-command_ball-command_id',
            'command_ball'
        );

        $this->dropIndex(
            'idx-command_ball-command_id',
            'command_ball'
        );

        $this->dropTable('command_ball');
    }
}
