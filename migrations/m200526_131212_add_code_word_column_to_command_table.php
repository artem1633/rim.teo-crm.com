<?php

use yii\db\Migration;

/**
 * Handles adding code_word to table `command`.
 */
class m200526_131212_add_code_word_column_to_command_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('command', 'code_word', $this->string()->comment('Кодовое слово'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('command', 'code_word');
    }
}
