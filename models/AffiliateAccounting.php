<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "affiliate_accounting".
 *
 * @property int $id
 * @property string $date_transaction Дата и время зачисления
 * @property int $company_id ID компании
 * @property int $referal_id ID реферала
 * @property string $amount Сумма партнерских отчислений
 */
class AffiliateAccounting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'affiliate_accounting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_transaction'], 'safe'],
            [['company_id'], 'required'],
            [['company_id', 'referal_id'], 'integer'],
            [['amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_transaction' => 'Дата зачисления',
            'company_id' => 'Компания',
            'referal_id' => 'Реферал',
            'amount' => 'Сумма',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    /*public function getCompanyModel()
    {
        return $this->hasOne(Users::className(), ['id' => 'company_id']);
    }*/


    /**
     * @return \yii\db\ActiveQuery
     */
    /*public function getReferalModel()
    {
        return $this->hasOne(Companies::className(), ['id' => 'referal_id']);
    }*/

}
