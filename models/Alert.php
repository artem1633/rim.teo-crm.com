<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "alert".
 *
 * @property int $id
 * @property int $questionary_id Анкета
 * @property string $name Наименование
 * @property double $count Количество
 * @property string $text Текст
 * @property int $status Статус
 * @property string $send_date Дата и время отправки
 *
 * @property Questionary $questionary
 */
class Alert extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alert';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['questionary_id', 'status'], 'integer'],
            [['name', 'text'], 'required'],
            [['count'], 'number'],
            [['text'/*, 'statuses', 'groups', 'vacancies'*/], 'string'],
            [['send_date'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['questionary_id'], 'exist', 'skipOnError' => true, 'targetClass' => Questionary::className(), 'targetAttribute' => ['questionary_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'questionary_id' => 'Анкета',
            'name' => 'Наименование',
            'count' => 'Количество',
            'text' => 'Текст',
            'status' => 'Статус',
            'send_date' => 'Дата и время отправки',
            'statuses' => 'Статусы',
            'groups' => 'Группы',
            'vacancies' => 'Специальность',
            'tags' => 'Теги',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->status = 0;
            $this->send_date = date('Y-m-d H:i:s');
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionary()
    {
        return $this->hasOne(Questionary::className(), ['id' => 'questionary_id']);
    }

    public function getStatusList()
    {
        return [
            1 => 'Отправлено',
            0 => 'В работе',
        ];
    }

    public function getStatusDescription()
    {
        if($this->status == 1) return "Отправлено";
        else return "В работе";
    }

    public function getQuestionaryList()
    {
        $questionary = Questionary::find()->all();
        return ArrayHelper::map( $questionary , 'id', 'name');
    }

    public function getGroupList()
    {
        $group = Group::find()->all();
        return ArrayHelper::map( $group , 'id', 'name');
    }

    public function getResumeStatusList()
    {
        $status = ResumeStatus::find()->all();
        return ArrayHelper::map( $status , 'id', 'name');
    }

    public function getVacancyList()
    {
        $vacancy = Vacancy::find()->all();
        return ArrayHelper::map( $vacancy , 'id', 'name');
    }

    public function getTagsList()
    {
        $tags = Tags::find()->all();
        return ArrayHelper::map( $tags , 'id', 'name');
    }
}
