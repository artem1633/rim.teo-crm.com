<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "command".
 *
 * @property int $id
 * @property string $name Наименование
 * @property int $balls Баллы
 *
 * @property Resume[] $resumes
 */
class Command extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'command';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['balls'], 'integer'],
            [['name', 'code_word'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'balls' => 'Баллы',
            'code_word' => 'Фамилия капитана',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResumes()
    {
        return $this->hasMany(Resume::className(), ['command_id' => 'id']);
    }
}
