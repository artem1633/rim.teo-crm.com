<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CommandBall;

/**
 * CommandBallSearch represents the model behind the search form about `app\models\CommandBall`.
 */
class CommandBallSearch extends CommandBall
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'balls'], 'integer'],
            [['comment', 'command_id', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CommandBall::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'command_id' => $this->command_id,
            'balls' => $this->balls,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        if($this->created_at != null){
            $date = explode(' - ', $this->created_at);
            $query->andWhere(['between', 'created_at', $date[0], $date[1]]);
        }

        return $dataProvider;
    }
}
