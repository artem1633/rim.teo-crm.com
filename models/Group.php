<?php

namespace app\models;

use Yii;
use app\base\AppActiveQuery;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "group".
 *
 * @property int $id
 * @property string $name
 * @property string $color
 */
class Group extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'group';
    }

    public static function find()
    {
        if(Yii::$app->user->identity->type != 0){
            $user_id = Yii::$app->user->identity->id;
        } else {
            $user_id = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'user_id' => $user_id,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false) {
            if(Yii::$app->user->identity->type !== 0)
            {
                $user_id = Yii::$app->user->identity->id;
                if($model->user_id != $user_id){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['user_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['color'], 'string', 'max' => 10],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'color' => 'Модуль цвета',
            'user_id' => 'Компания',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->user_id = Yii::$app->user->identity->id;
        }

        return parent::beforeSave($insert);
    }

    public function setDefaultValues()
    {
        $group = new Group();
        $group->name = 'Основной состав';
        $group->color = '#00bc71';
        $group->save();

        $group = new Group();
        $group->name = 'Дополнительный';
        $group->color = '#ff6d00';
        $group->save();

        $group = new Group();
        $group->name = 'Возможно';
        $group->color = '#edd94b';
        $group->save();

        $group = new Group();
        $group->name = 'Не подходит';
        $group->color = '#ff0000';
        $group->save();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResumes()
    {
        return $this->hasMany(Resume::className(), ['group_id' => 'id']);
    }

}
