<?php

namespace app\models;

use Yii; 
use app\base\AppActiveQuery;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
 
/**
 * This is the model class for table "resume".
 *
 * @property int $id
 * @property int $questionary_id Анкета
 * @property int $group_id Группа
 * @property int $status_id Статус
 * @property int $fit Подходит
 * @property string $date_cr Дата и время заполнения
 * @property int $category Список из анкет по категориям
 * @property int $is_new Новая
 * @property int $mark Оценка Оценка
 * @property string $code Уникальный код
 * @property string $telegram_chat_id Ид чата телеграма
 * @property int $connect_telegram Подключен к телеграмм
 * @property int $new_sms Новое сообщение
 * @property int $vacancy_id
 * @property string $values
 * @property int $correspondence
 * @property string $fio
 * @property string $tags
 * @property int $balls Баллы за ответы
 * @property int $ball_for_question Баллы за текстовые ответы
 * @property string $avatar Аватар
 * @property string $ip IP адрес
 *
 * @property Group $group
 * @property Questionary $questionary
 * @property ResumeStatus $status
 * @property Vacancy $vacancy
 */
class Resume extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'resume';
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false) {
            if(Yii::$app->user->identity->type !== 0)
            {
                $user_id = Yii::$app->user->identity->id;
                if($model->questionary->user_id != $user_id && $model->user_id != $user_id){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public $newQuestionary;
    public $newLink;
    public function rules()
    {
        return [
            [['questionary_id', 'fit', 'category', 'is_new', 'mark', 'connect_telegram', 'new_sms', 'correspondence', 'balls', 'ball_for_question', 'time_spent', 'delivered', 'show_in_shop', 'user_id', 'buyed', 'doptest', 'newQuestionary', 'command_id'], 'integer'],
            [['date_cr'], 'safe'],
            [['values'], 'string'],
            [['code'], 'unique'],
            [['sales'], 'number'],
            [['code', 'telegram_chat_id', 'fio', 'avatar', 'ip'], 'string', 'max' => 255],
            [['command_id'], 'exist', 'skipOnError' => true, 'targetClass' => Command::class, 'targetAttribute' => ['command_id' => 'id']],
            [['questionary_id'], 'exist', 'skipOnError' => true, 'targetClass' => Questionary::class, 'targetAttribute' => ['questionary_id' => 'id']],
            ['group_id', 'validateGroup'],
            ['status_id', 'validateStatus'],
            ['vacancy_id', 'validatVacancy'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    { 
        return [ 
            'id' => 'ID',
            'questionary_id' => 'Тест',
            'group_id' => 'Группа',
            'status_id' => 'Статус',
            'fit' => 'Подходит',
            'date_cr' => 'Дата и время заполнения',
            'category' => 'Список из анкет по категориям',
            'is_new' => 'Новая',
            'mark' => 'Оценка',
            'code' => 'Поделить тест',
            'telegram_chat_id' => 'Ид чата телеграма',
            'connect_telegram' => 'Телеграмм',
            'new_sms' => 'Новое сообщение',
            'vacancy_id' => 'Специальность',
            'values' => 'Значение',
            'correspondence' => 'Переписка',
            'fio' => 'ФИО',
            'tags' => 'Теги',
            'balls' => 'Баллы за ответы',
            'ball_for_question' => 'Дополнительные баллы',
            'avatar' => 'Аватар',
            'ip' => 'Ip адрес',
            'time_spent' => 'Время которое ушло на тест',
            'delivered' => 'Сдан или нет',
            'show_in_shop' => 'Не отоброжать в магазине',
            'sales' => 'Продано',
            'user_id' => 'Пользовател / Компания',
            'buyed' => 'Куплено',
            'doptest' => 'Доп. тест',
            'newQuestionary' => 'Тесты',
            'newLink' => 'Ссылка новая теста',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->date_cr = date('Y-m-d H:i:s');
            $this->connect_telegram = 0;

            $length = 10;
            $chars ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

            do {
                $string ='';
                for( $i = 0; $i < $length; $i++) {
                    $string .= $chars[rand(0,strlen($chars)-1)];
                }
            } while ( Resume::find()->where(['code' => $string])->one() != null );

            $this->code = $string;

        }

        return parent::beforeSave($insert);
    }

    public function getColorCode()
    {
        $length = 6;
        $chars ="abcdef1234567890";
        $string ='';
        for( $i = 0; $i < $length; $i++) {
            $string .= $chars[rand(0,strlen($chars)-1)];
        }
        return '#' . $string;
    }

    public function getTelegram()
    {
        return [
            1 => 'Да',
            0 => 'Нет',
        ];
    }

     public function getFit()
    {
        return [
            1 => 'Да',
            0 => 'Нет',
        ];
    }

    public function getTelegramDescription()
    {
        if($this->connect_telegram == 1) return 'Да';
        else return 'Нет';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommand()
    {
        return $this->hasOne(Command::class, ['id' => 'command_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::class, ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionary()
    {
        return $this->hasOne(Questionary::class, ['id' => 'questionary_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(ResumeStatus::class, ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(Tags::class, ['id' => 'tag_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVacancy()
    {
        return $this->hasOne(Vacancy::class, ['id' => 'vacancy_id']);
    }

    /**
     * @return array
     */
    public static function getCommandsList()
    {
        $names = ArrayHelper::getColumn(self::find()->where(['!=', 'fio', ''])->groupBy('fio')->all(), 'fio');
        return array_combine($names, $names);
    }

    //Получить описание типов пользователя.
    public function getFitDescription()
    {
        switch ($this->fit) {
            case 1: return "Да";
            case 0: return "Нет";
            default: return "<i style='color:red'>(Не задано)</i>";
        }
    }

    //Получить описание типов пользователя.
    public function getNewDescription()
    {
        switch ($this->is_new) {
            case 1: return "Да";
            case 0: return "Нет";
            default: return "<i style='color:red'>(Не задано)</i>";
        }
    }

    //Получить описание типов пользователя.
    public function getConnectedDescription()
    {
        switch ($this->connect_telegram) {
            case 1: return "Да";
            case 0: return "Нет";
            default: return "<i style='color:red'>(Не задано)</i>";
        }
    }

    //Получить описание типов пользователя.
    public function getNewSmsDescription()
    {
        switch ($this->new_sms) {
            case 1: return "Да";
            case 0: return "Нет";
            default: return "<i style='color:red'>(Не задано)</i>";
        }
    }

    //Получить описание типов пользователя.
    public function getCorrespondence()
    {
        switch ($this->correspondence) {
            case 1: return "Да";
            case 0: return "Нет";
            default: return "<i style='color:red'>(Не задано)</i>";
        }
    }

    //Получить описание типов пользователя.
    public function getMarkDescription()
    {
        switch ($this->mark) {
            case 0: return "0";
            case 1: return "1";
            case 2: return "2";
            case 3: return "3";
            case 4: return "4";
            case 5: return "5";
            case 6: return "6";
            case 7: return "7";
            case 8: return "8";
            case 9: return "9";
            case 10: return "10";
            default: return "<i style='color:red'>(Не задано)</i>";
        }
    }

    public function getGroupList()
    {
        $group = Group::find()->all();
        return ArrayHelper::map($group, 'id', 'name');
    }

    public function getStatusList()
    {
        $status = ResumeStatus::find()->all();
        return ArrayHelper::map($status, 'id', 'name');
    }

    public function getVacancyList()
    {
        $vacancy = Vacancy::find()->all();
        return ArrayHelper::map($vacancy, 'id', 'name');
    }

    public function getTagsList()
    {
        $tags = Tags::find()->all();
        return ArrayHelper::map($tags, 'id', 'name');
    }

    public function getQuestionaryList()
    {
        $questionary = Questionary::find()->all();
        return ArrayHelper::map($questionary, 'id', 'name');
    }

    public function getResumeList()
    {
        $resume = Resume::find()->all();
        $result = [];
        foreach ($resume as $value) {
            $result [] = [
                'id' => $value->id,
                'title' => 'ФИО=' . $value->fio . ', ID=' . $value->id,
            ];
        }

        return ArrayHelper::map($result, 'id', 'title');
    }

    public function getFitList()
    {
        return [
            0 => 'Нет',
            1 => 'Да',
        ];
    }

    public function getMarkList()
    {
        return [
            0 => 0,
            1 => 1,
            2 => 2,
            3 => 3,
            4 => 4,
            5 => 5,
            6 => 6,
            7 => 7,
            8 => 8,
            9 => 9,
            10 => 10,
        ];
    }

    public function getConnectList()
    {
        return [
            0 => 'Нет',
            1 => 'Да',
        ];
    }

    public function getNewSmsList()
    {
        return [
            0 => 'Нет',
            1 => 'Да',
        ];
    }

    public function getCorrespondenceList()
    {
        return [
            0 => 'Нет',
            1 => 'Да',
        ];
    }

    //Новый тег
    /*public function validateTags($attribute, $params)
    {
        $result = [];
        foreach ($this->tags as $value) {
            $tag = Tags::findOne($value);
            if($tag == null){
                $tag = new Tags();
                $tag->name = $value;
                
                if ($tag->save()) $result [] = [
                    'id' => $tag->id,
                ];
            }else{
                $result [] = [
                    'id' => $tag->id,
                ];
            }
        }

        //$this->tags = json_encode($result);
    }*/

    //Новая группа
    public function validateGroup($attribute, $params)
    {
        $group = Group::find()->where(['id' => $this->group_id])->one();
        if (!isset($group))
        {
            $group = new Group();
            $group->name = $this->group_id;
            
            if ($group->save())
            {
                $this->group_id = $group->id;
            }
            else
            {
                $this->addError($attribute,"Не создано новая группа");
            }
        }
    }

    //Новый статус
    public function validateStatus($attribute, $params)
    {
        $status = ResumeStatus::find()->where(['id' => $this->status_id])->one();
        if (!isset($status))
        {
            $status = new ResumeStatus();
            $status->name = $this->status_id;
            
            if ($status->save())
            {
                $this->status_id = $status->id;
            }
            else
            {
                $this->addError($attribute,"Не создано новый статус");
            }
        }
    }

    //Новая вакансия
    public function validatVacancy($attribute, $params)
    {
        $vacancy = Vacancy::find()->where(['id' => $this->vacancy_id])->one();
        if (!isset($vacancy))
        {
            $vacancy = new Vacancy();
            $vacancy->name = $this->vacancy_id;
            
            if ($vacancy->save())
            {
                $this->vacancy_id = $vacancy->id;
            }
            else
            {
                $this->addError($attribute,"Не создано новая вакансия");
            }
        }
    }

    public function validateTrue($string)
    {
        if( $string =='да' || $string == 'ДА' || $string == 'Да' || $string == 'дА') return 1;
        if( $string =='НЕТ' || $string == 'НЕт' || $string == 'Нет' || $string == 'НеТ' || $string == 'нЕТ' || $string == 'неТ' || $string == 'неТ' || $string == 'нЕт' || $string == 'нет' || $string == 'не' || $string == 'Не' || $string == 'нЕ' || $string == 'НЕ') return 0;
        return $string;
    }

    public function getTime()
    {
        $min = (int)($this->time_spent / 60);
        $sek = $this->time_spent - $min * 60;
        return $min . ' мин. ' . $sek . ' сек.';
    }
}
