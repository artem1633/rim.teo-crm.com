<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "setting_result".
 *
 * @property int $id
 * @property int $questionary_id Анкета/Тест
 * @property int $condition Условия
 * @property double $first_value Значения1
 * @property double $second_value Значения2
 * @property string $text Описания
 *
 * @property Questionary $questionary
 */
class SettingResult extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'setting_result';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['questionary_id', 'condition'], 'integer'],
            [['first_value', 'second_value'], 'number'],
            [['text'], 'string'],
            [['questionary_id'], 'exist', 'skipOnError' => true, 'targetClass' => Questionary::className(), 'targetAttribute' => ['questionary_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'questionary_id' => 'Анкета/Тест',
            'condition' => 'Условия',
            'first_value' => 'Значения',
            'second_value' => 'Значения2',
            'text' => 'Описания',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionary()
    {
        return $this->hasOne(Questionary::className(), ['id' => 'questionary_id']);
    }

    public function getConditionList()
    {
        return [
            1 => 'Больше (>)',
            2 => 'Больше или равно (>=)',
            3 => 'Меньше (<)',
            4 => 'Меньше или равно (<=)',
            5 => 'Между (< и < )',
            6 => 'Между (<= и <=)',
            7 => 'Между (<= и < )',
            8 => 'Между (< и <= )',
            9 => 'Равно (=)',
        ];
    }

    public function getDescription()
    {
        if($this->condition == 1) return 'Больше (>)';
        if($this->condition == 2) return 'Больше или равно (>=)';
        if($this->condition == 3) return 'Меньше (<)';
        if($this->condition == 4) return 'Меньше или равно (<=)';
        if($this->condition == 5) return 'Между (< и < )';
        if($this->condition == 6) return 'Между (<= и <=)';
        if($this->condition == 7) return 'Между (<= и < )';
        if($this->condition == 8) return 'Между (< и <= )';
        if($this->condition == 9) return 'Равно (=)';
    }

    public function findShowing()
    {
        if($this->condition === null) return true;
        if($this->condition == 1 || $this->condition == 2 || $this->condition == 3 || $this->condition == 4 || $this->condition == 9) return true;
        return false;
    }
}
