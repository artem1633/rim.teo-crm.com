<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use app\models\Products;
use app\models\Applications;
use app\models\additional\Common;
use app\models\additional\Columns;
use app\models\additional\Tariffs;
use app\models\additional\Permissions;
use app\models\additional\LegalInformation;
use app\models\Questionary;
use app\models\Resume;


/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $fio
 * @property string $login
 * @property string $password
 * @property string $telephone
 * @property int $type
 * @property string $telegram_id
 * @property string $foto Фото
 * @property string $utm UTM
 * @property int $agree Я согласен с обработкой персональных данных
 * @property string $unique_code_for_telegram
 * @property string $last_login_date Дата последнего захода
 * @property int $new_sms Новое сообщение
 * @property string $telegram_login Логин телеграма
 * @property int $connect_to_telegram Подключен к телеграма
 * @property string $register_date Дата регистрации
 * @property int $count_resume Количество Резюме
 * @property int $count_questionery Количество анкеты
 * @property resource $description Описание
 * @property double $main_balance Баланс основной
 * @property double $partner_balance Баланс партнерский
 * @property int $referal_id Реферал
 * @property double $prosent_referal Процент с оплат рефералов
 * @property double $questionary_sum Стоимость 1 анкеты
 * @property double $resume_sum Стоимость 1 резюме
 *
 * @property Chat[] $chats
 * @property Group[] $groups
 * @property Notification[] $notifications
 * @property Questionary[] $questionaries
 * @property ReferalRedirects[] $referalRedirects
 * @property ResumeStatus[] $resumeStatuses
 * @property Tags[] $tags
 * @property Vacancy[] $vacancies
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $new_password;
    public $file;
    public $page;

    const SUPER_COMPANY = '0';
    const COMPANY = '1';

    public static function tableName()
    {
        return 'users';
    }

    public function rules()
    {
        return [
            [['fio', 'login', 'password', 'type'], 'required'],
            [['type', 'agree', 'new_sms', 'connect_to_telegram','count_resume', 'count_questionery','referal_id'], 'integer'],
             [['last_login_date', 'register_date'], 'safe'],
            [['description'], 'string'],
            [['main_balance', 'partner_balance', 'prosent_referal', 'questionary_sum', 'resume_sum'], 'number'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg',], 
            [['fio', 'login', 'password', 'new_password', 'telegram_id', 'telephone', 'foto', 'utm', 'unique_code_for_telegram','telegram_login'], 'string', 'max' => 255],
        ];
    } 

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'login' => 'Логин',
            'password' => 'Пароль',
            'type' => 'Тип компании',
            'telegram_id' => 'Id чат телеграма',
            'new_password' => 'Новый пароль',
            'telephone' => 'Телефон',
            'foto' => 'Аватар',
            'file' => 'Аватар',
            'utm' => 'UTM',
            'agree' => 'Я согласен с обработкой персональных данных',
            'unique_code_for_telegram' => 'Уникальный код',
            'description' => 'Описание компании',
            'last_login_date' => 'Дата последнего захода',
            'new_sms' => 'Новое сообщение',
            'telegram_login' => 'Логин телеграма',
            'connect_to_telegram' => 'Подключен к телеграма',
            'register_date' => 'Дата регистрации',
            'count_resume' => 'Количество Резюме',
            'count_questionery' => 'Количество анкеты',
            'main_balance' => 'Баланс основной',
            'partner_balance' => 'Баланс партнерский',
            'referal_id' => 'Реферал',
            'prosent_referal' => 'Процент с оплат рефералов',
            'questionary_sum' => 'Стоимость 1 анкеты',
            'resume_sum' => 'Стоимость 1 резюме',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->password = Yii::$app->security->generatePasswordHash($this->password);
            
            $length = 20;
            $chars ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

            do {
                $string ='';
                for( $i = 0; $i < $length; $i++) {
                    $string .= $chars[rand(0,strlen($chars)-1)];
                }
            } while ( Users::find()->where(['utm' => $string])->one() != null );

            $this->utm = $string;

            do {
                $string ='';
                for( $i = 0; $i < $length; $i++) {
                    $string .= $chars[rand(0,strlen($chars)-1)];
                }
            } while ( Users::find()->where(['unique_code_for_telegram' => $string])->one() != null );

            $this->unique_code_for_telegram = $string;
        }

        if($this->new_password != null) {
            $this->password = Yii::$app->security->generatePasswordHash($this->new_password);
        }

            if($this->isNewRecord)
          {
            $this->register_date = date("Y-m-d H:i:s");
          }

       
            return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {        
        parent::afterSave($insert, $changedAttributes);
    }

    

    public function beforeDelete()
    {
        $notifications = Notification::find()->where(['user_id' => $this->id])->all();
        foreach ($notifications as $value) {
            $value->delete();
        }

        $chats = Chat::find()->where(['user_id' => $this->id])->all();
        foreach ($chats as $value) {
            $value->delete();
        }

        $group = Group::find()->where(['user_id' => $this->id])->all();
        foreach ($group as $value) {
            $value->delete();
        }

        $vacancy = Vacancy::find()->where(['user_id' => $this->id])->all();
        foreach ($vacancy as $value) {
            $value->delete();
        }
        
        $questionary = Questionary::find()->where(['user_id' => $this->id])->all();
        foreach ($questionary as $value) {
            $value->delete();
        }

        $status = ResumeStatus::find()->where(['user_id' => $this->id])->all();
        foreach ($status as $value) {
            $value->delete();
        }

        $tags = Tags::find()->where(['user_id' => $this->id])->all();
        foreach ($tags as $value) {
            $value->delete();
        }

       



        return parent::beforeDelete();
    }

    public function setDefaultValues()
    {
        $notification = new Notification();
        $notification->name = 'При новом посещение';
        $notification->key = 'visiting';
        $notification->value = '';
        $notification->save();

        $notification = new Notification();
        $notification->name = 'При новом заполнении';
        $notification->key = 'filling';
        $notification->value = '';
        $notification->save();

        $notification = new Notification();
        $notification->name = 'Новости и акция';
        $notification->key = 'news_and_action';
        $notification->value = '';
        $notification->save();

        $notification = new Notification();
        $notification->name = 'Сменили группу';
        $notification->key = 'change_group';
        $notification->value = '';
        $notification->save();

        Vacancy::setDefaultValues();
        Group::setDefaultValues();
        ResumeStatus::setDefaultValues();
    }

    //Получить список типов пользователя.
    public function getType()
    {
        return [
            self::SUPER_COMPANY => 'Супер компания',
            self::COMPANY => 'Компания',
        ];
    }

    //Получить описание типов пользователя.
    public function getTypeDescription()
    {
        switch ($this->type) {
            case 0: return "Супер компания";
            case 1: return "Компания";
            default: return "Неизвестно";
        }
    }

     public function getNewSmsList()
    {
        return [
            1 => 'Да',
            0 => 'Нет',
        ];
    }

     public function getNewSmsDescription()
    {
        if($this->new_sms == 1) return 'Да';
        else return 'Нет';
    }

  public function getConnectTelegramDescription()
    {
        if($this->connect_to_telegram == 1) return 'Да';
        else return 'Нет';
    }
    public function getPermission()
    {
        if($this->type == 0) return "Супер компания";
        else return "Компания";
    }

    //Список тарифов.
    public function getTariffList()
    {
        $tariffs = Tariffs::find()->all();
        return ArrayHelper::map( $tariffs , 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChats()
    {
        return $this->hasMany(Chat::className(), ['user_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Group::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotifications()
    {
        return $this->hasMany(Notification::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionaries()
    {
        return $this->hasMany(Questionary::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResumeStatuses()
    {
        return $this->hasMany(ResumeStatus::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferalRedirects()
    {
        return $this->hasMany(ReferalRedirects::className(), ['refer_company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tags::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVacancies()
    {
        return $this->hasMany(Vacancy::className(), ['user_id' => 'id']);
    }

    public static function getAllInArray()
    {
        $companies = self::find()
            ->orderBy(['id' => SORT_DESC]);

        return ArrayHelper::map($companies->all(), 'id', 'fio');
    }

    public function getResumeCount()
    {
        $questionary = Questionary::find()->where(['user_id' => $this->id])->all();
        $count = 0;
        foreach ($questionary as $value) {
            $count += Resume::find()->joinWith('questionary', true)
                ->where(['questionary.user_id' => $this->id ])
                ->andWhere(['questionary.id' => $value->id])->count();
        }

        return $count;
    }

    public function getReferal()
    {
        if($this->referal_id != null){
            $user = Users::findOne($this->referal_id);
            if($user != null) return $user->fio;
        }
        return null;
    }

}
