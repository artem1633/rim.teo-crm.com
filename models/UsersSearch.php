<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Users;

/**
 * UsersSearch represents the model behind the search form about `app\models\Users`.
 */
class UsersSearch extends Users
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type','count_resume', 'count_questionery'], 'integer'],
            [['fio', 'login', 'password', 'telegram_id', 'telephone', 'foto', 'utm', 'agree', 'unique_code_for_telegram', 'description', 'last_login_date', 'new_sms', 'telegram_login', 'connect_to_telegram', 'register_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Users::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'last_login_date' => $this->last_login_date,
            'register_date' => $this->register_date,
            'count_resume' => $this->count_resume,
            'count_questionery' => $this->count_questionery,
        ]);

        $query->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'unique_code_for_telegram', $this->unique_code_for_telegram])
            ->andFilterWhere(['like', 'foto', $this->foto])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'utm', $this->utm])
            ->andFilterWhere(['like', 'agree', $this->agree])
            ->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'telegram_id', $this->telegram_id])
            ->andFilterWhere(['like', 'new_sms', $this->new_sms])
            ->andFilterWhere(['like', 'telegram_login', $this->telegram_login])
            ->andFilterWhere(['like', 'connect_to_telegram', $this->connect_to_telegram]);

        return $dataProvider;
    }
}
