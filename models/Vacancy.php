<?php

namespace app\models;

use Yii;

use app\base\AppActiveQuery;
use yii\web\ForbiddenHttpException;
/**
 * This is the model class for table "vacancy".
 *
 * @property int $id
 * @property string $name
 */
class Vacancy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vacancy';
    }

    public static function find()
    {
        if(Yii::$app->user->identity->type != 0){
            $user_id = Yii::$app->user->identity->id;
        } else {
            $user_id = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'user_id' => $user_id,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false) {
            if(Yii::$app->user->identity->type !== 0)
            {
                $user_id = Yii::$app->user->identity->id;
                if($model->user_id != $user_id){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['user_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'user_id' => 'Компания',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->user_id = Yii::$app->user->identity->id;
        }

        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        $questionary = Questionary::find()->where(['vacancy_id' => $this->id])->all();
        foreach ($questionary as $value) {
            $value->delete();
        }
        return parent::beforeDelete();
    }

    public function setDefaultValues()
    {
        $vacancy = new Vacancy();
        $vacancy->name = 'Видео монтаж';
        $vacancy->save();

        $vacancy = new Vacancy();
        $vacancy->name = 'Программисты';
        $vacancy->save();

        $vacancy = new Vacancy();
        $vacancy->name = 'Работа на дому';
        $vacancy->save();

        $vacancy = new Vacancy();
        $vacancy->name = 'Личный помощник';
        $vacancy->save();

        $vacancy = new Vacancy();
        $vacancy->name = 'Создание сайтов';
        $vacancy->save();

        $vacancy = new Vacancy();
        $vacancy->name = 'Копирайтеры';
        $vacancy->save();

        $vacancy = new Vacancy();
        $vacancy->name = 'Дизайн';
        $vacancy->save();

        $vacancy = new Vacancy();
        $vacancy->name = 'Автоворонки';
        $vacancy->save();

        $vacancy = new Vacancy();
        $vacancy->name = 'Аналитик';
        $vacancy->save();

        $vacancy = new Vacancy();
        $vacancy->name = 'Боты';
        $vacancy->save();

        $vacancy = new Vacancy();
        $vacancy->name = 'Маркетолог';
        $vacancy->save();

        $vacancy = new Vacancy();
        $vacancy->name = 'Десктоп';
        $vacancy->save();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionaries()
    {
        return $this->hasMany(Questionary::className(), ['vacancy_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResumes()
    {
        return $this->hasMany(Resume::className(), ['vacancy_id' => 'id']);
    }

}
