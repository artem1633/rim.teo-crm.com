<?php

namespace app\modules\api\controllers;

use app\components\helpers\TagHelper;
use app\models\AccountingReport;
use app\models\Bots;
use app\models\BotsAnswers;
use app\models\BotsDialogs;
use app\models\CompanySettings;
use app\models\DailyReport;
use app\models\Dispatch;
use app\models\DispatchChats;
use app\models\DispatchConnect;
use app\models\DispatchRegist;
use app\models\DispatchStatus;
use app\models\Logs;
use app\models\Message;
use app\models\Proxy;
use app\models\ReferalRedirects;
use app\models\Settings;
use app\models\Users;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\rest\ActiveController;
use yii\web\Response;
use Yii;

/**
 * Default controller for the `api` module
 */
class ClientController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['testmsg', 'send','chatsls','sendsls', 'status', 'history', 'proxy', 'test', 'akkunread', 'getdialog', 'getbotdialog', 'akkread'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }


    public function actionTest()
    {
        //$userVk = '252341158,13390257,333043290';
        $text = 'тест';
        //$token = '89a237d21dfc53cd066408833a8c2a82538b6f89c11c3d70c8b778a512d4705a02509187ce1040707ecc7';
        $token = Settings::find()->where(['key' => 'vk_access_token'])->one();

        $url = 'https://api.vk.com/method/messages.send';
        $params = array(
            //  'user_id' => 252341158,13390257,333043290,    // Кому отправляем

            //  'group_id' => 101360637,
            'user_id' => 252341158,
            //'group_id' => 101360637,
            'message' => $text,   // Что отправляем
            'access_token' => $token->value, // Токен того кто отправляет
            'v' => '5.37',
        );

        // В $result вернется id отправленного сообщения
        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));
        $result = json_decode($result, true);


        // foreach ($result['response']['items'] as $i) {
        //     $message = DispatchStatus::find()->where(['account_id' => $i['message']['user_id']])->all();
        //     if ($message) {
        //         $message->status = DispatchStatus::STATUS_UNREAD_MESSAGE;
        //         $message->update();
        //     }
        //     var_dump($message->id);
        //     var_dump($i['message']['user_id']);

        // }
        return $result;
    }

    //Проверка прочитаных сообщений
    public function actionAkkread()
    {

        $akkAll = DispatchRegist::find()->where('status != :status', ['status' => 'account_blocked'])->all();

        $i = 0;
        /** @var DispatchRegist $akk */
        foreach ($akkAll as $akk) {

            $i++;//для логов

            $my_arr[$i]['akk'] = $akk->id;
            $my_arr[$i]['akk_status'] = $akk->status;

            $proxy = Proxy::findOne(['id' => $akk->proxy]);


            $mesAll = DispatchStatus::find()
                ->where(
                    [
                        'send' => true,
                        'read' => 'no',
                        'send_account_id' => $akk->id
                    ])->all();

            /** @var DispatchStatus $mes */
            foreach ($mesAll as $mes) {
                $his = getHistory($mes->account_id, $akk->token, "tcp://{$proxy->ip_adress}:{$proxy->port}");
                if ($his['response']['items'][0]['read_state']) {
                    $mes->read = 'yes';
                    $mes->update();

                    //Должно записывать что новый человек прочитал сообщение
                    $report = new DailyReport([
                        'company_id' => $akk->company_id,
                        'account_id' => $akk->id,
                        'sended_message_count' => 1,
                        'type' => DailyReport::TYPE_READ,
                        'dispatch_id' => $mes->dispatch_id,
                    ]);
                    $report->save();

                }
            }
        }
        echo "<pre>";
        print_r($my_arr);
        echo "</pre>";
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionGetdialog()//проверяем диалоги на не прочитанные сообщения
    {

        /** @var Settings $setingToken */
        $setingToken = Settings::find()->where(['key' => 'vk_access_token'])->one();
        $setingProxy = Settings::find()->where(['key' => 'proxy_server'])->one();

        $akkAll = DispatchRegist::find()->where('status = :status || status = :status_limit || status = :interval_not_end',
            ['status' => 'ok', ':status_limit' => 'limit_exceeded', ':interval_not_end' => 'interval_not_end'])->all();

        $i = 0;
        /** @var DispatchRegist $akk */
        foreach ($akkAll as $akk) {
            $connect = DispatchConnect::find()->where(['dispatch_regist_id' => $akk->id])->one();
            $i++;//для логов
            $i2 = 0;//для логов
            $akkPush = false; // отправка уведомлений
            $my_arr[$i]['akk'] = $akk->id;
            $my_arr[$i]['akk_status'] = $akk->status;

            $proxy = Proxy::findOne(['id' => $akk->proxy]);

            $result = $this->getDialog($akk->token, "tcp://{$proxy->ip_adress}:{$proxy->port}");

            if ($result['error']) {
                $my_arr[$i]['error'] = $result['error']["error_msg"];
                continue;
            }

            foreach ($result['response']['items'] as $item) {
                $i2++;
                $my_arr[$i][$i2]['message_user_id'] = $item['message']['user_id'];
                $my_arr[$i][$i2]['message_user_id_status'] = 'false';

                /** @var DispatchStatus $message */
                $message = DispatchStatus::find()
                    ->where('send = :send and new_message = :new_message and account_id = :account_id',
                        [
                            'send' => true,
                            'new_message' => false,
                            'account_id' => $item['message']['user_id'],
                            //'status' => 4,
                        ])->orderBy('id DESC')->one();

                $my_arr[$i][$i2]['body'] = $item['message']['body'];
                if ($message) {//пользователь только отправил нам уведомления
                    if ($message->status == 4) {
                        continue;
                    }
                    $dispatch = Dispatch::find()->where(['id' => $message->dispatch_id])->one();


                    $message->new_message = true;
                    $message->read = 'yes';
                    $message->update();

                    //подкачиваем историю на случай если было отправленно больше чем 1 не прочитанное сообщение
                    $results2 = getHistory($item['message']['user_id'], $akk->token, $proxy);

                    foreach ($results2['response']['items'] as $result2) {
                        if (!$result2['read_state']) {
                            /** @var Message $mesHistory */
                            if($result['from_id'] != $akk->id)
                            {
                                $mesHistory = new Message();
                                $mesHistory->date_time = date("Y-m-d H:i:s");
                                $mesHistory->company_id = $akk->company_id;
                                $mesHistory->dispatch_id = $message->dispatch_id;
                                $mesHistory->dispatch_registr_id = $akk->id;
                                $mesHistory->dispatch_status_id = $message->id;
                                if ($dispatch->text_data) $mesHistory->database_id = $dispatch->text_data;
                                $mesHistory->text = $result2['body'];
                                $mesHistory->from = Message::FROM_CLIENT;
                                $mesHistory->save();

                                $messagesCount = Message::find()->where([
                                    'company_id' => $akk->company_id,
                                    'dispatch_id' => $message->dispatch_id,
                                    'dispatch_registr_id' => $akk->id,
                                    'dispatch_status_id' => $message->id
                                ])->count();

                                $maxStoredMessages = 10;

                                if($messagesCount > $maxStoredMessages){
                                    $deleteCount = $messagesCount - $maxStoredMessages;
                                    $deleteMessages = Message::find()->where([
                                        'company_id' => $akk->company_id,
                                        'dispatch_id' => $message->dispatch_id,
                                        'dispatch_registr_id' => $akk->id,
                                        'dispatch_status_id' => $message->id
                                    ])->limit($deleteCount)->all();

                                    foreach ($deleteMessages as $msg){
                                        $msg->delete();
                                    }
                                }
                            }

                            $partnerUrl = Settings::findByKey('partner_domain')->value;

                            //if (!$akkPush) {//проверяем отправляли уведомления
                            $text = "
                            __________________________________________
                            У пользователя №{$message->send_account_id} новое не прочитаное сообщение 
                                      Рассылка: {$dispatch->name}
                                      Акк: https://vk.com/id{$item['message']['user_id']}
                                      Текст: {$result2['body']}

                                      {$partnerUrl}/dispatch-status/dialog?id={$message->id}";
                            $userAll = DispatchChats::find()->where(['dispatchId' => $message->dispatch_id])->all();
                            foreach ($userAll as $user) {
                                if ($user) {
                                    $this->send($user->chatId, $text, $setingToken->value, $setingProxy->value);
                                }
                            }
                        }
                    }

                    $my_arr[$i][$i2]['message_user_id_status'] = 'true';
                } else {
//                    $message = DispatchStatus::find()
//                        ->where(
//                            ['account_id' => $item['message']['user_id'],'company_id' => $akk->company_id,
//                                'new_message' => false]
//                        )->one();
//                    if (!$message) {
//                        continue;
//                    }
//
//                    $connect = DispatchConnect::find()->where(['dispatch_regist_id' => $akk->id])->one();
//                    /** @var DispatchStatus $message */
//                    //$message = new DispatchStatus();
//                    $message->account_id = $item['message']['user_id'];
//                    $message->send_account_id = $akk->id;
//                    $message->new_message = true;
//                    $message->read = 'yes';
//                    $message->send = true;
//                    //$message->company_id = $akk->company_id;
//                    if ($connect) $message->dispatch_id = $connect->dispatch_id;
//                    $message->save();
//                    if ($message->errors) {
//                        $my_arr[$i][$i2]['new_error'] = $message->errors;
//                        continue;
//                    }
//
//                    $company = Users::find()->where(['company_id' => $akk->company_id])->one();
//                    if ($company->vk_id) {
//                        $akk->error_notif = true;
//                        $text = "У пользователя №{$message->send_account_id} БЕЗ РАССЫЛКИ новое не прочитаное сообщение
//                              http://demo.teo-send.ru/dispatch-regist/show?unread={$message->send_account_id}
//                              ";
//                        $this->send($company->vk_id, $text, $setingToken->value, $setingProxy->value);
//                    }
//                    $my_arr[$i][$i2]['message_user_id_status'] = 'true';

                }
            }
        }
        echo "<pre>";
        print_r($my_arr);
        echo "</pre>";
    }


    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionGetbotdialog()//проверяем диалоги на не прочитанные сообщения
    {
        /** @var Settings $setingToken */
        /** @var Settings $setingProxy */
        $setingToken = Settings::find()->where(['key' => 'vk_access_token'])->one();
        $setingProxy = Settings::find()->where(['key' => 'proxy_server'])->one();

        //список всех рассылок с ботом
        $dispatchAll = Dispatch::find()->where(['autoAnswer' => 1])->all();

        $i = 0;
        /** @var Dispatch $dispatch */
        foreach ($dispatchAll as $dispatch) {
            $log[$i]['dispatch'] = $dispatch->name;
            $conectAll = DispatchConnect::find()
                ->where('dispatch_id = :dispatch_id', ['dispatch_id' => $dispatch->id])
                ->all();
            $i2 = 0;
            foreach ($conectAll as $conect) {

                /** @var DispatchRegist $akk */
                $akk = DispatchRegist::find()
                    ->where('id = :id and status != :status',
                        ['id' => $conect->dispatch_regist_id, 'status' => 'account_blocked'])
                    ->one();

                $log[$i][$i2]['akk'] = $akk->id;
                $proxy = Proxy::findOne(['id' => $akk->proxy]);

                //получаем список не прочитаных сообщений
                $result = $this->getDialog($akk->token, "tcp://{$proxy->ip_adress}:{$proxy->port}");

                $i3 = 0;
                //перебираем все не прочитаные сообщения
                foreach ($result['response']['items'] as $item) {

                    $log[$i][$i2][$i3]['items'] = $item['message']['user_id'];

                    $message = DispatchStatus::find()->where(['account_id' => $item['message']['user_id']])->one();
                    $log[$i][$i2][$i3]['message'] = 'true';
                    //если сервис не делал рассылку по этому пользователю то пропускаем
                    if (!$message) {
                        $log[$i][$i2][$i3]['message'] = 'false';
                        continue;
                    }

                    //получаем историю переписок пользователя
                    $his = getHistory($item['message']['user_id'], $akk->token, $proxy);

                    //кол-во всего сообщений
                    $count = $his['response']['count'];

                    $log[$i][$i2][$i3]['$count'] = $count;
                    //текст последнего сообщения
                    $bodyText = $his['response']['items'][$count - 1]['body'];
                    $log[$i][$i2][$i3]['$bodyText'] = $bodyText;

                    /** @var BotsDialogs $botDialog */
                    $botDialog = BotsDialogs::find()
                        ->where('botId = :id and stepNumber = :stepNumber',
                            ['id' => $dispatch->botId, 'stepNumber' => $count])
                        ->one();

                    //Автоматический ответ от бота включчен на любой ответ
                    if ($botDialog->anyAnswer) {
                        $log[$i][$i2][$i3]['$botDialogAvto'] = 'true';
                        //отправляем авто ответ
                        $this->send($item['message']['user_id'], $botDialog->result, $akk->token, $proxy);
                        //стоит уведомление
                        if ($botDialog->notify) {
                            /** @var Bots $bot */
                            $bot = Bots::find()->where(['id' => $botDialog->botId])->one();
                            $text = "Бот \"{$bot->name}\" автоматически отправил сообщение \"{$botDialog->result}\" 
                              пользователю https://vk.com/id{$item['message']['user_id']}  
                              Посмотреть переписку можно тут
                              http://demo.teo-send.ru/dispatch-regist/show?unread={$akk->id}
                              ";
                            $userAll = DispatchChats::find()->where(['dispatchId' => $dispatch->id])->all();
                            foreach ($userAll as $user) {
                                if ($user) {
                                    $this->send($user->chatId, $text, $setingToken->value, $setingProxy->value);
                                }
                            }
                        }
                        continue;
                    }

//                    if ($botDialog->delayMin) {
//                        //Условие ниже (3 строки) нужно вставить в условие/функцию сразу после получения ответа в диалоге.
////                        if ($botDialog->message_sent){ //Если дата отправки сообщения уже есть
////                            $botDialog->message_sent = null; //Удаляем дату последней отправки сообщения
////                        }
//
//                        $log[$i][$i2][$i3]['$botDialogDelay'] = 'true';
//                        $delay = $botDialog->delayMin;
//
//                        if (!$botDialog->message_sent) {//Если нет записи о времени отправки последнего сообщения
//                            $botDialog->message_sent = time(); //Задаем текущее время
//                            $botDialog->save();
//                            continue;
//                        } else { //Если есть запись о времени отправки сообщения
//                            if ((time() - $botDialog->message_sent) >= 60 * $delay){ //Если разница больше или равна установленному времени ожидания
//                                $this->send($item['message']['user_id'], $botDialog->result, $akk->token, $proxy);
//                                $botDialog->message_sent = time(); //Задаем новое время отправки сообщения.
//                                $botDialog->save();
//                            }
//
//                            //стоит уведомление
//                            if ($botDialog->notify) {
//                                /** @var Bots $bot */
//                                $bot = Bots::find()->where(['id' => $botDialog->botId])->one();
//                                $text = "После ожидания в течении {$delay} мин.
//                                бот \"{$bot->name}\" автоматически отправил сообщение \"{$botDialog->result}\"
//                              пользователю https://vk.com/id{$item['message']['user_id']}
//                              Посмотреть переписку можно тут
//                              http://demo.teo-send.ru/dispatch-regist/show?unread={$akk->id}
//                              ";
//                                $userAll = DispatchChats::find()->where(['dispatchId' => $dispatch->id])->all();
//                                foreach ($userAll as $user) {
//                                    if ($user) {
//                                        $this->send($user->chatId, $text, $setingToken->value, $setingProxy->value);
//                                    }
//                                }
//                            }
//                            continue;
//                        }
//
//
//                    }

                    if ($botDialog) {
                        $step = BotsAnswers::find()
                            ->where('dialogId = :dialogId and answerSet LIKE :bodyText',
                                ['dialogId' => $botDialog->id, 'bodyText' => "%{$bodyText}%"])
                            ->one();
                    }


                    $log[$i][$i2][$i3]['$botDialog'] = 'false';

                    //нашли совпадение
                    if ($step) {
                        $log[$i][$i2][$i3]['$botDialog'] = 'true';
                        //отправляем авто ответ
                        $this->send($item['message']['user_id'], $botDialog->result, $akk->token, $proxy);
                        //стоит уведомление
                        if ($botDialog->notify) {
                            /** @var Bots $bot */
                            $bot = Bots::find()->where(['id' => $botDialog->botId])->one();
                            $text = "Бот \"{$bot->name}\" автоматически отправил сообщение \"{$botDialog->result}\" 
                              пользователю https://vk.com/id{$item['message']['user_id']}  
                              Посмотреть переписку можно тут
                              http://demo.teo-send.ru/dispatch-regist/show?unread={$akk->id}
                              ";
                            $userAll = DispatchChats::find()->where(['dispatchId' => $dispatch->id])->all();
                            foreach ($userAll as $user) {
                                if ($user) {
                                    $this->send($user->chatId, $text, $setingToken->value, $setingProxy->value);
                                }
                            }
                        }
                        //не нашли нужного ответа
                    } else {
                        $bot = Bots::find()->where(['id' => $botDialog->botId])->one();
                        $text = "В рассылки \"{$dispatch->name} \" в Боте {$bot->name} нету подходящего ответа
                              Диалог с пользователем https://vk.com/id{$item['message']['user_id']}
                                Посмотреть переписку можно тут  
                              http://demo.teo-send.ru/dispatch-regist/show?unread={$akk->id}
                              ";

                        $userAll = DispatchChats::find()->where(['dispatchId' => $dispatch->id])->all();
                        foreach ($userAll as $user) {
                            if ($user) {
                                $this->send($user->chatId, $text, $setingToken->value, $setingProxy->value);
                            }
                        }
                    }


                }

            }

        }

        echo "<pre>";
        print_r($log);
        echo "</pre>";


    }


    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionAkkunread()//Проверяем все акк на ограничения
    {
        $i = 0;

        $setingToken = Settings::find()->where(['key' => 'vk_access_token'])->one();
        $setingProxy = Settings::find()->where(['key' => 'proxy_server'])->one();


        $akkAll = DispatchRegist::find()->where(['!=', 'status', DispatchRegist::STATUS_SLEEPING])->andWhere(['!=', 'status', 'account_blocked'])->all();
        /** @var DispatchRegist $akk */
        foreach ($akkAll as $akk) {
            $i++;

            $my_arr[$i]['acc'] = $akk->id;
            $my_arr[$i]['acc_status_start'] = $akk->status;

            /** @var DispatchRegist $data */
            $setting = CompanySettings::find()->where(['company_id' => $akk->company_id])->one();
            // кол-во сообщения в день
            $acc_message_count = $setting->messages_daily_limit;
            $my_arr[$i]['acc_message_count'] = $acc_message_count;
            $my_arr[$i]['sended_message_count'] = $akk->sended_message_count;


            //  Проверяем не перевышан лимит сообщения в день (статус становиться limit_exceeded при отправке)
            $d = date("Y-m-d H:i:s", strtotime($akk->last_dispatch_time . " +1 days"));
            $now = date("Y-m-d H:i:s");
            $hour = round((strtotime($d) - strtotime($now)) / (60 * 60));
            $my_arr[$i]['hour'] = $hour;

            $my_arr[$i]['hour_status'] = 'false';
            if ($hour < 0) {
                $akk->sended_message_count = 0;
                $akk->status = "ok";
                $my_arr[$i]['hour_status'] = 'true';
            }


            // Проверяем еще не прошел интервал (статус становиться interval_not_end при отправке)
            $date_2 = date("Y-m-d H:i:s");
            $interval_min = Settings::find()->where(['key' => 'interval_in_minutes'])->one();
            $diff = strtotime($date_2) - strtotime($akk->last_dispatch_time);
            $diff = round($interval_min->value - $diff / 60);
            $my_arr[$i]['diff'] = $diff;

            $my_arr[$i]['diff_count_status'] = 'false';
            if ($diff < 0 && $akk->sended_message_count < $acc_message_count) {
                $akk->status = 'ok';
                $my_arr[$i]['diff_count_status'] = 'true';
            }


            $proxy = Proxy::findOne(['id' => $akk->proxy]);

            if (!$proxy) {
//                    $akk->status = DispatchStatus::STATUS_ACCOUNT_BLOCKED;
//                    $akk->coment = 'Прокси не подключен';
                $akk->makeBlock('Прокси не подключен');
                $akk->update();
                continue;
            }
            $url = 'https://api.vk.com/method/account.setOnline';
            $params = array(
                'access_token' => $akk->token,
                'v' => '5.80'
            );

            // В $result вернется id отправленного сообщения
            $result = file_get_contents($url, false, stream_context_create(array(
                'http' => array(
                    'proxy' => "tcp://{$proxy->ip_adress}:{$proxy->port}",
                    'request_fulluri' => true,
                    'method' => 'POST',
                    'header' => 'Content-type: application/x-www-form-urlencoded',
                    'content' => http_build_query($params)
                )
            )));
            $result = json_decode($result, true);

            //Если проверка показада ошибку и ранее ошибки не было то нужно поставить статус и отправить уведомление
            if ($result['error'] and $akk->status != DispatchStatus::STATUS_ACCOUNT_BLOCKED) {
                //Если проблемма в том что акк заморозили
                if ($result['error']["error_msg"] = 'User authorization failed: invalid access_token (2).') {
                    $my_arr[$i]['error'] = $result['error']["error_msg"];
                    $akk->makeBlock('Заморожен');
                    if (!$akk->error_notif) {
                        $company = Users::find()->where(['company_id' => $akk->company_id])->one();
                        $connect = DispatchConnect::find()->where(['dispatch_regist_id' => $akk->id])->one();
                        if ($company->vk_id) {
                            $akk->error_notif = true;
                            $text = "Аккаунт \"{$akk->username}\" не доступен по причине \"{$result['error']["error_msg"]}\"
                            Для проверки перейдите http://demo.teo-send.ru/dispatch-regist/show-akk?block={$connect->dispatch_id}";
                            $this->send($company->vk_id, $text, $setingToken->value, $setingProxy->value);
                        }
                    }
                    //Если поблемма не в том что акк заморозили
                } else {
                    $my_arr[$i]['error'] = $result['error']["error_msg"];
//                    $akk->status = DispatchStatus::STATUS_ACCOUNT_BLOCKED;
//                    $akk->blocking_date = date('Y-m-d'); //Запись даты блокировки аккаунта
//                    $akk->coment = $result['error']["error_msg"];
                    $akk->makeBlock($result['error']["error_msg"]);
                    if (!$akk->error_notif) {
                        $company = Users::find()->where(['company_id' => $akk->company_id])->one();
                        $connect = DispatchConnect::find()->where(['dispatch_regist_id' => $akk->id])->one();
                        if ($company->vk_id) {
                            $akk->error_notif = true;
                            $text = "Аккаунт \"{$akk->username}\" не доступен по причине \"{$result['error']["error_msg"]}\"
                            Для проверки перейдите http://demo.teo-send.ru/dispatch-regist/show-akk?block={$connect->dispatch_id} ";
                            $this->send($company->vk_id, $text, $setingToken->value, $setingProxy->value);
                        }
                    }
                }
                //Если ранее акк был заблокирована а теперь нет
            } elseif (!$result['error'] and $akk->status == DispatchStatus::STATUS_ACCOUNT_BLOCKED) {
                // $my_arr[$i]['error'] = $result['error']["error_msg"];
                $akk->status = 'ok';
                $akk->coment = $result['error']["error_msg"];
                $akk->error_notif = false;
            }
            $my_arr[$i]['acc_status_finish'] = $akk->status;
            //Проверка на заполнение полей username и account_url
            if ($akk->status == 'ok' && !$akk->username && !$akk->account_url) {
                DispatchRegist::setVkInfo($akk);
                Yii::warning("Данные обновлены. Логин: $akk->login", __METHOD__);
            }
            $akk->update();
        }

        echo "<pre>";
        print_r($my_arr);
        echo "</pre>";

    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @return array
     */
    public function actionSendRefer()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $headers = Yii::$app->response->headers;
        $headers->add('Access-Control-Allow-Origin', '*');

        $refer = new ReferalRedirects([
            'ip' => $_SERVER['REMOTE_ADDR'],
            'refer_company_id' => $_GET['ref'],
            'user_agent' => Yii::$app->request->userAgent,
        ]);

        return ['result' => $refer->save()];
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionProxy()
    {
        $userVk = '252341158';
        $text = "Не работает прокси {$proxy->ip_adress}";
        $token = 'dd7da004e05231059e7175bf2f35e7a381681d28f8ff79bbe0b61164b29ee9ad433e30cf3ebb4edafc8a6';

        $proxys = Proxy::find()->all();
        foreach ($proxys as $proxy) {
            $result = false;
            $proxy11 = "{$proxy->ip_adress}:{$proxy->port}";
            $link = 'http://dynupdate.no-ip.com/ip.php';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_PROXY, $proxy11);
            curl_setopt($ch, CURLOPT_URL, $link);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($ch);
            curl_close($ch);

            if (!$result) {
                $proxy->status = 'no';
                $proxy->update();
                var_dump('no');
                var_dump($proxy->ip_adress);
                var_dump($result);

                $url = 'https://api.vk.com/method/messages.send';
                $params = array(
                    'user_id' => $userVk,    // Кому отправляем
                    'message' => $text,   // Что отправляем
                    'access_token' => $token, // Токен того кто отправляет
                    'v' => '5.37',
                );

                // В $result вернется id отправленного сообщения
                $result = file_get_contents($url, false, stream_context_create(array(
                    'http' => array(
                        // 'proxy' => $proxy,
                        'request_fulluri' => true,
                        'method' => 'POST',
                        'header' => 'Content-type: application/x-www-form-urlencoded',
                        'content' => http_build_query($params)
                    )
                )));
                $result = json_decode($result, true);

            } else {
                $proxy->status = 'yes';
                $proxy->update();
                var_dump('yes');
                var_dump($proxy->ip_adress);
                var_dump($result);

            }
        }

    }


    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionSend()
    {
        $i = 0;
        $max_send = 0;

        $interval_min = Settings::find()->where(['key' => 'interval_in_minutes'])->one();

        // Взять сообщение статусом В очериди
        $messages = Dispatch::find()->where(['status' => 'job'])->all();

        Yii::warning($messages, __METHOD__);

        if ($messages) {
            foreach ($messages as $message) {

                Yii::warning(Json::encode($message), __METHOD__);

                $string_acc = '';
                $string_count = 0;

                Yii::warning( $message->id, __METHOD__);

                $connects = DispatchConnect::find()->where(['dispatch_id' => $message->id])->all();

                Yii::warning($connects, __METHOD__);

                foreach ($connects as $connect) {
                    $iLim = 0; //Интервал между отправками
                    $i++;
                    $my_arr[$i]['dispatch_id'] = $connect->dispatch_id;
                    $my_arr[$i]['dispatch_regist_id'] = $connect->dispatch_regist_id;
                    $string_count = 0;
                    $payCount = 0;
                    $payAmount = 0;
                    $account = DispatchRegist::find()->where(['id' => $connect->dispatch_regist_id, 'status' => 'ok'])->one();

                    //VarDumper::dump($account, 10, true); die();

                    if ($account) {
                        $my_arr[$i]['account'] = $account->id;
                        $my_arr[$i]['account_status_start'] = $account->status;
                        $my_arr[$i]['account_name'] = $account->username;

                        /** @var DispatchRegist $data */
                        $setting = CompanySettings::find()->where(['company_id' => $account->company_id])->one();
                        // кол-во сообщения за раз
                        $count_message = $setting->messages_in_transaction;
                        // кол-во сообщения в день
                        $acc_message_count = $setting->messages_daily_limit;

                        // Получаем клиенты кто в очереди кому надо отправить в колчестве кол-во сообщения за раз
                        $clients = DispatchStatus::find()->where(['send' => false, 'dispatch_id' => $message->id])->orderBy(['id' => SORT_ASC,])->limit($count_message)->all();

                        //VarDumper::dump($clients, 10, true); die();

                        // если нет таких, то статус сообщения становиться отправлено (finish)
                        if (!$clients) {
                            $message->status = "finish";
                            $message->update();
                        } else {
                            // Отправляем сообщения каждому
                            foreach ($clients as $user) {

                                //VarDumper::dump($user, 10, true); die();

                                $iLim++;
                                $max_send++;
                                $proxy = Proxy::findOne(['id' => $account->proxy]);
                                if (!$proxy) {
                                    continue;
                                }
                                $companySetting = CompanySettings::findOne(['company_id' => $account->company_id]);
                                $priceByMessage = $companySetting->getPriceByMessage();
                                if ($companySetting->getGeneralBalans() < $priceByMessage) {
                                    continue;
                                }
                                if ($proxy->status == 'yes') {
                                    $send_proxy = "tcp://{$proxy->ip_adress}:{$proxy->port}";
                                    // задешка 5 секунд
                                    if ($iLim == 8) {
                                        sleep(5);
                                        $iLim = 0;
                                    }
                                    if ($max_send == 100) {
                                        echo "<pre>";
                                        print_r($my_arr);
                                        echo "</pre>";
                                        return true;
                                    }

                                    // Выбираем случайное сообщение
                                    for ($i = 0; $i < 20; $i++) {
                                        $num = 'text' . random_int(1, 4);
                                        $msg = $message[$num];
                                        if ($msg) {//Если сообщение не пустое - ввыходим из цикла
                                            break 1;
                                        }
                                    }

                                    //VarDumper::dump($msg, 10, true); die();

//                                     $msg = $this->replacingTags($msg, $user);//Обрабатываем теги выбранного сообщения

                                    $msg = TagHelper::handle($msg, [$account, $user]);

//                                    if ($msg != 'Недостаточно информации'){
//                                         Отправка сообщения
                                    $result = $this->send((int)trim($user->account_id), $msg, $account->token, $send_proxy);

                                    $mesHistory = new Message();
                                    $mesHistory->date_time = date("Y-m-d H:i:s");
                                    $mesHistory->company_id = $account->company_id;
                                    $mesHistory->dispatch_id = $message->id;
                                    $mesHistory->dispatch_registr_id = $account->id;
                                    $mesHistory->dispatch_status_id = $user->id;
                                    $mesHistory->text = $msg;
                                    $mesHistory->from = Message::FROM_ACCOUNT;
                                    $mesHistory->save();

                                    $string_count++;

                                    // сообщения успешно отправлено
                                    if ($result['response'] != null) {

                                        $user->send = true;
                                        $user->data = date("Y-m-d H:i:s");
                                        $user->read = "no";
                                        $user->send_account_id = $account->id;
                                        $user->response_id = $result['response'];
                                        $user->update();

                                        Yii::warning('Сообщение успешно отправлено', __METHOD__);

                                        // увеличеваем кол-во отправленных сообщения с аккаунта (изначально он равно к 0)
                                        $account->sended_message_count++;
                                        $account->all_sended_message_count++;
                                        $account->last_dispatch_time = $user->data;

                                        // БЛОК СПИСАНИЯ ДЕНЕГ С БАЛАНСА ЮЗЕРА

                                        if ($companySetting->payed()) {
                                            $payCount++;
                                            $payAmount += $priceByMessage;
                                        }

                                        // сообщения не отправилось записаваем причину в логах
                                    } elseif ($result['error']["error_msg"] == 'Permission to perform this action is denied') {
                                        $user->send = true;
                                        $user->data = date("Y-m-d H:i:s");
                                        $user->read = "no";
                                        $user->send_account_id = $account->id;
                                        $user->response_id = $result['response'];
                                        $user->update();
                                    } else {
                                        $my_arr[$i]['account'] = $user->account_id;
                                        $account->status = DispatchStatus::STATUS_ACCOUNT_BLOCKED;
                                        $account->blocking_date = date('Y-m-d'); //Запись даты блокировки аккаунта
                                        $account->coment = $result['error']["error_msg"];
                                        $log = new Logs([
                                            'user_id' => $account->id,
                                            'event_datetime' => date('Y-m-d H:i:s'),
                                            'event' => DispatchStatus::STATUS_ACCOUNT_BLOCKED,
                                        ]);
                                        $log->description = "Сообщения не отправилось пользователу {$user->account_id}, Ошибка:" . $result['error']['error_msg'];
                                        $log->save();
                                        $account->update();
                                        continue;
                                    }

                                    // считаем отправленный сообщения с аккаунта если он равно к кол-во сообщения за раз то статус меняем на (interval_not_end)
                                    $my_arr[$i]['string_count'] = $string_count;
                                    $my_arr[$i]['count_message'] = $count_message;
                                    $my_arr[$i]['interval_not_end'] = 'false';
                                    if ($string_count >= $count_message) {
                                        $account->status = "interval_not_end";
                                        $my_arr[$i]['interval_not_end'] = 'true';
                                    }

                                    // кол-во отправленых сообщения равно к кол-во сообщения в день то статус меняем на (limit_exceeded)
                                    $my_arr[$i]['sended_message_count'] = $account->sended_message_count;
                                    $my_arr[$i]['count_message'] = $acc_message_count;
                                    $my_arr[$i]['limit_exceeded'] = 'false';
                                    if ($account->sended_message_count >= $acc_message_count) {
                                        $account->status = "limit_exceeded";
                                        $my_arr[$i]['limit_exceeded'] = 'true';

                                        $report = new DailyReport([
                                            'company_id' => $account->company_id,
                                            'dispatch_id' => $message->id,
                                            'account_id' => $account->id,
                                            'sended_message_count' => $account->sended_message_count,
                                            'type' => DailyReport::TYPE_SENT,
                                        ]);
                                        $report->save();

                                        if ($payCount > 0) {
                                            $accountReport = new AccountingReport([
                                                'company_id' => $account->company_id,
                                                'operation_type' => AccountingReport::TYPE_DISPATCH_PAYED,
                                                'amount' => $payAmount,
                                                'description' => 'Списание средств за рассылку "' . $message->name . '". Отправлено ' . $payCount . ' сообщений',
                                            ]);
                                            $accountReport->save();
                                        }
                                    }
                                    $account->update();
//                                    } else {
//                                        Yii::warning('Недостаточно информации для замены тегов. Отправка пропущена', __METHOD__);
//                                        $my_arr['convert_tags'] =  'Недостаточно информации для замены тегов. Отправка пропущена';
//                                    }
                                }
                            }

                            $my_arr[$i]['account_status_finish'] = $account->status;

                        }

                    } else {
                        Yii::$app->session->setFlash('message_fail', "Нет готовых аккаунтов для отправки сообщении");
                    }

                    if ($string_count != 0) {
                        $string_acc .= "С аккаунта {$account->username} отправлен {$string_count} сообщения, ";
                        Yii::$app->session->setFlash('message_success', $string_acc);
                    }
                }

            }
        } else {

            Yii::$app->session->setFlash('message_info', "Нет готовых сообщение для отправки");
        }

        VarDumper::dump($my_arr, 10, true);
    }


    /**
     * Заменяет теги на соответствующие значения.
     * @param string $msg Сообщение
     * @param mixed $user Модель dispatch_status получателя сообщения
     * @return string
     */
    private function replacingTags($msg, $user)
    {
        Yii::warning('Сообщение до обработки: ' . $msg, __METHOD__);
        Yii::warning($user, __METHOD__);

        $dispatch_model = Dispatch::find()->where(['id' => $user->dispatch_id])->one();
        $account_model = DispatchRegist::find()->where(['id' => $user->send_account_id])->one();

        //VarDumper::dump($dispatch_model, 10, true); die();

        $sender = $account_model->username; //Имя отправителя
        $recipient = $user->name; //Имя получателя
//        $base = '--//--'; //Имя базы
        $city = $user->city; //Город получателя
        $dispatch_name = $dispatch_model->name; //Имя рассылки

        if (strpos($msg, '%}') > 0) {
            if (!$sender || !$recipient || !$city || !$dispatch_name){
                //VarDumper::dump('Недостаточно информации для подстановки!', 10, true); die();
                return 'Недостаточно информации';
            }
            $msg = str_replace(Dispatch::TAG_MESSAGE_SENDER, $sender, $msg);
            $msg = str_replace(Dispatch::TAG_MESSAGE_RECIPIENT, $recipient, $msg);
//            $msg = str_replace(Dispatch::TAG_BASE_NAME, $base, $msg);
            $msg = str_replace(Dispatch::TAG_CITY_RECIPIENT, $city, $msg);
            $msg = str_replace(Dispatch::TAG_DISPATCH_NAME, $dispatch_name, $msg);
            Yii::warning('Сообщение после обработки: ' . $msg, __METHOD__);

            //VarDumper::dump($msg, 10, true); die();

        }

        Yii::warning('Сообщение не содержит тегов', __METHOD__);

        return $msg;
    }

    function dateDifference($date_1)
    {

        $date_2 = date("Y-m-d H:i:s");

        $diff = strtotime($date_2) - strtotime($date_1);

        return $diff;

    }

    function send($userVk, $text, $token, $proxy)
    {
        $url = 'https://api.vk.com/method/messages.send';
        $params = array(
            'user_id' => $userVk,    // Кому отправляем
            'message' => $text,   // Что отправляем
            'access_token' => $token, // Токен того кто отправляет
            'v' => '5.37',
        );

        // В $result вернется id отправленного сообщения
        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'proxy' => $proxy,
                'request_fulluri' => true,
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));
        $result = json_decode($result, true);
        return $result;
    }


    /**
     * @param $token
     * @param $proxy
     * @return mixed
     */
    function getDialog($token, $proxy)
    {
        $url = 'https://api.vk.com/method/messages.getDialogs';
        $params = array(
            'unread' => '1',
            'preview_length' => '200',
            'access_token' => $token, // Токен того кто отправляет
            'v' => '5.80',
        );

        // В $result вернется id отправленного сообщения

        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'proxy' => $proxy,
                'request_fulluri' => true,
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));

        return json_decode($result, true);
    }

    /**
     * Это метод который ищет "спящие аккаунты", которые не используются более указанного кол-ва дней
     */
    public function actionSearchSleepingDispatchRegists()
    {
        $sleepingIntervalSetting = Settings::findByKey('sleeping_interval');
        if($sleepingIntervalSetting != null && $sleepingIntervalSetting->value != null)
        {
            $sleepingIntervalValue = $sleepingIntervalSetting->value;
        } else {
            $sleepingIntervalValue = 5;
        }
        $sleepingInterval = time() - ($sleepingIntervalValue * 86400); // 86400 — кол-во секунд в сутках
        $sleepingStartDate = date('Y-m-d H:i:s', $sleepingInterval);
        $accounts = DispatchRegist::find()->where(['<', 'last_dispatch_time', $sleepingStartDate])->all();

        foreach ($accounts as $account) {
            $account->makeSleep();
        }
    }

    /**
     * Проходит все аккаунты, у которых auto_view == 0 и оформляет их
     */
    public function actionApplyTemplates()
    {
        $dispatchRegists = DispatchRegist::find()->where(['auto_view' => 0])->limit(2)->all();

        /** @var \app\models\DispatchRegist $dispatchRegist */
        foreach ($dispatchRegists as $dispatchRegist)
        {
            $dispatchRegist->auto_view = 1;
            $dispatchRegist->makeAutoPage();
            $dispatchRegist->save(false);
        }
    }

    public function actionAkkInfo()
    {
        $setingProxy = Settings::find()->where(['key' => 'proxy_server'])->one();
        $setingToken = Settings::find()->where(['key' => 'vk_access_token'])->one();
        $allAkk = DispatchStatus::find()->where('photo is  null or name is null')->limit(150)->orderBy(['id' => SORT_DESC,])->all();
        $akks = ArrayHelper::map($allAkk, 'id', 'name');
        $list = '';
        /** @var DispatchStatus $item */
        foreach ($allAkk as $item) {

            if(stripos($item->default_account_id, 'vk.com/'))
            {
                if(stripos($item->default_account_id, 'vk.com/id'))
                {
                    $item->default_account_id = explode('vk.com/id', $item->default_account_id)[1];
                } else {
                    $item->default_account_id = explode('vk.com/', $item->default_account_id)[1];
                }
            }

            $list .= "{$item->default_account_id},";
        }
        $results = $this->getUser($list, 'photo_100,city,country,bdate,sex,domain', $setingToken->value, $setingProxy->value);
        $list = '';
        $i = 0;
        foreach ($results['response'] as $item2) {

            $users = DispatchStatus::find()
                ->where(['or', ['LIKE', 'default_account_id', strval($item2['id'])], ['LIKE', 'default_account_id', strval($item2['domain'])]])
                ->all();

            foreach ($users as $user) {

                if(isset($akks[$user->id]))
                {
                    unset($akks[$user->id]);
                }

                if ($user) {
                    $user->account_id = $item2['id'];
                    $user->name = "{$item2['first_name']}";
                    $user->photo = $item2['photo_100'];
                    $user->age = $item2['bdate'];
                    $user->gender = 'Ж';
                    if ($item2['sex'] == 2) {
                        $user->gender = 'М';
                    }
                    $user->city = $item2['country']['title'] . " " . $item2['city']['title'];
                    $user->save();
                    $i++;
                    $list .= "____{$i}
                  {$item2['first_name']}  {$item2['last_name']} ____
                  {$item2['country']['title']}  {$item2['city']['title']}____
                  {$item2['sex']}_____
                  {$user->gender}_____
                  {$item2['bdate']}_____
                  {$user->age}_____
                  
                  
                  
                  Akk {$item2['id']} Status";
                    if ($user->errors) {
                        $list .= "error";
                    } else {
                        $list .= "good";
                    }
                }
            }
        }
        return $list;
    }

    /**
     * Обновляет информацию у аккаутов, которые находятся в обрабатывающихся рассылках
     * @return string
     */
    public function actionAccountsUpdateInfo()
    {
        $globalList = '';
        $setingProxy = Settings::find()->where(['key' => 'proxy_server'])->one();
        $setingToken = Settings::find()->where(['key' => 'vk_access_token'])->one();
        $dispatches = Dispatch::find()->where(['status' => DispatchStatus::STATUS_HANDLE])->all();


        $counter = 0;
        foreach ($dispatches as $dispatch)
        {
            if($counter >= 10)
                break;

            $list = '';
            $clients = DispatchStatus::find()
                ->where('(account_id is  null or name is null) and (dispatch_id = :dispatch_id)', [':dispatch_id' => $dispatch->id])
                ->limit(100)
                ->all();

//            $accountsPks = array_values(ArrayHelper::map(DispatchConnect::find()->where(['dispatch_id' => $dispatch->id])->all(), 'id', 'dispatch_regist_id'));
//
//            $dispatchRegists = DispatchRegist::find()->where(['id' => $accountsPks, 'auto_view' => 0])->limit(2)->all();

//            foreach($dispatchRegists as $dispatchRegist)
//            {
////                $dispatchRegist->auto_view = 1;
//                $output = $dispatchRegist->makeAutoPage();
//                $dispatchRegist->save(false);
//
//                echo "<pre>";
//                print_r($output);
//                echo "</pre>";
//            }

            // $appliedCount = DispatchRegist::find()->where(['id' => $accountsPks, 'auto_view' => 1])->count();

            if(count($clients) == 0){
                $dispatch->status = DispatchStatus::STATUS_WAIT;
                $dispatch->save();
                continue;
            }

            foreach ($clients as $client)
            {
                if(stripos($client->default_account_id, 'vk.com/'))
                {
                    if(stripos($client->default_account_id, 'vk.com/id'))
                    {
                        $client->default_account_id = explode('vk.com/id', $client->default_account_id)[1];
                    } else {
                        $client->default_account_id = explode('vk.com/', $client->default_account_id)[1];
                    }
                }

                $list .= "{$client->default_account_id},";
            }

            $results = $this->getUser($list, 'photo_100,city,country,bdate,sex,domain', $setingToken->value, $setingProxy->value);
            $i = 0;
            //     echo "////////////////////////////";

            echo "<pre>";
            print_r($results);
            echo "</pre>";



            if(isset($results['error']))
            {
                echo '1';
                if($results['error']['error_code'] == '113')
                {
                    echo '2';
                    $id = $results['error']['request_params'][2]['value'];
                    echo "<br>$id<br>";
                    $id = trim(str_replace(',', '', $id));
                    $client = DispatchStatus::find()->where('`default_account_id` LIKE :id',[':id' => "%{$id}%"])->one();
//                    var_dump($id);
//                    echo DispatchStatus::find()->where('`default_account_id` LIKE :id',[':id' => "%{$id}%"])->createCommand()->getRawSql();;
                    if($client != null){
                        $client->delete();
                    }
                }
            }

            foreach ($results['response'] as $item)
            {
                if($item['deactivated'] == 'deleted' || $item['deactivated'] == 'banned')
                {
                    $id = $item['domain'];
                    $client = DispatchStatus::find()->where('`default_account_id` LIKE :id',[':id' => "%{$id}%"])->one();
                    var_dump($id);
                    echo DispatchStatus::find()->where('`default_account_id` LIKE :id',[':id' => "%{$id}%"])->createCommand()->getRawSql();;
                    if($client != null){
                        $client->delete();
                    }
                    $id = null;
                }

                $users = DispatchStatus::find()
                    ->where(['or', ['LIKE', 'default_account_id', strval($item['id'])], ['LIKE', 'default_account_id', strval($item['domain'])]])
                    ->all();


                foreach ($users as $user) {

                    if ($user) {

                        $user->account_id = $item['id'];
                        $user->name = "{$item['first_name']}";
                        $user->photo = $item['photo_100'];
                        $user->age = $item['bdate'];
                        $user->gender = 'Ж';
                        if ($item['sex'] == 2) {
                            $user->gender = 'М';
                        }
                        $user->city = $item['country']['title'] . " " . $item['city']['title'];
                        $user->save();
                        $i++;
                        $list2 .= "__{$i}
                 {$item['first_name']}  {$item['last_name']} __
                 {$item['country']['title']}  {$item['city']['title']}__
                 {$item['sex']}___
                 {$user->gender}___
                 {$item['bdate']}___
                 {$user->age}___



                 Akk {$item['id']} Status";
                        if ($user->errors) {
                            $list2 .= "error";
                        } else {
                            $list2 .= "good";
                        }
                    }
                }
            }

            $globalList .= $list2;
            $counter++;
        }
        $dispatchRegists = DispatchRegist::find()->where('auto_view = 0 AND (status = :ok or status = :limit_exceeded)',
            [':ok' => 'ok', ':limit_exceeded' => 'limit_exceeded'])->one();
        if ($dispatchRegists) {
            $dispatchRegists->makeAutoPage();
        }
        return $globalList;
    }


    /**
     * @param $token
     * @param $proxy
     * @return mixed
     */
    static function getUser($users, $fields, $token, $proxy)
    {
        if (!$token){

            $vk_id = Settings::find()->where(['key' => 'akk_notify'])->one()->value;
            $proxy = Settings::find()->where(['key' => 'proxy_server'])->one()->value;
            $token = Settings::find()->where(['key' => 'vk_access_token'])->one()->value;
            ClientController::request('messages.send', $proxy, [
                'access_token' => $token,
                'user_id' => $vk_id,
                'message' => "Запрос инфы без токена",
            ]);

            $setingToken = Settings::find()->where(['key' => 'vk_access_token'])->one();
            $token = $setingToken->value;
        }
        if (!$proxy) {
            $setingProxy = Settings::find()->where(['key' => 'proxy_server'])->one();
            $proxy = $setingProxy->value;
        }
        $url = 'https://api.vk.com/method/users.get';
        $params = array(
            'user_ids' => $users,
            'fields' => $fields,
            'name_case' => 'Nom', // Токен того кто отправляет
            'access_token' => $token, // Токен того кто отправляет
            'v' => '5.85',
        );

        // В $result вернется id отправленного сообщения

        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'proxy' => $proxy,
                'request_fulluri' => true,
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));

        return json_decode($result, true);

    }

    /**
     * @param $method
     * @param $akkount_id
     * @param array $params
     * @return mixed|null
     */
    static function request($method, $proxy, $params = [])
    {
        $url = 'https://api.vk.com/method/' . $method;
        $token = Settings::find()->where(['key' => 'vk_access_token'])->one()->value;

        $params['access_token'] = $token;
//        $params['count'] = 10;
        $params['v'] = '5.85';

        $result = file_get_contents($url, false, stream_context_create([
            'http' => [
                'proxy' => $proxy,
                'request_fulluri' => true,
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            ]
        ]));

        return $result ? Json::decode($result, true) : null;

    }


    public function actionSendMessage($userVk, $text, $token, $proxy)
    {
        $this->send($userVk, $text, $token, $proxy);
    }

    public function actionTestmsg($id){
//            $id = 4270;
        $user = DispatchStatus::find()->where(['id' => $id])->one();
//        VarDumper::dump($user, 10, true);
        $msg = 'Привет {%msgRecipient%}! Меня зовут {%msgSender%}. Имя рассылки {%dispatchName%}, твой город {%cityRecipient%}';
        $message = $this->replacingTags($msg, $user);
        return $message;
    }

    public function actionChatsls($id)
    {
        $clients = DispatchStatus::find()->where(['id' => $id])->one();
        if (!$clients) {
            return ['status' => false, 'errors' => 'no client'];
        }
        $account = DispatchRegist::find()->where(['id' => $clients->send_account_id])->one();
        if (!$account) {
            return ['status' => false, 'errors' => 'no account'];
        }


        //$results = getHistory($from_id,$token,$send_proxy);

        $myHistory = Message::find()->where([
            'dispatch_registr_id' =>  $account->id,
            'dispatch_status_id' =>  $clients->id,
        ])->all();


        return ['status' => true, 'errors' => null,'account'=>$account->id, 'history'=>$myHistory];
    }

    public function actionSendsls()
    {
        $clients = DispatchStatus::find()->where(['id' => $_POST['id']])->one();
        if (!$clients) {
            return ['status' => false, 'errors' => 'no client'];
        }
        $account = DispatchRegist::find()->where(['id' => $clients->send_account_id])->one();
        if (!$account) {
            return ['status' => false, 'errors' => 'no account'];
        }


        //$results = getHistory($from_id,$token,$send_proxy);

        $myHistory = Message::find()->where([
            'dispatch_registr_id' =>  $account->id,
            'dispatch_status_id' =>  $clients->id,
        ])->all();

        $from_id =  $clients->account_id;
        $token = $account->token;
        $message = $_POST['message'];
        $id = trim($_POST['id']);

        // Взять аккаунт доступен для отправки
        /** @var DispatchRegist $account */
        $proxy = Proxy::findOne(['id' => $account->proxy]);
        $send_proxy = "tcp://{$proxy->ip_adress}:{$proxy->port}";

        $result = send($from_id, $message, $token,$send_proxy);



        if ($result['response'] != null) {

            $dispatch = Dispatch::find()->where(['id' => $clients->dispatch_id])->one();
            /** @var DispatchStatus $status */

            $status = DispatchStatus::findOne(['id' => $id]);
            $status->new_message = false;
            $status->response_id = $result['response'];
            $status->data = date("Y-m-d H:i:s");
            $status->update();

            /** @var Message $mesHistory */
            $mesHistory = new Message();
            $mesHistory->date_time = date("Y-m-d H:i:s");
            $mesHistory->company_id = $account->company_id;
            $mesHistory->dispatch_id = $status->dispatch_id;
            $mesHistory->dispatch_registr_id = $account->id;
            $mesHistory->dispatch_status_id = $status->id;
            if ($dispatch->text_data) $mesHistory->database_id = $dispatch->text_data;
            $mesHistory->text = $message;
            $mesHistory->from = Message::FROM_ACCOUNT;
            $mesHistory->save();


            return ['status' => true, 'errors' => null,'info'=>'сообщение отправленно'];


        } else {
            return ['status' => false, 'errors' => 'Акк заблокирован', 'info'=>$result['response']];

        }




    }



}
