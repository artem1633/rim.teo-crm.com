<?php

namespace app\modules\page\controllers;

use app\models\Command;
use Symfony\Component\Console\Question\Question;
use Yii;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use app\models\Questionary;
use app\models\Questions;
use app\models\Applications;
use app\models\Resume;
use app\models\additional\Contacts;
use yii\web\NotFoundHttpException;
use app\models\Users;
use app\models\Settings;
use yii\web\Response;

/**
 * Default controller for the `page` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        if (!isset($_GET['link'])) throw new NotFoundHttpException('The requested page does not exist.');
        $questionary = Questionary::find()->where(['link' => $_GET['link'] ])->one();
        $post = $request->post();
        $model = new Resume();
        $fio = '';
        $balls = 0;
        $session = Yii::$app->session;
        $questions = Questions::find()->where(['questionary_id' => $questionary->id])->orderBy([ 'ordering' => SORT_ASC])->all();
        $setting = Settings::find()->where(['key' => 'telegram_chat'])->one();


        if($questionary != null){

            $result = [];
            if($post){
                foreach ($questions as $question) {
                    $key = $question->getTypeCode() . $question->id;
                    $own_option = isset($post['own_option' . $question->id]) ? $post['own_option' . $question->id] : null;

                    if(isset($post[$key])){
                        if($question->type == 0 || $question->type == 1 || $question->type == 2 || $question->type == 5 || $question->type == 7){
                            if($question->type == 0) $fio = $post[$key];
                            //$balls += $questionary->ball_for_question;
                            $result [] = [
                                'question' => $question->id,
                                'value' => $post[$key],
                                'own_option' => $own_option,
                            ];
                        }else{
                            $keys = [];
                            if($question->type == 3){
                                foreach ($post[$key] as $value) {
                                    $keys [] = [
                                        'key' => $value,
                                    ];
                                    foreach (json_decode($question->individual) as $individual) {
                                        if($individual->value == $value) $balls += $individual->ball;
                                    }
                                }
                            }else{
                                foreach ($post[$key] as $value) {
                                    $keys [] = [
                                        'key' => $value,
                                    ];
                                    foreach (json_decode($question->multiple) as $multiple) {
                                        if($multiple->question == $value) $balls += $multiple->ball;
                                    }
                                }
                            }
                            $result [] = [
                                'question' => $question->id,
                                'value' => json_encode($keys),
                                'own_option' => $own_option,
                            ];
                        }

                    }else{
                        if($question->type == 8)
                        {
                            $images = [];
                            if($_FILES["many_files"]["tmp_name"] != null) {
                                $i=-1;
                                foreach ($_FILES["many_files"]["tmp_name"] as $value) {
                                    $i++;
                                    $fileName = $_FILES["many_files"]["name"][$i];
                                    $targetFilePath = 'uploads/images/' . $fileName;
                                    $status = move_uploaded_file($value, $targetFilePath);
                                    if($status == 1)
                                    {
                                        $images [] = [
                                            'image_name' => $fileName,
                                        ];
                                    }
                                }
                                $result [] = [
                                    'question' => $question->id,
                                    'value' => json_encode($images),
                                    'own_option' => $own_option,
                                ];
                            }
                        }
                        if($question->type == 6){
                            if($_FILES["avatar"]["tmp_name"] != null) {
                                $i=-1;
                                foreach ($_FILES["avatar"]["tmp_name"] as $value) {
                                    $i++;
                                    $fileName = $_FILES["avatar"]["name"][$i];
                                    $targetFilePath = 'avatars/' . $fileName;
                                    $status = move_uploaded_file($value, $targetFilePath);
                                    if($status == 1) $avatar = $targetFilePath;
                                    else $avatar = 'images/nouser.png';
                                }
                                $result [] = [
                                    'question' => $question->id,
                                    'value' => $avatar,
                                    'own_option' => $own_option,
                                ];
                            }
                            else{
                                $result [] = [
                                    'question' => $question->id,
                                    'value' => '',
                                    'own_option' => $own_option,
                                ];
                            }
                        }
                    }
                }

                $model->questionary_id = $questionary->id;
                $model->group_id = null;
                $model->status_id = null;
                $model->fit = null;
                $model->category = null;
                $model->is_new = 1;
                $model->mark = null;
                $model->telegram_chat_id = null;
                $model->connect_telegram = null;
                $model->new_sms = 1;
                $model->vacancy_id = $questionary->vacancy_id;
                $model->correspondence = 0;
                $model->balls = $balls;
                $model->values = json_encode($result);
                $model->fio = $fio;
                $model->avatar = $avatar;
                $model->show_in_shop = $post['show_in_shop'];
                $model->ip = $_SERVER['REMOTE_ADDR'];
                if (isset($_GET['doptest'])) $model->doptest = $_GET['doptest'];
                $proverka = 0;
                // $lastResume = Resume::find()->where(['questionary_id' => $questionary->id])->orderBy(['id' => SORT_DESC ])->one();
                $lastResume = null;
                if($lastResume == null) $proverka = 1;
                else{
                    if( (time() - strtotime($lastResume->date_cr)) > 1 ) $proverka = 1;
                }

                if($proverka){
                    if($model->validate() && $model->save()){
                        Yii::$app->db->createCommand()->update('questionary',
                            [ 'filling_count' => $questionary->filling_count + 1 ],
                            [ 'id' => $questionary->id ])
                            ->execute();

                        $user = Users::findOne($questionary->user_id);
                        $user->main_balance = $user->main_balance - $user->resume_sum;
                        $user->save();

//                        file_get_contents("https://demo.teo-job.ru/api/botinfo/newresume?resume=".$model->id);

                        $setting = Settings::find()->where(['key' => 'text_after_filling'])->one();
                        $text = str_replace ("{unique_code_for_telegram}", $model->code , $setting->text);
//                        Yii::$app->session->setFlash('success', "Успешно отправлено. <br> ".$text);
                        $session['questionaries'] = null;
                        if( $session['questionaries'] == null || $session['questionaries'] == '' ) $session['questionaries'] = ''.$questionary->id;
                        else $session['questionaries'] .= ',' . $questionary->id;
                        return $this->redirect(['/'.$_GET['link']]);
                    }
                }
                else {
//                    Yii::$app->session->setFlash('error', "Вы уже отправили один ответ в течение часа.");
                    return $this->redirect(['/'.$_GET['link']]);
                }
            }
            else{
                Yii::$app->db->createCommand()->update('questionary',
                    [ 'count' => $questionary->count + 1 ],
                    [ 'id' => $questionary->id ])
                    ->execute();



                if($questionary->type == 2) {
                    $proverka = 0;
                    $lastResume = null;
                    // $lastResume = Resume::find()->where(['questionary_id' => $questionary->id])->orderBy(['id' => SORT_DESC ])->one();
                    if($lastResume == null) $proverka = 1;
                    else{
                        // if( (time() - strtotime($lastResume->date_cr)) > 0 ) $proverka = 1;
                    }

                    if($proverka){
                        if($session['end'] === 1){
//                                $session['question'] = null;
//                                if($session['watch'] == 1){
//                                $session['end'] = 0;
//                                $session['begin_time'] = null;
//                                }

//                            $resume = Resume::find()->where(['ip' => $_SERVER['REMOTE_ADDR']])->orderBy(['id' => SORT_DESC ])->one();
                            $r = Resume::find()->where(['questionary_id' => $questionary->id])->orderBy(['id' => SORT_DESC ])->one();
                            if($r && $questionary->id == $session['questionary']->id){
                                return $this->render('result', []);
                            } elseif(!$r && !$questionary->id){
                                $r = Resume::find()->where(['questionary_id' => $questionary->id])->orderBy(['id' => SORT_DESC ])->one();

                                // var_dump($r);
                                // exit;

                                $answers = [];
                                $i = 1;
                                foreach ($questions as $value)
                                {
                                    if($i == 1) {
                                        $question = Questions::findOne($value->id);
                                        $session['question'] = $question;
                                        $session['test_time'] = time() + $question->time;
                                    }
                                    $answers [] = [
                                        'turn' => $i++,
                                        'question_id' => $value->id,
                                        'answer' => null,
                                        'time' => null,
                                        'truth' => false,
                                    ];
                                }
                            }
                        }
                        if($session['begin_time'] === null || $session['question'] == null || $questionary->id != $session['questionary']->id){
                            $answers = [];
                            $i = 1;
                            foreach ($questions as $value)
                            {
                                if($i == 1) {
                                    $question = Questions::findOne($value->id);
                                    $session['question'] = $question;
                                    $session['test_time'] = time() + $question->time;
                                }
                                $answers [] = [
                                    'turn' => $i++,
                                    'question_id' => $value->id,
                                    'answer' => null,
                                    'time' => null,
                                    'truth' => false,
                                ];
                            }
//                                VarDumper::dump($questions, 10, true);
//                                exit;
                            $session['questionary'] = $questionary;
                            $session['questions'] = $questions;
                            $session['answers'] = $answers;
                            $session['link'] = $_GET['link'];
                            $session['max_test_count'] = $i - 1;
                            $session['begin_time'] = time();
                            $session['process'] = 0;
                            $session['turn'] = 1;
                            $session['end'] = 0;
                            if (isset($_GET['doptest'])) $session['doptest'] = $_GET['doptest'];
//                                $session['test_time'] = time() + $questionary->time_test * 60;
                        }



                        /*echo "<pre>";
                        print_r($session['answers']);
                        echo "</pre>";
                        die;*/


                        return $this->render('test', []);
                    }
                    else {
//                        Yii::$app->session->setFlash('error', "Вы уже отправили один ответ в течение часа.");
                        return $this->render('result', [
                            'lastResume' => $lastResume,
                        ]);
                    }
                }
                else {
                    return $this->render('@app/views/questions/link', [
                        'questionary' => $questionary,
                        'questions' => $questions,
                    ]);
                }
            }
        }
        else
        {
            $user = Users::find()->where(['utm' => $_GET['link']])->one();
            $questionary = Questionary::find()->where([ 'user_id' => $user->id, 'publish_company' => 1])->all();


            if($user != null ){
                return $this->render('index', [
                    'questionary'=>$questionary,
                    'user' => $user,
                ]);
            }
            else
            {
                $resume = Resume::find()->where(['code' => $_GET['link']])->one();
                $active = [];
                if($resume != null )
                {
                    foreach (json_decode($resume->values) as $value) {
                        $question = Questions::findOne($value->question);
                        if($question->type == 0 || $question->type == 6 || $question->general_access != 1){
                            if($question != null){
                                $name = $question->getQuestions($value->value, $value->own_option, $resume->ball_for_question, $resume);

                                $active += [
                                    $question->id => ['content' => $name],
                                ];
                            }
                        }
                    }
                    return $this->render('resume', [
                        'active'=>$active,
                    ]);
                }
                else
                {
                    return $this->render('error', [ ]);
                }
            }
        }
    }

    public function actionShowSession()
    {
        $session = Yii::$app->session;
        VarDumper::dump($session['answers'], 10, true);
        echo "<br>END<br>";
        VarDumper::dump($session['end'], 10, true);
        echo "<br>BEGIN TIME<br>";
        VarDumper::dump($session['begin_time'], 10, true);
        echo "<br>QUESTION<br>";
        VarDumper::dump($session['question'], 10, true);
        echo "<br>QUESTIONARY<br>";
        VarDumper::dump($session['questionary'], 10, true);

        exit;
    }

    public function actionCheckDoneByCommand($questionary_id, $command_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $resume = Resume::find()->where(['questionary_id' => $questionary_id, 'command_id' => $command_id])->one();

        return $resume ? ['message' => 'Команда уже проходила этот тест. Повторное прохождение теста невозможно'] : ['message' => null];
    }

    /**
     * @param $word
     * @param $command
     * @return array
     */
    public function actionCheckCode($word, $command, $questionary_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $command = Command::findOne($command);

        if($word == $command->code_word){

            $resume = Resume::find()->where(['questionary_id' => $questionary_id, 'command_id' => $command->id])->one();

            if($resume){
                return ['error' => 'Команда уже проходила этот тест. Повторное прохождение теста невозможно'];
            }

            return ['success' => true];
        }

        return ['error' => 'Не верно, попробуйте еще раз!'];
    }

    public function actionTest2(){
        $balls = 0;
//        $questions = Questions::find()->one();
        $questions = Questions::find()->all();

        foreach ($questions as $question) {
            $key = $question->getTypeCode() . $question->id;
            $own_option = isset($post['own_option' . $question->id]) ? $post['own_option' . $question->id] : null;

            if(isset($post[$key])){
                if($question->type == 0 || $question->type == 1 || $question->type == 2 || $question->type == 5 || $question->type == 7){
                    if($question->type == 0) $fio = $post[$key];
                    //$balls += $questionary->ball_for_question;
                    $result [] = [
                        'question' => $question->id,
                        'value' => $post[$key],
                        'own_option' => $own_option,
                    ];
                }else{
                    $keys = [];
                    if($question->type == 3){
                        foreach ($post[$key] as $value) {
                            $keys [] = [
                                'key' => $value,
                            ];
                            foreach (json_decode($question->individual) as $individual) {
                                if($individual->value == $value) $balls += $individual->ball;
                            }
                        }
                    }else{
                        foreach ($post[$key] as $value) {
                            $keys [] = [
                                'key' => $value,
                            ];
                            foreach (json_decode($question->multiple) as $multiple) {
                                if($multiple->question == $value) $balls += $multiple->ball;
                            }
                        }
                    }
                    $result [] = [
                        'question' => $question->id,
                        'value' => json_encode($keys),
                        'own_option' => $own_option,
                    ];
                }

            }else{
                echo '123';
            }
        }

        var_dump($balls);
        exit;
    }

    /**
     * @param $id
     * @param $turn
     * @param $value
     * @param $time
     */
    public function actionSetValues($id, $turn, $value, $time)
    {
        $session = Yii::$app->session;
        $answers = [];
        foreach ($session['answers'] as $answer) {
            if( $turn == $answer['turn'] && $id == $answer['question_id'] ) {

                $truth = false;

                $question = \app\models\Questions::findOne($answer['question_id']);

                if($question){
                    $number = 1;
                    foreach (json_decode($question->individual) as $question){
                        if($number == $value){
                            if($question->balls > 0){
                                $truth = true;
                            }
                        }
                        $number++;
                    }
                }


                $answers [] = [
                    'turn' => $answer['turn'],
                    'question_id' => $answer['question_id'],
                    'answer' => $value,
                    'time' => $time,
                    'truth' => $truth,
                ];
            }
            else{
                $answers [] = [
                    'turn' => $answer['turn'],
                    'question_id' => $answer['question_id'],
                    'answer' => $answer['answer'],
                    'time' => $answer['time'],
                    'truth' => $answer['truth'],
                ];
            }
        }

        $session['answers'] = null;
        $session['answers'] = $answers;
    }

    public function actionTest()
    {
        $session = Yii::$app->session;
        VarDumper::dump($session['answers'], 10, true);
    }

    /**
     * @param $end
     */
    public function actionEnd($end)
    {
        $session = Yii::$app->session;
        if($end == 1)
        {
            $result = []; $fio = ''; $balls = 0;
            $model = new Resume();
            $avatar = 'images/nouser.png';


            foreach ($session['answers'] as $answer)
            {
                $question = Questions::findOne($answer['question_id']);
                if($question != null)
                {
                    if($question->type == 0 || $question->type == 1 || $question->type == 2 || $question->type == 5 || $question->type == 6 || $question->type == 7 || $question->type == 8){
                        if($question->type == 0) $fio = $answer['answer'];
                        if($question->type == 6 && $answer['answer'] !== null) $avatar = $answer['answer'];
                        $result [] = [
                            'question' => $question->id,
                            'value' => $answer['answer'],
                            'time' => $answer['time'],
                            'truth' => $answer['truth'],
                        ];
                    }else{
                        $keys = [];
                        if($question->type == 3){
                            foreach (explode(',', $answer['answer']) as $value) {
                                $i = 0;
                                foreach (json_decode($question->individual) as $individual) {
                                    $i++;
                                    if($i == $value) {
                                        if($question->command_specify == 0){
                                            if($question->fast_time > $answer['time']){
                                                if($individual->ball > 0){
                                                    $balls += $question->fast_balls + $individual->ball;
                                                }
                                            } else {
                                                $balls += $individual->ball;
                                            }
                                        }

                                        $keys [] = [
                                            'key' => $individual->value,
                                        ];
                                    }
                                }
                            }
                        }else{
                            foreach (explode(',', $answer['answer']) as $value) {
                                $i = 0;
                                foreach (json_decode($question->multiple) as $multiple) {
                                    $i++;
                                    if($i == $value) {
                                        if($question->command_specify == 0){
                                            if($question->fast_time > $answer['time']){
                                                if($multiple->ball > 0){
                                                    $balls += $question->fast_balls + $multiple->ball;
                                                }
                                            } else {
                                                $balls += $multiple->ball;
                                            }
                                        }
                                        $keys [] = [
                                            'key' => $multiple->question,
                                        ];
                                    }
                                }
                            }
                        }
                        $result [] = [
                            'question' => $question->id,
                            'value' => json_encode($keys),
                            'time' => $answer['time']
                        ];
                    }

                }
            }

            foreach($session['questions'] as $question){
                if($question->command_specify == 1){
                    foreach ($session['answers'] as $answer){
                        if($answer['question_id'] == $question->id){
                            $model->command_id = $answer['answer'];
                        }
                    }
                }
            }

            if(Resume::find()->where(['command_id' => $model->command_id, 'questionary_id' => $session['questionary']->id])->count() > 0){
                return;
            }


            $command = Command::findOne($model->command_id);
            if($command){
                $command->balls += $balls;
                $command->save(false);
            }



            $model->questionary_id = $session['questionary']->id;
            $model->group_id = null;
            $model->status_id = null;
            $model->fit = null;
            $model->category = null;
            $model->is_new = 1;
            $model->mark = null;
            $model->telegram_chat_id = null;
            $model->connect_telegram = null;
            $model->new_sms = 1;
            $model->vacancy_id = $session['questionary']->vacancy_id;
            $model->correspondence = 0;
            $model->balls = $balls;
            $model->values = json_encode($result);
            $model->fio = $fio;
            $model->avatar = $avatar;
            $model->time_spent = time() - $session['begin_time'];
            $model->delivered = null;
            $model->ip = $_SERVER['REMOTE_ADDR'];
            $model->doptest = $session['doptest'];



            if($model->validate() && $model->save()) {
                Yii::$app->db->createCommand()->update('questionary',
                    [ 'filling_count' => $session['questionary']->filling_count + 1 ],
                    [ 'id' => $session['questionary']->id ])
                    ->execute();
//                file_get_contents("https://demo.teo-job.ru/api/botinfo/newresume?resume=".$model->id);
                $setting = Settings::find()->where(['key' => 'text_after_filling'])->one();
                $text = str_replace ("{unique_code_for_telegram}", $model->code , $setting->text);
//                Yii::$app->session->setFlash('success', "Успешно отправлено. <br> ".$text);
                $session['resume'] = $model;
                $session['end'] = 1;
                $session['begin_time'] = null;
                $session['question'] = null;
                if( $session['questionaries'] === null) $session['questionaries'] = ''.$session['questionary']->id;
                else $session['questionaries'] .= ',' . $session['questionary']->id;
            }
        }
    }

    public function actionStandings()
    {
        return $this->render('standings');
    }

    /**
     * @return string
     */
    public function actionSetLogo()
    {
        if(isset($_POST) == true){
            //generate unique file name
            $id = $_POST['id'];
            $turn = $_POST['turn'];
            $fileName = time().'_'.basename($_FILES["file"]["name"]);

            //file upload path
            $targetDir = "avatars/";
            $targetFilePath = $targetDir . $fileName;

            //allow certain file formats
            $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
            $allowTypes = array('jpg','png','jpeg','gif');

            if(in_array($fileType, $allowTypes)){
                //upload file to server
                if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){
                    //insert file data into the database if needed
                    $session = Yii::$app->session;
                    $answers = [];
                    foreach ($session['answers'] as $answer) {
                        if( $turn == $answer['turn'] && $id == $answer['question_id'] ) {
                            $answers [] = [
                                'turn' => $answer['turn'],
                                'question_id' => $answer['question_id'],
                                'answer' => $targetFilePath,
                                'time' => $answer['time'],
                                'truth' => $answer['truth'],
                            ];
                        }
                        else{
                            $answers [] = [
                                'turn' => $answer['turn'],
                                'question_id' => $answer['question_id'],
                                'answer' => $answer['answer'],
                                'time' => $answer['time'],
                                'truth' => $answer['truth']
                            ];
                        }
                    }

                    $session['answers'] = null;
                    $session['answers'] = $answers;
                    $response['status'] = 'ok';
                }else{
                    $response['status'] = 'err';
                }
            }else{
                $response['status'] = 'type_err';
            }

            //render response data in JSON format
            return json_encode($response);
        }
    }

    public function actionGetTime()
    {
        return time();
    }

    public function actionChange($id, $turn)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $session = Yii::$app->session;
        $session['turn'] = $turn;

        $answerCount = 0;
        foreach ($session['answers'] as $value) {
            if($value['answer'] !== null) $answerCount++;
            if($turn == $value['turn']) $question_id = $value['question_id'];
        }

        if($answerCount == 0) $process = 0;
        else $process = (int)($answerCount / $session['max_test_count'] * 100);
        $session['process'] = $process;

        if($id != -1){
            $question = Questions::findOne($id);
            $session['question'] = $question;
        } else {
            $question = Questions::findOne($question_id);
            $session['question'] = $question;
        }

        $session['test_time'] = time() + $question->time;

        return ['test_time' => $session['test_time']];
    }

    public function actionPolicy()
    {
        return $this->render('policy');
    }

    public function actionSetManyFiles()
    {
        if(isset($_POST) == true){
            //generate unique file name
            $id = $_POST['id'];
            $turn = $_POST['turn'];
            $fileName = time().'_'.basename($_FILES["file"]["name"]);

            //file upload path
            $targetDir = "uploads/images/";
            $targetFilePath = $targetDir . $fileName;

            //allow certain file formats
            $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
            $allowTypes = array('jpg','png','jpeg','gif');

            if(in_array($fileType, $allowTypes)){
                //upload file to server
                if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){
                    //insert file data into the database if needed
                    $session = Yii::$app->session;
                    $answers = [];
                    foreach ($session['answers'] as $answer) {
                        if( $turn == $answer['turn'] && $id == $answer['question_id'] ) {
                            $value = json_decode($answer['answer']);
                            $value [] = [
                                'image_name' => $fileName,
                            ];
                            $answers [] = [
                                'turn' => $answer['turn'],
                                'question_id' => $answer['question_id'],
                                'answer' => json_encode($value),
                                'time' => $answer['time'],
                            ];
                        }
                        else{
                            $answers [] = [
                                'turn' => $answer['turn'],
                                'question_id' => $answer['question_id'],
                                'answer' => $answer['answer'],
                                'time' => $answer['time'],
                            ];
                        }
                    }

                    $session['answers'] = null;
                    $session['answers'] = $answers;
                    $response['status'] = 'ok';
                }else{
                    $response['status'] = 'err';
                }
            }else{
                $response['status'] = 'type_err';
            }

            //render response data in JSON format
            return json_encode($response);
        }
    }
}
