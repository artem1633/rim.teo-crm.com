<?php

use yii\helpers\Html;
use app\models\Questionary;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use app\models\Resume;
use app\models\Command;
use php_rutils\RUtils;

$_csrf = \Yii::$app->request->getCsrfToken();
$session = Yii::$app->session;
$question = $session['question'];
$turn = $session['turn'];
if (!file_exists('avatars/'.$session['answers'][$turn - 1]['answer']) || $session['answers'][$turn - 1]['answer'] === null) {
    $path = 'https://' . $_SERVER['SERVER_NAME'].'/images/nouser.png';
} else {
    $path = 'https://' . $_SERVER['SERVER_NAME'] . '/avatars/' . $session['answers'][$turn - 1]['answer'];
}


$questionary = Questionary::findOne($question->questionary_id);


$ball = $session['resume']->balls + $model->ball_for_question;


?>

    <style>
        h3 {
            font-family: "CoreSans-Bold";
            color: #005b6e;
            display: inline-block;
            font-size: 18px;
            width: 200px;
        }
    </style>

    <div class="test-container lightmode">
        <div class="test-box animated fadeInDown" style="width: unset;">
            <div class="test-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="process-bar">
                            <?php
                            $i = 0;
                            foreach ($session['answers'] as $value) {
                                $button = '';
                                $i++;
                                if($value['time'] == null) $button = 'process-item'; else $button = 'process-item item-success';
                                ?>
                                <div class="<?=$button?>"></div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="text-center" style="margin-top: 100px;">
                            <img src="/img/won-img.png" alt="" style="display: inline-block; height: 100px; width: 100%; object-fit: contain;">
                            <h3 style="margin-top: 10px; font-size: 25px; width: 100%;">Вы заработали <?= RUtils::numeral()->getPlural($ball, ['балл', 'балла', 'баллов']) ?>.</h3>
                            <h3 style="margin-top: 0; font-size: 25px; width: 100%;">Следите за турнирной таблицей!</h3>
                            <p style="margin-top: 15px; color: #005b6e; font-size: 14px;">Правильные ответы будут доступны на сайте после 15 июня </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php

if (class_exists('yii\debug\Module')) {
    $this->off(\yii\web\View::EVENT_END_BODY, [\yii\debug\Module::getInstance(), 'renderToolbar']);
}



?>