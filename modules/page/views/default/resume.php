<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\Pjax;
use kartik\sortinput\SortableInput;
use yii\widgets\ActiveForm;
use app\models\Tags;


CrudAsset::register($this);
$this->title = 'Блок вопросов';
$this->params['breadcrumbs'][] = $this->title;

?>
    <div class="container">
        <div class="col-md-12" style="padding-top: 20px;">
            <div class="panel panel-success panel-hidden-controls" >
                <div class="panel-heading ui-draggable-handle">
                    <h1 class="panel-title">
                        <b>Ответы на вопросы</b>
                    </h1>
                </div>
                <div class="panel-body">
                    <div style=" max-height: 600px; overflow: auto;">
                        <?= SortableInput::widget([
                            'name'=>'active',
                            'id'=>'actives',
                            'items' => $active,
                            'hideInput' => true,
                            'sortableOptions' => [
                                'connected'=>true,
                                'itemOptions'=>['class'=>'', 'style' => 'background-color: #dff0d8;'],
                            ],
                            'options' => ['class'=>'form-control', 'readonly'=>true]
                        ])?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php

if (class_exists('yii\debug\Module')) {
    $this->off(\yii\web\View::EVENT_END_BODY, [\yii\debug\Module::getInstance(), 'renderToolbar']);
}

?>