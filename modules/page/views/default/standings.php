<?php

use yii\helpers\Html;
use app\models\Questionary;
use yii\widgets\Pjax;
use app\models\SettingResult;
use app\models\Command;


$_csrf = \Yii::$app->request->getCsrfToken();
$session = Yii::$app->session;

$settingResults = SettingResult::find()->where(['questionary_id' => $session['resume']->questionary->id])->all();
$ball = $session['resume']->balls + $model->ball_for_question;

$command = Command::findOne($session['resume']->command_id);

\app\assets\StandingsAsset::register($this);

/** @var Command[] $commands */
$commands = Command::find()->orderBy('balls desc')->limit(1000)->all();

$currentCommandNumber = 0;


$counter = 1;
foreach ($commands as $command){
    if($command->id == $session['resume']->command_id){
        $currentCommandNumber = $counter;
    }
    $counter++;
}

$this->title = "Турнирная таблица";

?>

<img class="img-background" src="/img/backgrounds/standings.jpg" alt="">

<div class="filters">
    <div class="row" style="padding: 0px 20%;">
        <div class="col-md-12">
            <label for="">ПОИСК КОМАНДЫ:</label>
            <input id="search-input" type="text" placeholder="Введите название вашей команды">
            <button id="search-btn" class="search-btn">Найти</button>
            <span>Ваше место: <span id="command-number"><?=$currentCommandNumber?></span></span>
        </div>
        <div class="col-md-12">
            <table style="width: 100%;">
                <thead style="padding: 0px 20%;">
                <tr>
                    <th style="width: 30%;">Место</th>
                    <th style="width: 30%;">Команда</th>
                    <th style="width: 30%;">Баллы</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<div class="row" style="padding: 0px 20%;">
    <div class="col-md-12">
        <table style="width: 100%; margin-top: 0; border-spacing: 0 5px;">
            <tbody id="search-body-body">
            <?php $counter = 1; foreach($commands as $command): ?>
                <tr data-name="<?=$command->name?>" class="animated fadeInUp fixed-row<?=($counter <= 10 ? ' row-warning' : '')?>">
                    <td><?=$counter?><?=($counter <= 10 ? ' <i class="fa fa-star"></i>' : '')?></td>
                    <td><?=$command->name?></td>
                    <td><?=$command->balls?></td>
                </tr>
                <?php $counter++; endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<?php

if (class_exists('yii\debug\Module')) {
    $this->off(\yii\web\View::EVENT_END_BODY, [\yii\debug\Module::getInstance(), 'renderToolbar']);
}

?>