<?php

use yii\helpers\Html;
use app\models\Questionary;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use app\models\Resume;
use app\models\Command;

$_csrf = \Yii::$app->request->getCsrfToken();
$session = Yii::$app->session;
$question = $session['question'];
$turn = $session['turn'];
if (!file_exists('avatars/'.$session['answers'][$turn - 1]['answer']) || $session['answers'][$turn - 1]['answer'] === null) {
    $path = 'https://' . $_SERVER['SERVER_NAME'].'/images/nouser.png';
} else {
    $path = 'https://' . $_SERVER['SERVER_NAME'] . '/avatars/' . $session['answers'][$turn - 1]['answer'];
}


$questionary = Questionary::findOne($question->questionary_id);

$nextLabel = ' Следующий';

//foreach($session['answers'] as $value){
//    if($value['time'] == null){
//        $nextLabel = ' Начать!';
//    }
//    break;
//}


if($turn == 1){
    $nextLabel = 'Начать';
}

?>

    <style>
        h3 {
            font-family: "CoreSans-Bold";
            color: #005b6e;
            font-size: 22px;
        }

        p, label {
            font-family: "CoreSans-Regular";
            color: #005b6e;
            font-size: 16px;
        }

        .btn {
            font-family: "CoreSans-Bold" !important;
        }
    </style>

    <style>
        .panel .panel-body.panel-body-half .panel-body.panel-body-image .panel-body-inform {
            bottom: auto;
            left: auto;
            right: -15px;
            top: 50%;
            margin-left: 0px;
            margin-top: -15px;
        }
        .panel .panel-title {
            font-size: 16px;
            font-weight: 400;
            line-height: 30px;
            display: block;
            float: left;
            color: #1b1e24;
        }
        .panel .panel-footer {
            background: #F5F5F5;
            border: 0px;
            border-top: 1px solid #E3E3E3;
            line-height: 30px;
            padding: 10px;
        }
        .panel-default .panel-heading,
        .panel-primary .panel-heading,
        .panel-success .panel-heading,
        .panel-info .panel-heading,
        .panel-warning .panel-heading,
        .panel-danger .panel-heading {
            background: #F5F5F5;
            border-color: #E5E5E5;
        }
        .panel-fullscreen-wrap {
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 999;
            background: #FFF;
        }
        .panel-fullscreen-wrap .panel {
            -moz-border-radius: 0px;
            -webkit-border-radius: 0px;
            border-radius: 0px;
            margin: 0px;
        }
        .panel-fullscreen-wrap .panel .panel-body {
            overflow-y: scroll;
        }
        /* PANEL GROUP / ACCORDION */
        .panel-group {
            float: left;
            width: 100%;
        }
        .panel-group .panel-heading + .panel-collapse > .panel-body {
            border-top: 0px;
        }
        .panel-title > a {
            text-decoration: none;
        }
        .panel-group.accordion .panel-body {
            display: none;
        }
        .panel-group.accordion .panel-body.panel-body-open {
            display: block;
        }
    </style>

    <div class="test-container lightmode">
        <div class="test-box animated fadeInDown" style="width: unset;">
            <div class="test-body">

                <div class="row">
                    <?php Pjax::begin(['enablePushState' => false, 'id' => 'question-pjax']) ?>
                    <div class="col-md-12 col-sm-12">
                        <div class="process-bar">
                            <?php
                            $i = 0;
                            foreach ($session['answers'] as $value) {
                                $button = '';
                                $i++;
                                if($value['time'] == null) $button = 'process-item'; else $button = 'process-item item-success';
                                ?>
                                <div class="<?=$button?>"></div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <div class="panel panel-warning">
                            <div class="panel-body text-left">
                                <div>
                                    <div class="col-md-12">
                                        <h3><?=$question->name?> </h3>
                                        <br>
                                    </div>
                                    <h4><?=$question->description?></h4>
                                    <?php if($question->command_specify): ?>
                                        <p style="padding-left: 10px;">Выберите из списка название вашей команды и впишите фамилию капитана, указанные при регистрации, соблюдая регистры, наличие пробелов и символов.
                                            Если вы не можете их вспомнить, свяжитесь со службой поддержки, нажав на окно в правом нижнем углу сайта, или напишите нам на info@familymoneyfest.ru
                                        </p>
                                    <?php endif; ?>
                                    <br>
                                    <?php
                                    if($question->type == 0)
                                    {
                                        ?>
                                        <div class="col-md-12">
                                            <?php if($question->command_specify): ?>
                                                <?= \kartik\select2\Select2::widget([
                                                    'id' => 'default'.$question->id,
                                                    'name' => 'default'.$question->id,
                                                    'value' => $session['answers'][$turn - 1]['answer'],
                                                    'data' => ArrayHelper::map(Command::find()->all(), 'id', 'name'),
                                                    'pluginOptions' => [
                                                        'tags' => true,
                                                        'placeholder' => 'Выберите название команды'
//                                                        'allowClear' => true,
                                                    ],
                                                    'pluginEvents' => [
                                                        "change" => "function() {
                                                            
                                                            $.get('/page/default/check-done-by-command',  {'questionary_id':".$questionary->id.", 'command_id': $('#default".$question->id."').val()}, function(response){

                                                                if(response.message != null){
                                                                    //document.getElementById('demo').innerHTML = response.message;
                                                                } else {
                                                                    $.get('/page/default/set-values', {'id':".$question->id.", 'turn': ".$turn.", 'value':$('#default".$question->id."').val(), 'time': timePass}, function(data){ timePass = 1; } );
                                                                }

                                                            });
                                                        }",
                                                    ],
                                                    'options' => [
                                                        'data-command' => 'command'
                                                    ],
                                                ]) ?>

                                                <input id="code_word" placeholder="Фамилия капитана" type="text" class="form-control" style="margin-top: 17px; width: 100%; float: left;"><button id="code_word_btn" class="btn btn-success" style="float: left; margin-top: 17px; margin-left: 12px;">Начать</button>
                                            <?php else: ?>
                                                <textarea name="default<?=$question->id?>" id="default<?=$question->id?>" <?= $question->require ? 'required="required"' : '' ?> class="form-control" onchange="$.get('/page/default/set-values', {'id':<?=$question->id?>, 'turn': <?=$turn?>, 'value':$('#default<?=$question->id?>').val(), 'time': timePass}, function(data){ timePass = 1; } );" rows="1"><?=$session['answers'][$turn - 1]['answer']?></textarea>
                                            <?php endif; ?>
                                        </div>
                                        <?php
                                    }
                                    if($question->type == 1)
                                    {
                                        ?>
                                        <div class="col-md-12">
                                            <?php if($question->command_specify): ?>
                                                <?= \kartik\select2\Select2::widget([
                                                    'id' => 'textarea'.$question->id,
                                                    'name' => 'textarea'.$question->id,
                                                    'value' => $session['answers'][$turn - 1]['answer'],
                                                    'data' => ArrayHelper::map(Command::find()->all(), 'id', 'name'),
                                                    'pluginOptions' => [
                                                        'tags' => true,
                                                        'placeholder' => 'Выберите название команды'
//                                                        'allowClear' => true,
                                                    ],
                                                    'pluginEvents' => [
                                                        "change" => "function() {
                                                            
                                                            $.get('/page/default/check-done-by-command',  {'questionary_id':".$questionary->id.", 'command_id': $('#default".$question->id."').val()}, function(response){

                                                                if(response.message != null){
                                                                    alert(response.message);
                                                                } else {
                                                                    $.get('/page/default/set-values', {'id':".$question->id.", 'turn': ".$turn.", 'value':$('#default".$question->id."').val(), 'time': timePass}, function(data){ timePass = 1; } );
                                                                }

                                                            });
                                                        }",
                                                    ],
                                                    'options' => [
                                                        'data-command' => 'command'
                                                    ],
                                                ]) ?>

                                                <input id="code_word" placeholder="Фамилия капитана" type="text" class="form-control" style="margin-top: 17px; width: 100%; float: left;"><button id="code_word_btn" class="btn btn-success" style="float: left; margin-top: 17px; margin-left: 12px;">Проверить</button>
                                            <?php else: ?>
                                                <textarea name="textarea<?=$question->id?>" id="textarea<?=$question->id?>" <?= $question->require ? 'required="required"' : '' ?> class="form-control" onchange="$.get('/page/default/set-values', {'id':<?=$question->id?>, 'turn': <?=$turn?>, 'value':$('#textarea<?=$question->id?>').val(), 'time': timePass}, function(data){ timePass = 1; } );" rows="1"><?=$session['answers'][$turn - 1]['answer']?></textarea>
                                            <?php endif; ?>
                                        </div>
                                        <?php
                                    }
                                    if($question->type == 2)
                                    {
                                        ?>
                                        <div class="col-md-12">
                                            <input type="number" id="number<?=$question->id?>" name="number<?=$question->id?>" value="<?=$session['answers'][$turn - 1]['answer']?>" onchange="$.get('/page/default/set-values', {'id':<?=$question->id?>, 'turn': <?=$turn?>, 'value':$('#number<?=$question->id?>').val(), 'time': timePass}, function(data){ timePass = 1; } );" <?= $question->require ? 'required="required"' : '' ?> class="validate[required,maxSize[8] form-control">
                                        </div>
                                        <?php
                                    }
                                    if($question->type == 3)
                                    {
                                        ?>
                                        <?php
                                        $i = 0;
                                        foreach (json_decode($question->individual) as $value)
                                        {
                                            $i++;
                                            if( $session['answers'][$turn - 1]['answer'] == $i ) $checked = 'checked=""';
                                            else $checked = '';
                                            ?>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-check" style="text-align: left;">
                                                        <input class="form-check-input" type="radio" id="radio<?=$question->id?><?=$i?>" name="radio<?=$question->id?>[]" <?=$checked?> value="<?=$value->value?>" <?= $question->require ? 'required="required"' : '' ?> style="vertical-align: top;width:20px;height:20px;"
                                                               onchange="
                                                                       var cboxes = document.getElementsByName('radio<?=$question->id?>[]');
                                                                       var len = cboxes.length;
                                                                       for (var i=0; i<len; i++) {
                                                                       if( cboxes[i].checked ){
                                                                       $.get('/page/default/set-values', {'id':<?=$question->id?>, 'turn': <?=$turn?>, 'value': i + 1, 'time': timePass}, function(data){ timePass = 1; } );
                                                                       }
                                                                       }
                                                                       "
                                                        >
                                                        <label class="form-check-label" for="radio<?=$question->id?><?=$i?>" style="margin-left: 12px; width: 75%;"><?=$value->value?></label>
                                                    </div>
                                                </div>

                                            </div>
                                        <?php } ?>
                                        <?php
                                    }
                                    if($question->type == 4)
                                    {
                                        ?>
                                        <?php
                                        $i = 0;
                                        foreach (json_decode($question->multiple) as $value) {
                                            $i++;
                                            $array = explode(',', $session['answers'][$turn - 1]['answer']);
                                            if( in_array($i, $array) ) $checked = 'checked=""';
                                            else $checked = '';
                                            ?>
                                            <div class="row">
                                                <div class="col-md-1 col-sm-1 col-xs-1">
                                                    <input type = "checkbox" <?=$checked?> name="checkbox<?=$question->id?>[]"
                                                           onchange="
                                                                   var cboxes = document.getElementsByName('checkbox<?=$question->id?>[]');
                                                                   var len = cboxes.length; var y = 0; var result = '';
                                                                   for (var i=0; i<len; i++) {
                                                                   if( cboxes[i].checked ){
                                                                   var id = i + 1;
                                                                   if(y === 0) result = id;
                                                                   else result += ',' + id;
                                                                   y++;
                                                                   }
                                                                   }
                                                                   $.get('/page/default/set-values', {'id':<?=$question->id?>, 'turn': <?=$turn?>, 'value':result, 'time': timePass}, function(data){ timePass = 1; } );
                                                                   "
                                                           value="<?=$value->question?>" <?= $question->require ? 'required="required"' : '' ?> style="width:20px; height:20px;" >
                                                </div>
                                                <label style="margin-top: 5px;" class="col-md-11 control-label"><?=$value->question?></label>
                                            </div>
                                        <?php } ?>
                                        <?php
                                    }
                                    if($question->type == 5)
                                    {
                                        ?>
                                        <div class="col-md-12">
                                            <input type="date" name="date<?=$question->id?>" id="date<?=$question->id?>" value="<?=$session['answers'][$turn - 1]['answer']?>" onchange="$.get('/page/default/set-values', {'id':<?=$question->id?>, 'turn': <?=$turn?>, 'value':$('#date<?=$question->id?>').val(), 'time': timePass}, function(data){ timePass = 1; } );" <?= $question->require ? 'required="required"' : '' ?> class="form-control" name="date[]">
                                        </div>
                                    <?php       }
                                    if($question->type == 6)
                                    {
                                        ?>
                                        <div class="col-md-12">
                                            <center>
                                                <div id="logo_file<?=$question->id?>">
                                                    <img style="width:250px; height:250px;"  src="<?=$path?>">
                                                </div>
                                                <br>
                                                <div id="logo<?=$question->id?>"></div>
                                                <button style="display:block;width:120px; height:30px;" name="button23<?=$question->id?>[]" id="button23<?=$question->id?>" onclick="document.getElementById('fileInput<?=$question->id?>').click()">Загрузить аватар</button>
                                                <input type="file" name="fileInput<?=$question->id?>[]" style="display:none" id="fileInput<?=$question->id?>" class="poster23_image<?=$question->id?>" accept="image/*" />
                                            </center>
                                        </div>
                                        <?php
                                    }
                                    if($question->type == 7)
                                    {
                                        ?>
                                        <div class="col-md-12">
                                            <textarea id="bigtextarea<?=$question->id?>"  name="bigtextarea<?=$question->id?>" <?= $question->require ? 'required="required"' : '' ?> onchange="$.get('/page/default/set-values', {'id':<?=$question->id?>, 'turn': <?=$turn?>, 'value':$('#bigtextarea<?=$question->id?>').val(), 'time': timePass}, function(data){ timePass = 1; } );" class="form-control" rows="6"><?=$session['answers'][$turn - 1]['answer']?></textarea>

                                        </div>
                                        <?php
                                    }
                                    if($question->type == 8)
                                    {
                                        ?>
                                        <div class="col-md-12">
                                            <center>
                                                <div id="many_files<?=$question->id?>"></div>
                                                <button style="display:block;width:120px; height:30px;" name="many_files<?=$question->id?>[]" id="many_files<?=$question->id?>" onclick="document.getElementById('fileInputMany<?=$question->id?>').click()">Загрузить файлы</button>
                                                <input type="file" name="fileInputMany<?=$question->id?>[]" style="display:none" id="fileInputMany<?=$question->id?>" multiple class="poster23_image_many<?=$question->id?>" accept="image/*" />
                                            </center>
                                        </div>
                                        <?php
                                    }
                                    ?>

                                </div>
                            </div>
                            <!--                    <div class="panel-footer text-center">-->
                            <!--                    </div>                            -->
                        </div>
                        <div class="text-center">
                            <div class="widget-big-int pull-right" data-command-cpecify="<?=$question->command_specify?>" style="margin-left: 50px; font-size: 18px; font-weight: bold; color:#000; width: 100%; display: block;" id="demo"></div>
                            <?php /*$turn > 1 ? Html::a( Questionary::getPreviousArrow() . 'Предыдущий' , ['/anketa/test', 'turn' => $turn - 1], [ 'class' => 'btn btn-info' ]) : ''*/ ?>
                            <?php if($turn > 1) { ?>
                                <div class="hidden">
                                    <a class="btn btn-info" onclick="$.get('/page/default/change', {'id' : '-1', 'turn' : <?=$turn - 1?> }, function(data){ $.pjax.reload({container:'#test-pjax', async: false}); $.pjax.reload({container:'#question-pjax', async: false}); } );" >
                                        <?= Questionary::getPreviousArrow() . 'Предыдущий' ?>
                                    </a>
                                </div>
                            <?php } ?>
                            <?php /*$session['max_test_count'] > $turn ? Html::a( $nextLabel . Questionary::getNextArrow() , ['/anketa/test', 'turn' => $turn + 1], [ 'class' => 'btn btn-info pull-right' ]) : ''*/ ?>
                            <?php if($session['max_test_count'] > $turn) { ?>
                                <?php if($question->command_specify): ?>
                                    <a id="btn-next" class="btn btn-info" style="display: none; margin-top: 10px;"  onclick="turn = turn + 1; $.get('/page/default/change', {'id' : '-1', 'turn' : turn }, function(data){ $.pjax.reload({container:'#test-pjax', async: false}); $.pjax.reload({container:'#question-pjax', async: false}); countDownDate = data.test_time; } );" >
                                        <?= $nextLabel . Questionary::getNextArrow() ?>
                                    </a>
                                <?php else: ?>
                                    <a id="btn-next" class="btn btn-info"  style="margin-top: 10px;" onclick="turn = turn + 1; $.get('/page/default/change', {'id' : '-1', 'turn' : turn }, function(data){ $.pjax.reload({container:'#test-pjax', async: false}); $.pjax.reload({container:'#question-pjax', async: false}); countDownDate = data.test_time; } );" >
                                        <?= $nextLabel . Questionary::getNextArrow() ?>
                                    </a>
                                <?php endif; ?>

                            <?php } else { ?>
                                <a class="btn btn-info" style="cursor:pointer;" onclick="$.get('/page/default/end', {'end' : '1' }, function(data){ $.pjax.reload({container:'#test-pjax', async: false}); $.pjax.reload({container:'#question-pjax', async: false}); } );">
                                    <span c style="font-size: 20px; color: white;">Закончить тест </span>
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                    <?php Pjax::end() ?>
                    <?php Pjax::begin(['enablePushState' => false, 'id' => 'test-pjax']) ?>
                    <div class="hidden">
                        <div class="col-md-3">
                            <div class="panel panel-default">
                                <div class="panel-body" style="background-color: #3FC5FA; color: white;">
                                    <div class="profile-controls">
                                        <a class="profile-control-right" style="cursor: pointer;" title="Закончить тест" onclick="$.get('/page/default/end', {'end' : '1' }, function(data){ $.pjax.reload({container:'#test-pjax', async: false}); $.pjax.reload({container:'#question-pjax', async: false}); } );" >
                                            <span class="glyphicon glyphicon-off" style="font-size: 25px; color: white;"></span>
                                        </a>
                                        <!-- <div class="widget-big-int plugin-clock pull-right" style="font-size: 20px">19<span>:</span>34</div> -->
                                    </div>
                                </div>
                                <div class="panel-body" style="background-color: #3FC5FA; min-height: 500px;">
                                    <ul class="test-count">
                                        <?php
                                        $i = 0;
                                        foreach ($session['answers'] as $value) {
                                            $button = '';
                                            $i++;
//                                        if($value['answer'] !== null) $button = 'btn-success';
//                                        if($value['truth'] == 1) $button = 'btn-success'; else $button = 'btn-danger';
                                            if($value['truth'] == 1) $button = 'btn-success'; else $button = '';
                                            ?>
                                            <li style="color: #fff;font-weight: bold;">
                                                <div class="hidden">
                                                    <a class="btn test_button <?=$button?>"  onclick="$.get('/page/default/change', {'id' : <?=$value['question_id']?>, 'turn' : <?=$i?> }, function(data){ $.pjax.reload({container:'#test-pjax', async: false}); $.pjax.reload({container:'#question-pjax', async: false}); } );" >
                                                        <?=$i?>
                                                    </a>
                                                </div>
                                                <a class="btn test_button <?=$button?>" href="#">
                                                    <?=$i?>
                                                </a>
                                                &nbsp; <?=$i?> - вопрос
                                                <?php // Html::a( $i, ['/anketa/test', 'turn' => $i], [ 'class' => 'btn test_button ' . $button ]) ?>
                                                <!-- &nbsp; <?php //$i?> - вопрос -->
                                            </li>
                                            <br>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                    <br><br><br>
                                    <div class="present-lod">
                                        <b>Пройдено:</b>
                                    </div>
                                    <div class="loading-pres">
                                        <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <?=$session['process']?>%"><?=$session['process']?>%</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php Pjax::end() ?>
                </div>
            </div>
        </div>
    </div>

<?php
$this->registerJs(<<<JS

    var fileCollection = new Array();

    /*Загрузка файла*/

    $(function(){
        //file input field trigger when the drop box is clicked
        $("#logo$question->id").click(function(){
            $("#fileInput$question->id").click();
        });
        
        //prevent browsers from opening the file when its dragged and dropped
        $(document).on('drop dragover', function (e) {
            e.preventDefault();
        });

        //call a function to handle file upload on select file
        $('input[name^=\'fileInput$question->id\']').on('change', fileUpload);
    });

    function fileUpload(event){
        //notify user about the file upload status
        $("#logo$question->id").html(event.target.value+" загрузка...");
        
        //get selected file
        files = event.target.files;
        
        //form data check the above bullet for what it is  
        var data = new FormData();                                   

        //file data is presented as an array
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            if(!file.type.match('image.*')) {              
                //check file type
                $("#logo$question->id").html("Пожалуйста, выберите файл изображения.");
            }else{
                //append the uploadable file to FormData object
                data.append('file', file, file.name);
                data.append('_csrf', '{$_csrf}');
                data.append('id', '{$question->id}');
                data.append('turn', '{$turn}');
                
                //create a new XMLHttpRequest
                var xhr = new XMLHttpRequest();     
                
                //post file data for upload
                xhr.open('POST', '/page/default/set-logo', true);  
                xhr.send(data);
                xhr.onload = function () {
                    //get response and show the uploading status
                    var response = JSON.parse(xhr.responseText);
                    if(xhr.status === 200 && response.status == 'ok'){
                        $("#logo$question->id").html("Файл был успешно загружен. Нажмите, чтобы загрузить другой.");
                    }else {
                        $("#logo$question->id").html("Возникла проблема. Пожалуйста, попробуйте еще раз.");
                    }
                };
            }
        }
    }

    $(document).on('change', '.poster23_image$question->id', function(e){
        var files = e.target.files;
        $.each(files, function(i, file){
            fileCollection.push(file);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){
                var template = '<img style="width:250px; height:250px;" src="'+e.target.result+'"> ';
                $('#logo_file$question->id').html('');
                $('#logo_file$question->id').append(template);
            };
        });
    });

        /*Загрузка файла*/

    $(function(){
        //file input field trigger when the drop box is clicked
        $("#many_files$question->id").click(function(){
            $("#fileInputMany$question->id").click();
        });
        
        //prevent browsers from opening the file when its dragged and dropped
        $(document).on('drop dragover', function (e) {
            e.preventDefault();
        });

        //call a function to handle file upload on select file
        $('input[name^=\'fileInputMany$question->id\']').on('change', fileUploadMany);
    });

    function fileUploadMany(event){
        //notify user about the file upload status
        $("#many_files$question->id").html(event.target.value+" загрузка...");
        
        //get selected file
        files = event.target.files;
        
        //form data check the above bullet for what it is  
        var data = new FormData();                                   

        //file data is presented as an array
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            if(!file.type.match('image.*')) {              
                //check file type
                $("#many_files$question->id").html("Пожалуйста, выберите файл изображения.");
            }else{
                //append the uploadable file to FormData object
                data.append('file', file, file.name);
                data.append('_csrf', '{$_csrf}');
                data.append('id', '{$question->id}');
                data.append('turn', '{$turn}');
                
                //create a new XMLHttpRequest
                var xhr = new XMLHttpRequest();     
                //alert('sdssss');
                //post file data for upload
                xhr.open('POST', '/page/default/set-many-files', true);  
                xhr.send(data);
                xhr.onload = function () {
                    //get response and show the uploading status
                    var response = JSON.parse(xhr.responseText);
                    if(xhr.status === 200 && response.status == 'ok'){
                        $("#many_files$question->id").html("Файл был успешно загружен. Нажмите, чтобы загрузить другой.");
                    }else {
                        $("#many_files$question->id").html("Возникла проблема. Пожалуйста, попробуйте еще раз.");
                    }
                };
            }
        }
    }

JS
);


?>


    <input type="hidden" id="time" name="time" value="<?=time()?>">
    <script>
        // Set the date we're counting down to
        var countDownDate = <?=$session['test_time']?>;

        <?php if($question->command_specify): ?>
        var trueCode = false;
        <?php else:  ?>
        var trueCode = true;
        <?php endif; ?>

        var messageCounter = 0;

        //    var countDownDate = 1589562458725;
        //    alert(<?//=time()?>//);

        var timePass = 0;

        var turn = <?=$turn?>;

        var totalTurn = <?=count($session['answers']) + 1?>;

        //     Update the count down every 1 second
        var x = setInterval(function() {


            // Get todays date and time

            // $.get('/page/default/get-time',{ },function(data) { $('#time').val(data); } );
            // var now = document.getElementById('time').value;

            var now = Math.round((Date.now() / 1000));

            //alert(countDownDate + ' ' + now);
            //alert(now);
            // Find the distance between now and the count down date
            var distance = (countDownDate - now) * 1000;

            // Time calculations for days, hours, minutes and seconds
            //var days = Math.floor(distance / (1000 * 60 * 60 * 24));

//      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="demo"
            if(messageCounter != 0){
                messageCounter++;
                if(messageCounter == 4){
                    messageCounter = 0;
                }
            } else {
//                document.getElementById("demo").innerHTML = minutes + "м " + seconds + "с ";

                if(seconds != 0) {
                    if (turn != 1) {
                    	var cpecifyCommand = $("#demo").data('command-cpecify');
                    	if(cpecifyCommand == false){
                        	document.getElementById("demo").innerHTML = "Время пошло: " + seconds + " сек. ";
                    	}
                    }
                }
            }

            timePass = timePass + 1;


            // If the count down is finished, write some text
            <?php if($question->isLast): ?>
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("demo").innerHTML = "Время закончено";
                $.get('/page/default/end', {'end' : '1' }, function(data){ $.pjax.reload({container:'#test-pjax', async: false}); $.pjax.reload({container:'#question-pjax', async: false}); } );
//            window.location.href = "/page/default/test?end=1";
            }
            <?php else: ?>

            if (distance < 0) {
                turn = turn + 1;

                if(trueCode == true){


                    if(turn == totalTurn){
                        clearInterval(x);
                        document.getElementById("demo").innerHTML = "Время закончено";
                        $.get('/page/default/end', {'end' : '1' }, function(data){ $.pjax.reload({container:'#test-pjax', async: false}); $.pjax.reload({container:'#question-pjax', async: false}); } );
                    } else {
                        $.get('/page/default/change', {'id' : '-1', 'turn' : turn }, function(data){

                            $.pjax.reload({container:'#test-pjax', async: false});
                            $.pjax.reload({container:'#question-pjax', async: false});
                            window.location.href = "/<?=$_GET['link']?>";
                            countDownDate = data.test_time;
                            timePass = 0;

                        });
                    }
                } else {
                    clearInterval(x);
                    document.getElementById("demo").innerHTML = "Время вышло, попробуйте ещё раз!";
                }


//            $.get('/page/default/change', {'id' : '-1', 'turn' : turn }, function(data){
//                $.pjax.reload({container:'#test-pjax', async: false});
//                $.pjax.reload({container:'#question-pjax', async: false});
//                window.location.href = null;
//                countDownDate = data.test_time;
//            console.log(data);
//            });
//        clearInterval(x);
//        document.getElementById("demo").innerHTML = "Время закончено";
//        $.get('/page/default/end', {'end' : '1' }, function(data){ $.pjax.reload({container:'#test-pjax', async: false}); $.pjax.reload({container:'#question-pjax', async: false}); } );
                //window.location.href = "/page/default/test?end=1";
            }
            <?php endif; ?>

        }, 1000);


        <?php if($question->command_specify): ?>

        $('#code_word_btn').click(function(){
            var codeWord = $('#code_word').val();
            var command = $('[data-command="command"]').val();

            $.get('/page/default/check-code?command='+command+'&word='+codeWord+'&questionary_id=<?=$questionary->id?>', function(response){

                if(response.error){
//                        alert(response.error);
                    document.getElementById("demo").innerHTML = response.error;
                    messageCounter = 0;
                } else {
                    trueCode = true;
                    $('#btn-next').show();
                }

            });
        });

        <?php endif; ?>

        function nextStepBtn(){
            $.get('/page/default/change', {'id' : '-1', 'turn' : turn }, function(data){
                $.pjax.reload({container:'#test-pjax', async: false});
                $.pjax.reload({container:'#question-pjax', async: false});
                window.location.href = null;
                countDownDate = data.test_time;
                console.log(data);
            });
        clearInterval(x);
        document.getElementById("demo").innerHTML = "Время закончено";
        $.get('/page/default/end', {'end' : '1' }, function(data){ $.pjax.reload({container:'#test-pjax', async: false}); $.pjax.reload({container:'#question-pjax', async: false}); } );
            window.location.href = "/page/default/test?end=1";
        }
    </script>

<?php

if (class_exists('yii\debug\Module')) {
    $this->off(\yii\web\View::EVENT_END_BODY, [\yii\debug\Module::getInstance(), 'renderToolbar']);
}

?>