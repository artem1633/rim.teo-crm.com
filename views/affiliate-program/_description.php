<?php

use yii\helpers\Html;
use app\models\Users;

$affAmount = $company->affiliate_amount ? $company->affiliate_amount : 0;

?>
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-6">
                <h3 class="panel-title">Сводные данные</h3>
            </div>
            <div class="col-md-6 text-right" id="refLink">
                <span style="cursor: pointer; color: green" onclick="copyToClipboard('https://demo.teo-job.ru/site/register?ref=<?= Yii::$app->user->identity->id; ?>');"><i class="fa fa-copy"></i> Ваша ссылка:  <span id="copy-to"><?='https://' . $_SERVER['SERVER_NAME'] . '/site/register?ref='. Yii::$app->user->identity->id?></span></span>
            </div>
        </div>
    </div>
    <div class="panel-body" style="padding-bottom: 15px">
        <div class="row">
            <div class="col-md-10 details-affiliate">
                <div class="row">
                    <div class="col-md-1 text-center">
                        <i class="fa fa-credit-card fa-2x"></i>
                    </div>
                    <div class="col-md-4">
                        Баланс партнерского счета
                    </div>
                    <div class="col-md-7 text-bold">
                         <?= number_format(Yii::$app->user->identity->partner_balance, 2, ',', ' ') ?> <i class="fa fa-rub"></i>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1 text-center">
                        <i class="fa fa-user-secret fa-2x"></i>
                    </div>
                    <div class="col-md-4">
                        Количество рефералов
                    </div>
                    <div class="col-md-7 text-bold">
                        <?= Users::find()->where(['referal_id' => Yii::$app->user->identity->id ])->count() ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1 text-center">
                        <i class="fa fa-pie-chart fa-2x"></i>
                    </div>
                    <div class="col-md-4">
                        Процентная ставка
                    </div>
                    <div class="col-md-7 text-bold">
                        <?= Yii::$app->user->identity->prosent_referal; ?> %
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1 text-center">
                        <i class="fa fa-link fa-2x"></i>
                    </div>
                    <div class="col-md-4">
                        Ссылка для привлечения пользователей
                    </div>
                    <div class="col-md-7 text-bold">
                       <?= 'https://' . $_SERVER['SERVER_NAME'] . '/site/register?ref='. Yii::$app->user->identity->id ?> <span style="cursor: pointer; color: green" onclick="copyToClipboard('https://demo.teo-job.ru/site/register?ref=<?= Yii::$app->user->identity->id; ?>');"><i class="fa fa-copy"></i></span>
                        <br/>
                        <div class="copy-result label label-info"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1 text-center">
                    </div>
                    <div class="col-md-4">
                        Количество посещений
                    </div>
                    <div class="col-md-7 text-bold">
                        <?= \app\models\ReferalRedirects::find()->where(['refer_company_id' => Yii::$app->user->identity->id])->count() ?>
                        <br/>
                        <div class="copy-result label label-info"></div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Принципы и условия работы партнерской программы</h3>
    </div>
    <div class="panel-body" style="padding-bottom: 15px">
        <?=$description?>
    </div>
</div>


<script>
    function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(element).select();
        document.execCommand("copy");
        $temp.remove();
        $('.copy-result').html('Скопировано в буфер обмена');
        setTimeout(func, 1000);
    }

    function func() {
        $('.copy-result').empty();
    }

</script>

