<?php

use yii\helpers\Html,
    kartik\form\ActiveForm,
    kartik\select2\Select2;

?>
<div class="payment-orders-create">

    <div class="payment-orders-form row">

        <?php $form = ActiveForm::begin(); ?>
        <div class="col-md-6">
            <?= $form->field($model, 'amount')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'type')->label()->widget(Select2::classname(), [
                'data' => [1 => 'На карту', 2 => 'Яндекс Деньги', 3 => 'На основной баланс'],
                'options' => [
                    'placeholder' => 'Выберите',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'requisites')->textarea(['rows' => 6]) ?>
        </div>


        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
