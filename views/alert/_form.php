<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Alert */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alert-form" style="padding:20px;">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <!-- <div class="col-md-2">
            <?php // $form->field($model, 'count')->textInput(['disabled' => true]) ?>
        </div> -->
        <div class="col-md-12">
            <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>
            <?php /*$form->field($model, 'text')->widget(CKEditor::className(),[
                'editorOptions' => [
                    'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                    'inline' => false, //по умолчанию false
                    'height' => '200px',
                ],
            ]);*/
            ?>
        </div>
    </div>

<!--     <div class="row">
        <div class="col-md-6">
            <?php /*$form->field($model, 'statuses')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getResumeStatusList(),
                'id' => 'statuses',
                'options' => ['placeholder' => 'Выберите ...'],
                'pluginOptions' => [
                    'multiple' => true,
                    'allowClear' => true,
                ],
            ])*/ ?>
        </div>
        <div class="col-md-6">
            <?php /*$form->field($model, 'groups')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getGroupList(),
                'id' => 'groups',
                'options' => ['placeholder' => 'Выберите ...'],
                'pluginOptions' => [
                    'multiple' => true,
                    'allowClear' => true,
                ],
            ])*/ ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?php /*$form->field($model, 'vacancies')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getVacancyList(),
                'id' => 'vacancies',
                'options' => ['placeholder' => 'Выберите ...'],
                'pluginOptions' => [
                    'multiple' => true,
                    'allowClear' => true,
                ],
            ])*/ ?>
        </div>

        <div class="col-md-6">
            <?php /*$form->field($model, 'tags')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getTagsList(),
                'id' => 'tags',
                'options' => ['placeholder' => 'Выберите ...'],
                'pluginOptions' => [
                    'multiple' => true,
                    'allowClear' => true,
                ],
            ])*/ ?>
        </div>        
    </div> -->

    
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
