<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Alert */
?>
<div class="alert-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
