<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Command;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'command_id',
        'value' => 'command.name',
        'filter' => ArrayHelper::map(Command::find()->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => '', 'multiple' => true],
            'pluginOptions' => [
                'allowClear' => true,
                'tags' => false,
                'tokenSeparators' => [','],
            ],
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'balls',
        'width' => '5%',
        'hAlign' => GridView::ALIGN_CENTER,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'comment',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_at',
        'format' => ['date', 'php:d M Y H:i'],
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
            'convertFormat'=>true,
            'pluginEvents' => [
                'cancel.daterangepicker'=>'function(ev, picker) {$("#commandballsearch-created_at").val(""); $("#commandballsearch-created_at").trigger("change"); }'
            ],
            'pluginOptions' => [
                'opens'=>'right',
                'locale' => [
                    'cancelLabel' => 'Clear',
                    'format' => 'Y-m-d',
                ]
            ]
        ],
        'width' => '10%',
    ],
//    [
//        'class' => 'kartik\grid\ActionColumn',
//        'dropdown' => false,
//        'vAlign'=>'middle',
//        'urlCreator' => function($action, $model, $key, $index) {
//                return Url::to([$action,'id'=>$key]);
//        },
//        'template' => '{update}{delete}',
//        'buttons' => [
//            'delete' => function ($url, $model) {
//                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
//                    'role'=>'modal-remote', 'title'=>'Удалить',
//                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                    'data-request-method'=>'post',
//                    'data-confirm-title'=>'Вы уверены?',
//                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
//                ]);
//            },
//            'update' => function ($url, $model) {
//                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
//                    'role'=>'modal-remote', 'title'=>'Изменить',
//                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                    'data-request-method'=>'post',
//                ])."&nbsp;";
//            }
//        ],
//    ],

];   