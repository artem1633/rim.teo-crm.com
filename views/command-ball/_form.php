<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CommandBall */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="command-ball-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'command_id')->widget(\kartik\select2\Select2::class, [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Command::find()->all(), 'id', 'name'),
    ]) ?>

    <?= $form->field($model, 'balls')->input('number') ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
