<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CommandBall */

?>
<div class="command-ball-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
