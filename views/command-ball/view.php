<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CommandBall */
?>
<div class="command-ball-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'command_id',
            'balls',
            'comment:ntext',
        ],
    ]) ?>

</div>
