<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Command */
?>
<div class="command-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
