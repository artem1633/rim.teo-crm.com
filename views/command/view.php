<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Command */
?>
<div class="command-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'balls',
        ],
    ]) ?>

</div>
