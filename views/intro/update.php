<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\lessons */
?>
<div class="intro-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
