<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Notification */

CrudAsset::register($this);
?>

<div class="panel panel-default tabs">                            
    <ul class="nav nav-tabs" role="tablist">
        <!-- <li class=""><a href="#tab-first" role="tab" data-toggle="tab" aria-expanded="true">Уведомление</a></li> -->
        <li class="active"><a href="#tab-second" role="tab" data-toggle="tab" aria-expanded="true">Вакансия</a></li>
        <li class=""><a href="#tab-third" role="tab" data-toggle="tab" aria-expanded="false">Группа</a></li>
        <li class=""><a href="#tab-fourth" role="tab" data-toggle="tab" aria-expanded="false">Статус теста</a></li>
        <li class=""><a href="#tab-five" role="tab" data-toggle="tab" aria-expanded="false">Теги</a></li>
    </ul>                            
    <div class="panel-body tab-content">
        <div class="tab-pane" id="tab-first">
            <div id="ajaxCrudDatatable">
                <?=GridView::widget([
                    'id'=>'notification',
                    'dataProvider' => $notDataProvider,
                    //'filterModel' => $searchModel,
                    'pjax'=>true,
                    'responsiveWrap' => false,
                    'columns' => require(__DIR__.'/_columns.php'),
                    'toolbar'=> [
                        ['content'=> '',
                        ],
                    ],          
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,          
                    'panel' => [
                        'type' => 'warning', 
                        'heading' => '<i class="glyphicon glyphicon-list"></i> Список уведомлений',
                        'before'=>'',
                        'after'=>'',
                    ]
                ])?>
            </div>
        </div>
        <div class="tab-pane active" id="tab-second">
                <?=GridView::widget([
                    'id'=>'vacancy',
                    'dataProvider' => $vacancyProvider,
                    'filterModel' => $vacancyModel,
                    'pjax'=>true,
                    'responsiveWrap' => false,
                    'columns' => require(__DIR__.'/_vacancy_columns.php'),
                    'toolbar'=> [
                        ['content'=>
                            '<div style="margin-top:10px;">' .
                            Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['/vacancy/create'],
                            ['role'=>'modal-remote','title'=> 'Добавить', 'class'=>'btn btn-info']).
                            '<ul class="panel-controls">
                                <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                                <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                            </ul>  '.
                            '</div>'
                        ],
                    ],          
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,          
                    'panel' => [
                        'type' => 'warning', 
                        'heading' => '<i class="glyphicon glyphicon-list"></i> Список вакансий',
                        'before'=>'',
                        'after'=>BulkButtonWidget::widget([
                                    'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>Удалить все',
                                        ["/vacancy/bulk-delete"] ,
                                        [
                                            "class"=>"btn btn-danger btn-xs",
                                            'role'=>'modal-remote-bulk',
                                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                            'data-request-method'=>'post',
                                            'data-confirm-title'=>'Подтвердите действие',
                                            'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                                        ]),
                                ]).                        
                                '<div class="clearfix"></div>',
                    ]
                ])?>
        </div>
        <div class="tab-pane" id="tab-third">
            <?=GridView::widget([
                    'id'=>'group',
                    'dataProvider' => $groupProvider,
                    'filterModel' => $groupModel,
                    'responsiveWrap' => false,
                    'pjax'=>true,
                    'columns' => require(__DIR__.'/_group_columns.php'),
                    'toolbar'=> [
                        ['content'=>
                            '<div style="margin-top:10px;">' .
                            Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['/group/create'],
                            ['role'=>'modal-remote','title'=> 'Добавить', 'class'=>'btn btn-info']).
                            '<ul class="panel-controls">
                                <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                                <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                            </ul>  '.
                            '</div>'
                        ],
                    ],          
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,          
                    'panel' => [
                        'type' => 'warning', 
                        'heading' => '<i class="glyphicon glyphicon-list"></i> Список групп',
                        'before'=>'',
                        'after'=>BulkButtonWidget::widget([
                                    'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>Удалить все',
                                        ["/group/bulk-delete"] ,
                                        [
                                            "class"=>"btn btn-danger btn-xs",
                                            'role'=>'modal-remote-bulk',
                                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                            'data-request-method'=>'post',
                                            'data-confirm-title'=>'Подтвердите действие',
                                            'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                                        ]),
                                ]).                        
                                '<div class="clearfix"></div>',
                    ]
                ])?>
        </div>
        <div class="tab-pane" id="tab-fourth">
            <?=GridView::widget([
                    'id'=>'resume',
                    'dataProvider' => $resumeProvider,
                    'filterModel' => $resumeProvider,
                    'pjax'=>true,
                    'responsiveWrap' => false,
                    'columns' => require(__DIR__.'/_resume_columns.php'),
                    'toolbar'=> [
                        ['content'=>
                            '<div style="margin-top:10px;">' .
                            Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['/resume-status/create'],
                            ['role'=>'modal-remote','title'=> 'Добавить', 'class'=>'btn btn-info']).
                            '<ul class="panel-controls">
                                <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                                <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                            </ul>  '.
                            '</div>'
                        ],
                    ],          
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,          
                    'panel' => [
                        'type' => 'warning', 
                        'heading' => '<i class="glyphicon glyphicon-list"></i> Список статусов',
                        'before'=>'',
                        'after'=>BulkButtonWidget::widget([
                                    'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>Удалить все',
                                        ["/resume-status/bulk-delete"] ,
                                        [
                                            "class"=>"btn btn-danger btn-xs",
                                            'role'=>'modal-remote-bulk',
                                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                            'data-request-method'=>'post',
                                            'data-confirm-title'=>'Подтвердите действие',
                                            'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                                        ]),
                                ]).                        
                                '<div class="clearfix"></div>',
                    ]
                ])?>
        </div>
        <div class="tab-pane" id="tab-five">
            <?=GridView::widget([
                    'id'=>'tags',
                    'dataProvider' => $tagsProvider,
                    'filterModel' => $tagsModel,
                    'pjax'=>true,
                    'responsiveWrap' => false,
                    'columns' => require(__DIR__.'/_tags_columns.php'),
                    'toolbar'=> [
                        ['content'=>
                            '<div style="margin-top:10px;">' .
                            Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['/tags/create'],
                            ['role'=>'modal-remote','title'=> 'Добавить', 'class'=>'btn btn-info']).
                            '<ul class="panel-controls">
                                <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                                <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                            </ul>  '.
                            '</div>'
                        ],
                    ],          
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,          
                    'panel' => [
                        'type' => 'warning', 
                        'heading' => '<i class="glyphicon glyphicon-list"></i> Список статусов',
                        'before'=>'',
                        'after'=>BulkButtonWidget::widget([
                                    'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>Удалить все',
                                        ["/tags/bulk-delete"] ,
                                        [
                                            "class"=>"btn btn-danger btn-xs",
                                            'role'=>'modal-remote-bulk',
                                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                            'data-request-method'=>'post',
                                            'data-confirm-title'=>'Подтвердите действие',
                                            'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                                        ]),
                                ]).                        
                                '<div class="clearfix"></div>',
                    ]
                ])?>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>