<?php
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;
?>

<?=GridView::widget([ 
    'id'=>'crud-datatable',
    'responsiveWrap' => false,
    'dataProvider' => $model->QuestionsValue,
    'columns' => [
        [
            'class' => 'kartik\grid\SerialColumn',
            'width' => '30px',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'name',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'type',
            'content' => function($data){
                return $data->getTypeDescription();
            }
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'description',
            'format' => 'html'
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'require',
            'content' => function($data){
                if($data->require == 1) return 'Да';
                else return 'Нет';
            }
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'view_in_table',
            'content' => function($data){
                if($data->view_in_table == 1) return 'Да';
                else return 'Нет';
            }
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'vAlign'=>'middle',
            'template' => '{leadUpdate} {leadDelete}',
            'urlCreator' => function($action, $model, $key, $index) { 
                    return Url::to([$action,'id'=>$key]);
            },
            'buttons'  => [ 
                'leadSorting' => function ($url, $model) {
                    $url = Url::to(['/questions/sorting', 'questionary_id' => $model->questionary_id]);
                    return Html::a('<span class="glyphicon glyphicon-sort"></span>', $url, ['role'=>'modal-remote','title'=>'Поменять порядок', 'data-toggle'=>'tooltip']);
                },           
                'leadUpdate' => function ($url, $model) {
                    $url = Url::to(['/questions/update', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip']);
                },
                'leadDelete' => function ($url, $model) {
                    $url = Url::to(['/questions/delete', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                        'role'=>'modal-remote','title'=>'Удалить', 
                              'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                              'data-request-method'=>'post',
                              'data-toggle'=>'tooltip',
                              'data-confirm-title'=>'Подтвердите действие',
                              'data-confirm-message'=>'Вы уверены что хотите удалить этог элемент?',
                    ]);
                },
            ],
        ],
    ],
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
])?>
