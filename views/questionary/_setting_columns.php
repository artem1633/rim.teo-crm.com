<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    /*[
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],*/
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'condition',
        'content' => function($data){
            return $data->getDescription();
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'first_value',
        'content' => function($data){
            $result = $data->first_value;
            if(!$data->findShowing()) $result .= ' и ' . $data->second_value;
            return $result;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'text',
        'format' => 'html'
    ],
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'visible' => Yii::$app->user->identity->type == 0,
        'content' => function($data){
            return $data->user->fio . ' ('.$data->user->getTypeDescription().')';
        }
    ],*/
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{leadUpdate} {leadDelete}',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'buttons'  => [           
            'leadUpdate' => function ($url, $model) {
                $url = Url::to(['/questionary/update-condition', 'id' => $model->id]);
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip']);
            },
            'leadDelete' => function ($url, $model) {
                $url = Url::to(['/questionary/delete-condition', 'id' => $model->id]);
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                    'role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить этог элемент?',
                ]);
            },
        ],
    ],

];   