<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Resume */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="resume-form" style="padding:20px;">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'condition')->dropDownList($model->getConditionList(), ['id' => 'type']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'first_value')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-3" id="second" <?php if($model->findShowing()) echo 'style=" display: none;"';?> >
            <?= $form->field($model, 'second_value')->textInput(['type' => 'number']) ?>
        </div>
    </div>   

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'text')->widget(CKEditor::className(),[
                'editorOptions' => [
                    'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                    'inline' => false, //по умолчанию false
                    'height' => '200px',
                ],
            ]);
            ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?php 
$this->registerJs(<<<JS
    $('#type').on('change', function() 
    {  
        var type = this.value;
        if(type == 1) $('#second').hide();
        if(type == 2) $('#second').hide();
        if(type == 3) $('#second').hide();
        if(type == 4) $('#second').hide();
        if(type == 9) $('#second').hide();

        if(type == 5) $('#second').show();
        if(type == 6) $('#second').show();
        if(type == 7) $('#second').show();
        if(type == 8) $('#second').show();
    }
);
JS
);
?>