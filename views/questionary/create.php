<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Questionary */

?>
<div class="questionary-create">
	<?php 
		if($model->step == 1) echo $this->render('_form', [ 'model' => $model,]);
		if($model->step == 2) echo $this->render('_form2', [ 'model' => $model,])
	?>
</div>
