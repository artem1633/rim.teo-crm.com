<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ResumeStatus */

?>
<div class="resume-status-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
