<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ResumeStatus */
?>
<div class="resume-status-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
