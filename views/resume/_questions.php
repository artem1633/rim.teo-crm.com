<?php
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Questions;
use kartik\sortinput\SortableInput;

$active = [];
$table = "";
$i = 0;
foreach (json_decode($model->values) as $value) {
    $i++;
    $question = Questions::find()
        ->where(['id' => $value->question])
        ->andWhere(['or',
               ['general_access'=>0],
               ['general_access'=>null]
           ])
        ->one();
    if($question != null){
        $name = $question->getSelectesItem($value->value, $question->own_option, $model->ball_for_question);
        $own_option = null;
        if($question->own_option == 1) $own_option = ' (Свой вариант : ' . $value->own_option . ')';

        $answer = '';
        if($question->type == 3){
            foreach (json_decode($question->individual) as $individual) {

                foreach (json_decode($value->value) as $val) {
                    if($val->key == $individual->value) {
                        $answer .= $individual->value;
                    }
                }
            }
        }
        else{
            if($question->type == 4){
                foreach (json_decode($question->multiple) as $multiple) {
                    
                    foreach (json_decode($value->value) as $val) {
                        if($val->key == $multiple->question) {
                            $answer .= $multiple->question . '; ';
                        }
                    }
                }
            }
            else{
                if($question->type == 8){
                    foreach (json_decode($value->value) as $multiple) {
                        $answer .= $multiple->image_name . '; ';
                    }
                }
                else $answer .= $value->value;
            }
        }

        $answer .= $own_option;
        $table .=
            '
                <tr>
                    <td>'.$i.'</td>
                    <td>'.$question->name.'</td>
                    <td>'.$answer.'</td>
                    <td>'.$question->getTypeDescription().'</td>
                </tr>
            ';
    }
}
?>
<div class="skip-export kv-expanded-row crud-datatable_b837d169" data-index="5" data-key="67" style="">
    <div id="crud-datatable" class="grid-view is-bs3 hide-resize" data-krajee-grid="kvGridInit_50fc69fa" data-krajee-ps="ps_crud_datatable_container">
        <div id="crud-datatable-container" class="table-responsive kv-grid-container">
            <table class="kv-grid-table table table-bordered table-striped table-condensed">
            <thead>
                <tr>
                    <th>#</th>
                    <th><a>Вопрос</a></th>
                    <th><a>Ответ</a></th>
                    <th><a>Тип ответа</a></th>
                </tr>
            </thead>
            <tbody>
                <?=$table?>
            </tbody>
            </table>
        </div>
    </div>
</div>