<?php

use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Command;

$session = Yii::$app->session;
?>
<?php $form = ActiveForm::begin(); ?>
    <div class="col-md-3">
        <div class="input-group" style="width:200px;">
            <input type="text" placeholder="Команда" class="form-control" name="name" <?= $post['name'] ? 'value="'.$post['name'].'"' : (isset($_GET['page']) ? 'value="'.$session['name'].'"' : '' ) ?> >
            <div class="input-group-btn">
                <button class="btn btn-info" type="submit"><span class="fa fa-search"></span></button>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>
