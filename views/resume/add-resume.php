<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

?>

<div class="resume-form" style="padding:20px;">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'newQuestionary')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getQuestionaryList(),
                'options' => ['placeholder' => 'Выберите ...',
                 'onchange'=>'
                      $.post("/resume/getlink?resume_id='.$model->id.'&id="+$(this).val(),function(data)
                      {$("#newLink").val(data);});
                      '
            ],
                'size' => kartik\select2\Select2::SMALL,
                'pluginOptions' => [
                    'tags' => true,
                    'allowClear' => true,
                ],
            ])?>
        </div>
        <div class="col-md-12"> 
            <?= $form->field($model, 'newLink')->textInput(['id' => 'newLink']) ?>      
        </div>

    </div>


    <?php ActiveForm::end(); ?>
    
</div>
