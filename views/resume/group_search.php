<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

?>
<div class="col-md-3" style="width:180px;">
	
<?php /*Html::dropDownList('group', Yii::$app->session['group_id'], Yii::$app->session['group'], 
	[
		'prompt' => 'Выберите группу','class' => 'form-control','style' => 'width:130px;',
		'onchange' =>'$.post("/resume/group?group="+$(this).val(), function(data){});'
	] 
)*/ ?>
<?=Select2::widget([
    'name' => 'group',
    'data' => Yii::$app->session['group'],
    'value' => Yii::$app->session['group_id'],
    'size' => 'sm',
    'pluginOptions' => [
        'allowClear' => true,
    ],
    'options' => [
        'style' => 'width:130px;',
        'placeholder' => 'Выберите группу',
        'onchange'=>'$.post("/resume/group?group="+$(this).val(), function(data){});'
    ],
])?>
</div>