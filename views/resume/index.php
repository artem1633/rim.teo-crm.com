<?php

use app\models\Lessons;
use app\models\LessonsUsers;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use app\models\Group;
use app\models\ResumeStatus;
use app\models\Tags;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $searchModel app\models\QuestionarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$group = ArrayHelper::map(Group::find()->all(), 'id', 'name');
Yii::$app->session['group'] = $group;

$status = ArrayHelper::map(ResumeStatus::find()->all(), 'id', 'name');
Yii::$app->session['status'] = $status;

$tags = ArrayHelper::map(Tags::find()->all(), 'id', 'name');
Yii::$app->session['tags'] = $tags;

$this->title = 'Результаты';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>


        <div class="questionary-index">
            <div id="ajaxCrudDatatable">
                <?=GridView::widget([
                    'id'=>'crud-datatable',
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    'responsiveWrap' => false,
                    'pjax'=>true,
                    'columns' => require(__DIR__.'/_columns.php'),
                    'toolbar'=> [
                        ['content'=>
                       '<div style="margin-top:10px;">
                            <ul class="panel-controls">
                            <li>'.$this->render('group_search', []).'  </li>
                            <li>'.$this->render('status_search', []).'  </li>
                                <li data-introindex="8-10">'.$this->render('_search', ['post' => $post, 'searchModel' => $searchModel]).'  </li>
                                <li data-introindex="8-11">'.Html::a('<i class="glyphicon glyphicon-export"></i>', ["/resume/export"] , [ "data-pjax" => 0, "class"=>"btn btn-warning btn-xs", 'title' => 'Выгрузка резюме в ексель' ]).'</li>
                            </ul>  '.
                            '</div>'
                        ],
                    ],          
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,          
                    'panel' => [
                        'type' => 'warning', 
                        'heading' => '<i class="glyphicon glyphicon-list"></i> Список результатов    
    <button  type="button" class="btn btn-xs btn-warning" onclick=\'startIntro8();\'>Начать обучение</button>',
                        'before'=>'',
                        'after'=>BulkButtonWidget::widget([
                                    'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>Удалить все',
                                        ["bulk-delete"] ,
                                        [
                                            "class"=>"btn btn-danger btn-xs",
                                            'role'=>'modal-remote-bulk',
                                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                            'data-request-method'=>'post',
                                            'data-confirm-title'=>'Подтвердите действие',
                                            'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                                        ]),
                                ]).                        
                                '<div class="clearfix"></div>',
                    ]
                ])?>
            </div>
        </div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<?php
$start = 0;
$lessons_pass = LessonsUsers::getLessonsStatus(8);
if (!$lessons_pass) {
    $start = 1;
    LessonsUsers::setPassed(8);
}
$lessons8 = Lessons::getLessonsGroup(8);
?>
<script type="text/javascript">
    $(document).ready(function() {
        var start = <?=$start?>;
        if (start == 1) {
            startIntro8();
        }
    });


    function startIntro8(){
        var intro = introJs();
        intro.setOptions({
            nextLabel:"Дальше",
            prevLabel:"Назад",
            skipLabel:"Пропустить",
            doneLabel:"Понятно",
            steps: [
                <?php
                foreach ($lessons8 as $lesson)
                    echo '{element: document.querySelectorAll(\'[data-introindex="8-'.$lesson['step'].'"]\')[0],'.
                        'intro: "'.$lesson['hint'].'"},';?>

            ]
        });
        intro.setOption("nextLabel", "Дальше").setOption("prevLabel", "Назад").start();
    }
</script>