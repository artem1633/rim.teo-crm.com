<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $searchModel app\models\QuestionarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Магазин Результатов';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
            <div id="ajaxCrudDatatable">
                <?=GridView::widget([
                    'id'=>'crud-datatable',
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    'responsiveWrap' => false,
                    'pjax'=>true,
                    'columns' => require(__DIR__.'/_shop_columns.php'),
                    'toolbar'=> [
                        ['content'=>
                       '<div style="margin-top:10px;">
                            <ul class="panel-controls">
                                <li data-introindex="8-10">'.$this->render('_search', ['post' => $post]).'  </li>
                            </ul>  '.
                            '</div>'
                        ],
                    ],          
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,          
                    'panel' => [
                        'type' => 'warning', 
                        'heading' => '<i class="glyphicon glyphicon-list"></i> Магазин Результатов',
                        'before'=>'',
                        'after'=>'',
                    ]
                ])?>
            </div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>