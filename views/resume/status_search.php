<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

?>
<div class="col-md-3" style="width:180px;">
<?php /*Html::dropDownList('status', Yii::$app->session['status_id'], Yii::$app->session['status'], 
	[
		'prompt' => 'Выберите статуса','class' => 'form-control','style' => 'width:130px;',
		'onchange' =>'$.post("/resume/status?status="+$(this).val(), function(data){});'
	] 
)*/ ?>
<?=Select2::widget([
    'name' => 'status',
    'data' => Yii::$app->session['status'],
    'value' => Yii::$app->session['status_id'],
    'size' => 'sm',
    'pluginOptions' => [
        'allowClear' => true,
    ],
    'options' => [
        'style' => 'width:130px;',
        'placeholder' => 'Выберите статуса',
        'onchange'=>'$.post("/resume/status?status="+$(this).val(), function(data){});'
    ],
])?>
</div>