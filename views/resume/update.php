<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Resume */
?>
<div class="resume-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
