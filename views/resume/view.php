<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\Pjax;
use kartik\sortinput\SortableInput;
use yii\widgets\ActiveForm;
use app\models\Tags;
use app\models\Chat;
use app\models\SettingResult;

$settingResults = SettingResult::find()->where(['questionary_id' => $model->questionary->id])->all();
$ball = $model->balls + $model->ball_for_question;
$result = '';

foreach ($settingResults as $value) {
    if($value->condition == 1 && $ball > $value->first_value) $result .= "<br> {$value->text}";
    if($value->condition == 2 && $ball >= $value->first_value) $result .= "<br> {$value->text}";
    if($value->condition == 3 && $ball < $value->first_value) $result .= "<br> {$value->text}";
    if($value->condition == 4 && $ball <= $value->first_value) $result .= "<br> {$value->text}";
    if($value->condition == 5 && $value->first_value < $ball && $ball < $value->second_value) $result .= "<br> {$value->text}";
    if($value->condition == 6 && $value->first_value <= $ball && $ball <= $value->second_value) $result .= "<br> {$value->text}";
    if($value->condition == 7 && $value->first_value <= $ball && $ball < $value->second_value) $result .= "<br> {$value->text}";
    if($value->condition == 8 && $value->first_value < $ball && $ball <= $value->second_value) $result .= "<br> {$value->text}";
    if($value->condition == 9 && $value->first_value == $ball) $result .= "<br> {$value->text}";
}

CrudAsset::register($this);
$this->title = 'Анкета';
$this->params['breadcrumbs'][] = $this->title;

if($model->avatar == null) $path = 'http://' . $_SERVER['SERVER_NAME'] . '/images/nouser.png';
else $path = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $model->avatar;

$url = Url::to(['/resume/view', 'id' => $model->id]);
$update = Html::a('<button class="btn btn-info btn-xs" data-introindex="8-6"><span class="glyphicon glyphicon-eye-open"></span></button>', $url, ['data-pjax'=>'0','title'=>'Изменить', 'target' => '_blank', 'data-toggle'=>'tooltip']);


if($model->questionary_id == null) $que = '';
else $que = Html::a('<button class="btn btn-primary btn-xs" data-introindex="8-5"><span class="glyphicon glyphicon-file"></span></button>', ['/questionary/questions', 'id' => $model->questionary_id], ['data-pjax'=>'0','title'=> 'Открыть Анкету '.$model->questionary->name, 'target' => '_blank' , 'data-toggle'=>'tooltip']);

$url = Url::to(['/resume/delete', 'id' => $model->id]);
$delete = Html::a('<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>', $url,
[
    'role'=>'modal-remote','title'=>'Удалить',
    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
    'data-request-method'=>'post',
    'data-toggle'=>'tooltip',
    'data-confirm-title'=>'Подтвердите действие',
    'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
]);

?>
<div class="users-page-index">
    
        <div class="question-container">                            
            <div class="row">
                <div class="col-md-7" style="padding-top: 20px;">
                    <div class="panel panel-success">
                        <div class="panel-heading ui-draggable-handle">
                            <h3 class="panel-title"><b><?=$model->questionary->name != null ? $model->questionary->name : 'Блок вопросов' ?></b></h3>
                            <span class="pull-right">
                                <a href="#" class="panel-collapse"><button class="btn btn-warning btn-xs"><span class="fa fa-angle-down"></span></button></a>
                                <?=$add  . '&nbsp;'. $update . '&nbsp; '. $que . '&nbsp; '?>
                            </span>
                        </div>
                        <div class="panel-body">
                            <div style="margin: 10px; max-height: 600px; overflow: auto;">                              
                                <?= SortableInput::widget([
                                    'name'=>'active',
                                    'id'=>'actives',
                                    'items' => $active,
                                    'hideInput' => true,
                                    'sortableOptions' => [
                                        'connected'=>true,
                                        'itemOptions'=>['class'=>'', 'style' => 'background-color: #dff0d8;'],
                                    ],
                                    'options' => ['class'=>'form-control', 'readonly'=>true]
                                ])?>
                            </div>
                        </div>      
                        <div class="panel-footer">
                        </div>                            
                    </div>
                    
                <?php 
                foreach ($dataProvider->getModels() as $model) 
                {
                    $url = Url::to(['/resume/print', 'id' => $model->id]);
                    $print = Html::a('<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-print"></span></button>', $url, ['data-pjax'=>'0','title'=>'Печать', 'target' => '_blank', 'data-toggle'=>'tooltip']);

                    $url = Url::to(['/resume/view', 'id' => $model->id]);
                    $update = Html::a('<button class="btn btn-info btn-xs" data-introindex="8-6"><span class="glyphicon glyphicon-eye-open"></span></button>', $url, ['data-pjax'=>'0','title'=>'Изменить', 'target' => '_blank', 'data-toggle'=>'tooltip']);

                    $share = Html::a('<button class="btn btn-warning btn-xs" data-introindex="8-4"><span class="glyphicon glyphicon-share"></span></button>', ['/'.$model->code], ['data-pjax' => '0','title'=>'Поделиться','target'=> "_blank", 'data-toggle'=>'tooltip']);

                    if($model->questionary_id == null) $que = '';
                    else $que = Html::a('<button class="btn btn-primary btn-xs" data-introindex="8-5"><span class="glyphicon glyphicon-file"></span></button>', ['/questionary/questions', 'id' => $model->questionary_id], ['data-pjax'=>'0','title'=> 'Открыть Анкету '.$model->questionary->name, 'target' => '_blank' , 'data-toggle'=>'tooltip']);

                    $url = Url::to(['/resume/delete', 'id' => $model->id]);
                    $delete = Html::a('<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>', $url,
                        [
                            'role'=>'modal-remote','title'=>'Удалить',
                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                            'data-request-method'=>'post',
                            'data-toggle'=>'tooltip',
                            'data-confirm-title'=>'Подтвердите действие',
                            'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                        ]);

                ?>
                    <div class="panel panel-warning panel-toggled" style="margin-top: 20px;" >
                        <div class="panel-heading ui-draggable-handle">
                            <h3 class="panel-title"><b><?=$model->questionary->name?></b></h3>
                            <span class="pull-right">
                                <a href="#" class="panel-collapse"><button class="btn btn-warning btn-xs"><span class="fa fa-angle-up"></span></button></a>
                                <?=$print . '&nbsp;'. $update . '&nbsp; '. $share . '&nbsp; '. $que . '&nbsp; ' . $delete?> 
                            </span>
                        </div>
                        <div class="panel-body">
                            <?= \Yii::$app->controller->renderPartial('resume_questions', ['model'=>$model]);?>
                        </div>
                    </div>
                <?php
                } ?>
                </div>

                <div class="col-md-5">  
                    <div class="col-md-12" style="padding-top: 20px;">                        
                        <div class="panel panel-success panel-hidden-controls" >
                            <div class="panel-heading ui-draggable-handle">
                                <h1 class="panel-title"> <b>Общие настройки</b> </h1>
                                <?= Html::a('Редактировать &nbsp;<i class="glyphicon glyphicon-pencil"></i>', ['/resume/update', 'id' => $model->id,], [ 'role'=>'modal-remote','title'=> 'Редактировать','class'=>'btn btn-warning btn-rounded pull-right',]) ?>
                                <ul class="panel-controls">
                                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-up"></span></a></li>
                                </ul>
                            </div>
                            <div class="panel-body">
                                <div style="max-height: 600px; overflow: auto; display: flex; justify-content: space-between;"> 
                                    <?php Pjax::begin(['enablePushState' => false, 'id' => 'crud-datatable-pjax']) ?>
                                    <div>
                                    <p><b>ФИО</b> : <?=$model->fio ?></p>
                                    <?php if($model->doptest != null){?>
                                        <p><b>Перейти в осноной результат</b> : <?= Html::a('https://'.$_SERVER['SERVER_NAME'].'/resume/view?id='.$model->doptest, Url::to(['/resume/view', 'id' => $model->doptest]), ['data-pjax'=>'0','title'=>'Изменить', 'target' => '_blank', 'data-toggle'=>'tooltip']) ?></p>
                                    <?php } ?>
                                    <p><b>Баллы за ответы</b> : <?=$model->balls + $model->ball_for_question ?></p>
                                    <p>
                                        <b>Теги : </b>
                                        <ul class="list-tags">
                                            <?php 
                                                foreach (json_decode($model->tags) as $value) {
                                                    $tag = Tags::findOne($value->id);
                                            ?>
                                                <li><a href="#"><span class="fa fa-tag"></span> <?=$tag->name?></a></li>
                                            <?php
                                                }
                                            ?>
                                        </ul>
                                    </p>
                                    <p><b>Названия теста</b> : <?=$questionary->name?></p>
                                    <p><b>Оценка</b> : <?=$model->getMarkDescription()?></p>
                                    <?php if($model->questionary->type == 2) { ?>
                                        <p><b>Время которое ушло на тест</b> : <?= $model->getTime()?></p>
                                        <p><b>Результат</b> : <?= $result ?></p>
                                    <?php } ?>

                                    <p><b>Дата и время заполнения</b> : <?= date('H:i:s d.m.Y', strtotime($model->date_cr) )?></p>
                                    <p><b>Баллы за текстовые ответы</b> : <?=$model->ball_for_question?></p>
                                    <?= Yii::$app->user->identity->type == 0 ? '<p><b>Ip адрес</b> : '.$model->ip.'</p>' : ''?>
                                    </div>
                                    <?php Pjax::end() ?>
                                    <div style="background-image: url(<?=$path?>); background-repeat: no-repeat; background-size: cover; width: 100px; height: 100px; "></div>
                                </div> 
                                       
                                </div>      
                            <div class="panel-footer">
                            </div>                            
                        </div>
                    </div>
                </div>

                

            </div>
        </div>

</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<?php 
$chat = Chat::find()->where(['chat_id' => '#resume-'.$model->id, 'is_read' => 0 ])->andWhere([ '!=', 'user_id', Yii::$app->user->identity->id ])->one();
if($chat != null){
    $chat->is_read = 1;
    $chat->save();
}

?>