<?php
/* @var $this yii\web\View */
use app\models\Settings;

$setting = Settings::find()->where(['key' => 'policy'])->one();
?>

<p>
	<div class="row" style="padding: 50px;">
		<div class="col-md-12">
			<h1 style="color:white;text-align:center;"><?= $setting->name?></h1>
			<br>
			<div style="color:white;">
	    		<?= $setting->text?>				
			</div>
		</div>
	</div>
</p>
