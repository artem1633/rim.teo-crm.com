<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Sliders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sliders-form">

    <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-md-5">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-5">
                <?= $form->field($model, 'view_time')->textInput(['type'=>'number']) ?>
            </div>
            <div class="col-md-2">
                <div style="margin-top:30px;">
                    <?= $form->field($model, 'view')->checkbox()?> 
                </div>
            </div>
        </div>

        <div class="row"> 
            <div class="col-md-12">
                <?= $form->field($model, 'text')->widget(CKEditor::className(),[
                    'editorOptions' => [
                        'preset' => 'full', 
                        'inline' => false, //по умолчанию false
                        'height' => '200px',
                    ],
                    ]);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div id="poster23_file">
                    <?= $model->fone != null ? '<img style="width:100%; height:250px;" src="https://' . $_SERVER["SERVER_NAME"] . "/uploads/sliders/" . $model->fone .' ">' : '' ?>
                </div>
                <br>
                <?= $form->field($model, 'poster23_file')->fileInput(['class'=>"poster23_image"]); ?>
            </div>
        </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>


<?php 
$this->registerJs(<<<JS

$(document).ready(function(){
    var fileCollection = new Array();

    $(document).on('change', '.poster23_image', function(e){
        var files = e.target.files;
        $.each(files, function(i, file){
            fileCollection.push(file);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){
                var template = '<img style="width:100%; max-height:180px;" src="'+e.target.result+'"> ';
                $('#poster23_file').html('');
                $('#poster23_file').append(template);
            };
        });
    });


});

JS
);
?>