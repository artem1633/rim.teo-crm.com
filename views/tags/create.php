<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tags */

?>
<div class="tags-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
