<?php

use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;


\johnitvn\ajaxcrud\CrudAsset::register($this);

?>


<div class="col-md-12" style="padding-top: 30px;">
    <div class="panel panel-warning panel-toggled">
        <div class="panel-heading ui-draggable-handle">
            <h3 class="panel-title"><b>Код виджета</b></h3>
            <ul class="panel-controls">
                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-up"></span></a></li>
                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
            </ul>
        </div>
        <div class="panel-body">
            <div class="content-frame-body content-frame-body-left" style="max-height: 50px; overflow: auto; " >
                <?=htmlspecialchars('<iframe id="otpwgt-undefined46756" src="'.'https://' . $_SERVER['SERVER_NAME'] . '/page/default/standings" frameborder="0" style="width:100%; height: 400px;"  scrolling="no" ></iframe>')?>
            </div>
        </div>
    </div>
</div>

<?php \yii\widgets\Pjax::begin(['id' => 'result-crud-datatable']) ?>

<?=GridView::widget([
    'id'=>'crud-datatable',
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    'responsiveWrap' => false,
    'striped' => false,
    'rowOptions' => function($model, $key, $index, $grid){
        if($index < 10){
            return ['class' => 'success'];
        } else if($index >= 10 && $index < 50){
            return ['class' => 'warning'];
        }else if($index >= 50 && $index < 100){
            return ['class' => 'danger'];
        }
    },
    'pjax'=>false,
    'columns' => [
        [
            'class' => 'kartik\grid\SerialColumn',
            'width' => '30px',
        ],
        [
            'attribute' => 'balls',
            'label' => 'Кол-во баллов',
            'hAlign' => GridView::ALIGN_CENTER,
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'name',
            'label' => 'Название команды',
            'content' => function($data){
                if(Yii::$app->user->isGuest == false){
                    return $data->name.' '.Html::a('<i class="fa fa-plus text-success"></i>', ['command-ball/create', 'commandId' => $data->id, 'reloadPjaxContainer' => '#result-crud-datatable'], ['role' => 'modal-remote']);
                }

                return $data->name;
            }
        ],
    ],
    'toolbar'=> [
        ['content'=> ''
        ],
    ],
    'condensed' => true,
    'responsive' => true,
    'panel' => [
        'type' => 'warning',
        'heading' => '<i class="glyphicon glyphicon-list"></i> Список результатов    
    <button  type="button" class="btn btn-xs btn-warning" onclick=\'startIntro8();\'>Начать обучение</button>',
        'before'=>'',
        'after'=>'',
    ]
])?>


<?php \yii\widgets\Pjax::end() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
