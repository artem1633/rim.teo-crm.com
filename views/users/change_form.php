<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin([ 'options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]); ?>
   
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>        
            </div>
            
            <div class="col-md-6">
                <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>        
            </div>

            <div class="col-md-4">
                <?= $form->field($model, 'telegram_id')->textInput(['maxlength' => true]) ?>        
            </div>

            <div class="col-md-4">
                <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-md-4">
                 <?= $form->field($model, 'new_password')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-md-12">
                <?= $form->field($model, 'description')->widget(CKEditor::className(),[
                    'editorOptions' => [
                        'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                        'height' => '100px',
                    ],
                ]);
                ?>
            </div>
            
        </div>

    <?php ActiveForm::end(); ?>

</div>

