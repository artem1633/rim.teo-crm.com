<?php

use app\models\Lessons;
use app\models\Settings;
use kartik\grid\GridView;
use yii\helpers\Html;
use dosamigos\chartjs\ChartJs;
use app\models\Resume;
use app\models\ResumeStatus;
use app\models\LessonsUsers;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\select2\Select2;


$this->title = 'Рабочий стол';
?>
<div class="site-index">
    <button  type="button" class="btn btn-xs btn-warning" onclick='startIntro();'>Начать обучение</button>

    <br>


    <br>
    <br>

    <div class="row">
        <?php
        foreach ($questionaries as $questionary) {
            $count = Resume::find()->where(['questionary_id' => $questionary->id])->count();
            $statuses = ResumeStatus::find()->all();
            $labels = [];
            $data = [];
            $backgroundColor = [];
            $borderColor = [];
            foreach ($statuses as $status) {
                $labels [] = $status->name;
                $data [] = Resume::find()->where(['questionary_id' => $questionary->id, 'status_id' => $status->id])->count();
                $backgroundColor [] = Resume::getColorCode();
                $borderColor [] = '#fff';
            }

            $kolvo = Resume::find()->where(['questionary_id' => $questionary->id, 'status_id' => null])->count();
            if($kolvo > 0){
                $labels [] = 'Статус не дано';
                $data [] = Resume::find()->where(['questionary_id' => $questionary->id, 'status_id' => null])->count();
                $backgroundColor [] = Resume::getColorCode();
                $borderColor [] = '#fff';
            }
            ?>
            <div class="col-md-4 col-xs-12 col-sm-6" style="padding-top: 40px;">
                <div class="panel panel-warning panel-hidden-controls">
                    <div class="panel-heading ui-draggable-handle">
                        <h3 class="panel-title"><?=$questionary->name?></h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                            <li><a href="#" onclick=" $.post('/questionary/set-status?id=<?=$questionary->id?>'); " class="panel-remove"><span class="fa fa-times"></span></a></li>
                        </ul>
                    </div>
                    <div class="panel-body" style="height: 350px;">
                        <?php
                        if($count == 0){
                            echo "<h3>На этой анкете нету ни каких резюме</h3>";
                        }
                        else {
                            echo ChartJs::widget([
                                'type' => 'pie',
                                'id' => 'questionary' . $questionary->id,
                                'options' => [
                                    'height' => 225,
                                    'width' => 300
                                ],
                                'data' => [
                                    'labels' => $labels,
                                    'datasets' => [
                                        [
                                            'data' => $data,
                                            'backgroundColor' => $backgroundColor,
                                            'borderColor' => $borderColor,
                                        ],
                                    ]
                                ]
                            ]);
                        }
                        ?>

                    </div>
                    <div class="panel-footer">
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>


<?= $this->render('_dashboard_result', [
    'searchModel' => $searchModel,
    'dataProvider' => $dataProvider,
]) ?>

<?php
$lessons_pass = LessonsUsers::getLessonsStatus(1);
if (!$lessons_pass) {
    $param = Settings::find()->where(['key'=>'instruction_text'])->one();
    Modal::begin([
        'header' => '<h2>Добро пожаловать в TEO JOB!</h2>',
        'clientOptions' => ['show' => true],
        'footer' => '<button style="margin-top: -4px;" type="button" class="btn btn-xs btn-warning" '.
            'data-dismiss="modal" onclick=\'startIntro();\'>Начать обучение</button>'
    ]);
    echo $param->text;
    Modal::end();
    LessonsUsers::setPassed(1);
}
$lessons = Lessons::getLessonsGroup(1);
?>
<script type="text/javascript">
    function startIntro(){
        var intro = introJs();
        intro.setOptions({
            nextLabel:"Дальше",
            prevLabel:"Назад",
            skipLabel:"Пропустить",
            doneLabel:"Понятно",
            steps: [
                <?php
                foreach ($lessons as $lesson)
                    echo '{element: document.querySelectorAll(\'[data-introindex="'.$lesson['step'].'"]\')[0],'.
                        'intro: "'.$lesson['hint'].'"},';?>

            ]
        });
        intro.setOption("nextLabel", "Дальше").setOption("prevLabel", "Назад").start();
    }
</script>