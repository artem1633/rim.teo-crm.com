<?php

use yii\helpers\Html;
use yii\helpers\Url;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
?>

    <div class="users-view">
        <div class="row" style="text-align: center;">
            <div class="col-md-12" style="margin-top: 5px;">
                <?= Html::a('Войти через Instagram', ['/users/exit-from-accaunt/', 'url' => $instagram->getLoginUrl()], ['class' => 'btn btn-lg btn-success', 'data-pjax' => '0', 'style' => 'width:350px;']); ?>
            </div>
            <div class="col-md-12" style="margin-top: 5px;">
                <?= Html::a('Войти через VK', ['/users/exit-from-accaunt/', 'url' => $dialog_url], ['class' => 'btn btn-lg btn-primary', 'data-pjax' => '0', 'style' => 'width:350px;']); ?>
            </div>
            <div class="col-md-12" style="margin-top: 5px;">
                <?= Html::a('Войти через VK', ['/users/exit-from-accaunt/', 'url' => $url_facebook], ['class' => 'btn btn-lg btn-warning', 'data-pjax' => '0', 'style' => 'width:350px;']); ?>
            </div>
        </div>
    </div>
