<?php

use yii\helpers\Html;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use app\models\Users;
use app\models\Questionary;

CrudAsset::register($this);
$model = Users::findOne(Yii::$app->user->identity->id);

if (!file_exists('avatars/'.$model->foto) || $model->foto == '') {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/examples/images/users/avatar.jpg';
} else {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/'.$model->foto;
}

?>

<?php Pjax::begin(['enablePushState' => false, 'id' => 'profile-pjax']) ?>
<div class="row">
    <div class="">
        <!-- CONTACT ITEM -->
        <div class="profile-container">
            <div class="panel panel-default">
                <div class="panel-body profile">
                    <div class="profile-image">
                        <img src="<?= $path?>" alt="Nadia Ali">
                    </div>
                    <div class="profile-data">
                        <div class="profile-data-name"><?=$model->fio?></div>
                        <div class="profile-data-title"><?=$model->getTypeDescription()?></div>
                    </div>
                    <div class="profile-controls">
                        <?= Html::a('<span class="fa fa-pencil"></span>', ['/users/change', 'id' => $model->id], [ 'role' => 'modal-remote', 'title'=> 'Профиль','class'=>'profile-control-left']); ?>
                        <?= Html::a('<span class="fa fa-download"></span>', ['/users/avatar'], [ 'role' => 'modal-remote', 'title'=> 'Загрузить аватар','class'=>'profile-control-right']); ?>
                    </div>
                </div>                                
                <div class="panel-body">                                    
                    <div class="contact-info">
                        <p><b>ФИО</b> : <?= $model->fio ?></p> 
                        <p><b>Логин</b> : <?= $model->login ?></p>
                        <p><b>Id чат телеграма</b> : <?= $model->telegram_id ?></p>
                        <p><b>Телефон</b> : <?=$model->telephone ?></p>
                        <p><b>Страница компании</b> : <?=
                            Html::a( 'https://' . $_SERVER['SERVER_NAME'] . '/' . $model->utm , [ '/'.$model->utm ], ['data-pjax'=>'0', 'title'=> 'Создать', 'target'=>'_blank', ]); ?></p>
                        <p><b>Уникальный код</b> : <?=$model->unique_code_for_telegram ?></p>
                        <p><b>Новое сообщение</b> : <?=$model->getNewSmsDescription() ?></p>
                        <p><b>Логин телеграма</b> : <?=$model->telegram_login ?></p>
                        <p><b>Дата регистрации</b> : <?=$model->register_date ?></p>
                        <p><b>Подключен к телеграма </b> : <?=$model->getConnectTelegramDescription() ?></p>
                        <p><b>Баланс основной</b> : <?=$model->main_balance ?></p>
                        <p><b>Баланс партнерский</b> : <?=$model->partner_balance ?></p>
                        <p><b>Реферал</b> : <?= $model->getReferal() ?></p>
                        <p><b>Реферальная ссылка</b> : <?=
                            Html::a( 'https://' . $_SERVER['SERVER_NAME'] . '/site/register?ref='. md5($model->id), [ '/site/register?ref='. md5($model->id) ], ['data-pjax'=>'0', 'title'=> 'Реферальная ссылка', 'target'=>'_blank', ]); ?></p>
                        <p><b>Процент с оплат рефералов</b> : <?=$model->prosent_referal ?></p>
                        <p><b>Стоимость 1 анкеты</b> : <?=$model->questionary_sum ?></p>
                        <p><b>Стоимость 1 резюме</b> : <?=$model->resume_sum ?></p>
                        <p><b>Дата последнего захода</b> : <?=$model->last_login_date ?></p>
                        <p><b>Количество резюме</b> : <?= $model->getResumeCount() ?></p>
                        <p><b>Количество анкеты</b> : <?= Questionary::find()->where(['user_id' => $model->id])->count() ?></p>
                        <p><b>Описание компании</b> : <?=$model->description ?></p>                              
                    </div>
                </div>                                
            </div>
    </div>
        <!-- END CONTACT ITEM -->
    </div>                       
</div>
<?php Pjax::end() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>