<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\Pjax;
use kartik\sortinput\SortableInput;
use yii\widgets\ActiveForm;
use app\models\Tags;
use app\models\Questionary;


CrudAsset::register($this);
$this->title = 'Ползователь';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="users-page-index">
    <?php Pjax::begin(['enablePushState' => false, 'id' => 'crud-datatable-pjax']) ?>
        <div class="question-container">                            
            <div class="row">
                <div class="col-md-6" style="padding-top: 20px;">

                        <div class="panel panel-success panel-hidden-controls" >
                            <div class="panel-heading ui-draggable-handle">
                                <h3 class="panel-title"><b>Чат</b></h3>
                                <ul class="panel-controls">
                                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-up"></span></a></li>
                                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                </ul>
                            </div>
                            <div class="panel-body">
                                <div class="content-frame-body content-frame-body-left" style="max-height: 400px; overflow: auto; " >
                                    <div class="messages messages-img">
                                        <?php 
                                        foreach ($chatText as $value) {
                                            $identity = Yii::$app->user->identity;
                                            if($value->user_id == $identity->id) $in = '';
                                            else $in = 'in';
                                            if (!file_exists('avatars/'.$identity->foto) || $identity->foto == '') {
                                                $path = 'http://' . $_SERVER['SERVER_NAME'].'/examples/images/users/avatar.jpg';
                                            } else {
                                                $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/'.$identity->foto;
                                            }
                                        ?>
                                        <div class="item <?=$in?> item-visible">
                                            <div class="image">
                                                <img src="<?=$path?>" alt="<?=$value->user->fio?>">
                                            </div>
                                            <div class="text">
                                                <div class="heading">
                                                    <a href="#"><?=$value->user->fio?></a>
                                                    <span class="date"><?= date( 'H:i:s d.m.Y', strtotime($value->date_time) ) ?></span>
                                                </div>
                                                <?=$value->text?>
                                            </div>
                                        </div>
                                        <?php } ?>                            
                                    </div>                                    
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="panel panel-default push-up-10">
                                    <div class="panel-body panel-body-search">
                                        <?php $form = ActiveForm::begin(); ?>
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <button class="btn btn-warning"><span class="fa fa-camera"></span></button>
                                                    <button class="btn btn-danger"><span class="fa fa-chain"></span></button>
                                                </div>
                                                <input type="text" name="text" class="form-control" placeholder="Написать сообщение...">
                                                <div class="input-group-btn">
                                                    <button class="btn btn-info">Отправить</button>
                                                </div>
                                            </div>
                                        <?php ActiveForm::end(); ?> 
                                    </div>
                                </div>
                            </div>
                        </div>
                                           
                </div>
 
                <div class="col-md-6">   
                    <div class="col-md-12" style="padding-top: 20px;">                        
                        <div class="panel panel-success panel-hidden-controls" >
                            <div class="panel-heading ui-draggable-handle">
                                <h1 class="panel-title"> <b>Пользователь</b> </h1>
                                <?= Html::a('Редактировать &nbsp;<i class="glyphicon glyphicon-pencil"></i>', ['/users/update', 'id' => $model->id,], [ 'role'=>'modal-remote','title'=> 'Редактировать','class'=>'btn btn-warning btn-rounded pull-right',]) ?>
                                <ul class="panel-controls">
                                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                </ul>                                
                            </div>
                            <div class="panel-body">
                                <div style="margin: 10px; max-height: 600px; overflow: auto;"> 
                                    <p><b>ФИО</b> : <?= $model->fio ?></p> 
                                    <p><b>Логин</b> : <?= $model->login ?></p>
                                    <p><b>Id чат телеграма</b> : <?= $model->telegram_id ?></p>
                                    <p><b>Телефон</b> : <?=$model->telephone ?></p>
                                    <p><b>Страница компании</b> : <?=
                                        Html::a( 'https://' . $_SERVER['SERVER_NAME'] . '/' . $model->utm , [ '/'.$model->utm ], ['data-pjax'=>'0', 'title'=> 'Создать', 'target'=>'_blank', ]); ?></p>
                                    <p><b>Уникальный код</b> : <?=$model->unique_code_for_telegram ?></p>
                                    <p><b>Новое сообщение</b> : <?=$model->getNewSmsDescription() ?></p>
                                    <p><b>Логин телеграма</b> : <?=$model->telegram_login ?></p>
                                    <p><b>Дата регистрации</b> : <?=$model->register_date ?></p>
                                    <p><b>Подключен к телеграма </b> : <?=$model->getConnectTelegramDescription() ?></p>
                                    <p><b>Баланс основной</b> : <?=$model->main_balance ?></p>
                                    <p><b>Баланс партнерский</b> : <?=$model->partner_balance ?></p>
                                    <p><b>Реферал</b> : <?= $model->getReferal() ?></p>
                                    <p><b>Реферальная ссылка</b> : <?=
                                        Html::a( 'https://' . $_SERVER['SERVER_NAME'] . '/site/register?ref='. md5($model->id), [ '/site/register?ref='. md5($model->id) ], ['data-pjax'=>'0', 'title'=> 'Реферальная ссылка', 'target'=>'_blank', ]); ?></p>
                                    <p><b>Процент с оплат рефералов</b> : <?=$model->prosent_referal ?></p>
                                    <p><b>Стоимость 1 анкеты</b> : <?=$model->questionary_sum ?></p>
                                    <p><b>Стоимость 1 резюме</b> : <?=$model->resume_sum ?></p>
                                    <p><b>Дата последнего захода</b> : <?=$model->last_login_date ?></p>
                                    <p><b>Количество резюме</b> : <?= $model->getResumeCount() ?></p>
                                    <p><b>Количество анкеты</b> : <?= Questionary::find()->where(['user_id' => $model->id])->count() ?></p>
                                    <p><b>Описание компании</b> : <?=$model->description ?></p>
                                    </div>
                                </div>      
                                                       
                        </div>
                    </div>
                  
                </div>

            </div>
        </div>
    <?php Pjax::end() ?>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>