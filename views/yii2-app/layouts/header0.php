<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\helpers\Url;
use app\models\Users;

$user_id = Yii::$app->user->identity->id;
/*Yii::$app->db->createCommand()->update('users', ['active_date' => date('Y-m-d H:i:s')], [ 'id' => \Yii::$app->user->identity->id ])->execute();*/

?>

<header >

    <div class="wrapper">
        <div class="logo">
            <a href="<?=Url::toRoute([Yii::$app->homeUrl])?>"><h2><?=Yii::$app->name?></h2></a>
        </div>
        <div class="hdr_menu">
            <ul>
                <li class=""><?= Html::a('Рабочий стол', ['/#'], []); ?></li>
                <li class=""><?= Html::a('Резюме', ['/resume/index'], []); ?></li>
                <li class=""><?= Html::a('Анкеты', ['/questionary/index'], []); ?></li>
                <?php if(Yii::$app->user->identity->type == 0) { ?>
                <li class="dropdown">
                    <a href="#" class="open_toggle">Администратор <span class="caret"></span></a>
                    <ul class="dropdown_body">
                        <li><?= Html::a('Пользователи', ['/users/index'], []); ?></li>
                        <li><?= Html::a('Настройки', ['/settings/index'], []); ?></li>
                    </ul>
                </li>
                <?php } ?>
                <li class="dropdown">
                    <a href="#" class="open_toggle">Настройки <span class="caret"></span></a>
                    <ul class="dropdown_body">
                        <li><?= Html::a('Уведомление', ['/notification/index'], []); ?></li>
                        <li><?= Html::a('Ваканция', ['/vacancy/index'], []); ?></li>
                        <li><?= Html::a('Группа', ['/group/index'], []); ?></li>
                        <li><?= Html::a('Статус резюме', ['/resume-status/index'], []); ?></li>
                    </ul>
                </li>               
            </ul>
        </div>
        <div class="hdr_rght">
            <div class="navbar-custom-menu">

                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span><?=Yii::$app->user->identity->fio ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-header">
                                <img src="<?= 'http://' . $_SERVER['SERVER_NAME'] ?>/images/nouser.png" class="img-circle" alt="User Image"/>
                                <p> <?=Yii::$app->user->identity->fio ?> </p>
                            </li>
                            <li class="user-footer">
                                    <?= Html::a('Профиль', ['users/change', 'id' => Yii::$app->user->identity->id],
                                    ['role'=>'modal-remote','title'=> 'Профиль','class'=>'btn btn-default btn-flat']); ?>
                                    <?= Html::a(
                                        'Выход',
                                        ['/site/logout'],
                                        ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                    ) ?>
                            </li>
                        </ul>
                    </li>
                    
                </ul>
            </div>
        </div>
        <div class="hamburger">
            <span></span>
            <span></span>
            <span></span>
        </div>            
    </div>
         
</header>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => "modal-lg",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>