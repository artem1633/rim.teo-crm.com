<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\helpers\Url;
use app\models\Users;
use yii\widgets\Pjax;

if (!file_exists('avatars/'.Yii::$app->user->identity->foto) || Yii::$app->user->identity->foto == '') {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/examples/images/users/avatar.jpg';
} else {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/'.Yii::$app->user->identity->foto;
}

?>
<?php 
        $session = Yii::$app->session;
        if($session['left'] == 'small') $left="x-navigation-minimized";
        else $left="x-navigation-custom";
    ?>
<!-- START PAGE SIDEBAR -->
            <div class="page-sidebar page-sidebar-fixed scroll">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation <?=$left?>">
                    <li class="xn-logo">
                        <a href="<?=Url::toRoute([Yii::$app->homeUrl])?>"><?=Yii::$app->name?></a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-title">Menu</li>
                    <li class="introduction-first" data-introindex="2" data-intro="Текст2">
                        <?= Html::a('<span class="fa fa-desktop"></span> <span class="xn-text">Показатели</span>', ['/users/dashboard'], []); ?>
                    </li>
                    <li class="introduction-first" data-introindex="3" data-intro="Текст3">
                        <?= Html::a('<span class="fa fa-files-o" style="width: 30px;"> <span class="xn-visible" style="color: #ffffff;font-size: 10px;width:0px;">Результаты</span></span> <span class="xn-text">Результаты</span>', ['/resume/index'], []); ?>
                    </li>
                    <li class="introduction-first" data-introindex="4" data-intro="Текст4">
                        <?= Html::a('<span class="fa fa-file-o" style="width: 30px;"><span class="xn-visible" style="color: #ffffff;font-size: 10px;width:0px;">Тесты</span></span> <span class="xn-text">Тесты</span>', ['/questionary/index'], []); ?>
                    </li>
                    <li class="introduction-first" data-introindex="5" data-intro="Текст5">
                        <?= Html::a('<span class="fa fa-bookmark"></span>Шаблоны', ['/questionary/index', 'is_template' => 1], []); ?>
                    </li>
                    <li class="introduction-first" data-introindex="5" data-intro="Текст5">
                        <?= Html::a('<span class="fa fa-users"></span>Команды', ['/command/index'], []); ?>
                    </li>
                    <li class="introduction-first" data-introindex="5" data-intro="Текст5">
                        <?= Html::a('<span class="fa fa-circle"></span>Быллы комманды', ['/command-ball/index'], []); ?>
                    </li>

                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->