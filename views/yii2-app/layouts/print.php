<?php
use yii\helpers\Html;

    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>

    <body class="">
    <?php $this->beginBody() ?>
        <div class="page-container ">

            <!-- PAGE CONTENT -->
            <div class="page-content">
                <?= $content ?>
             </div>                       
            <!-- END PAGE CONTENT -->
        </div>
    <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>

