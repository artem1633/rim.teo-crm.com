$(document).ready(function(){
    $('#search-btn').click(function(){
        var value = $('#search-input').val();


        $('#search-body-body tr').each(function(){
            var name = $(this).data('name').toLowerCase();

            if(value == ''){
                $(this).show();
            } else {
                if(name.indexOf(value.toLowerCase()) > -1){
                    $(this).show();
                } else {
                    $(this).hide();
                }
            }
        });
    });

    $('#search-input').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            $('#search-btn').trigger('click');
        }
    });
});